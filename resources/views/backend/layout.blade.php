    <!-- $company_url  = Request()->company; -->
    <!-- dd($company_info); -->
    <!-- if( empty($company_info->company_id) ) {
        echo '<script>window.location = "'.url('404-error').'"</script>';
        die;
    } -->
    @php
    $company_url = 'mahadev';
    $company_info = App\Model\Company::where('company_sitename', 'LIKE', $company_url)->first();

    $company = $company_info->company_sitename;
    @endphp

    @if(session()->has('user_auth'))

    @php
    $profile = App\Model\User::with('role')->find(session('user_auth'));
    @endphp

    @if(auth()->user()->role_id == 1)
        @include('backend.common.header')
        @include('backend.inc.'.$page)
        @include('backend.common.footer')
    @else
        @include('backend.common.agentheader')
        @include('backend.inc.'.$page)
        @include('backend.common.agentfooter')

    @endif

    @else

    @include('backend.login')

    @endif