@php
$session = session('destinations');
print_r($session);

if(!empty($session))
    $cities = \App\Model\City::with(['state', 'country'])->whereIn('city_id', $session)->get();

@endphp
@if(!empty($session))
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th>Country</th>
                    <th>State</th>
                    <th>City</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cities as $ct)
                <tr>
                    <td>{{ $ct->country->country_name }}</td>
                    <td>{{ $ct->state->state_name }}</td>
                    <td>{{ $ct->city_name }}</td>
                    <td> <a href="#remove-dest"><i class="icon-close"></i></a> </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@else
    <div class="alert alert-warning">
        None of any destination place is added yet.
    </div>
@endif
