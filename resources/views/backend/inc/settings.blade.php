<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1>Settings</h1>
            </div>
            <ul class="breadcrumb float-right clearfix">
                <li class="breadcrumb-item"><a href="{{ url('service-panel') }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Settings</li>
            </ul>
        </div>
    </div>
</section>
<div>
    <form method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-sm-12 m-auto">
                <div class="card">
                    <h3 class="card-title mb-3">
                        <i class="icon-cog"></i> Settings
                    </h3>
                    @if (\Session::has('success'))
                    <div class="alert alert-success">
                        {!! \Session::get('success') !!}
                    </div>
                    @endif

                    <div class="form-group">
                        <label>Service App Version</label>
                        <input type="text" name="record[setting_customer_app_version]" value="{{ @$edit->setting_customer_app_version }}" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Partner App Version</label>
                        <input type="text" name="record[setting_partner_app_version]" value="{{ @$edit->setting_partner_app_version }}" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Refer String</label>
                        <input type="text" name="record[setting_refer_string]" value="{{ @$edit->setting_refer_string }}" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Refer Amount</label>
                        <input type="text" name="record[setting_refer_amount]" value="{{ @$edit->setting_refer_amount }}" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Max wallet amount can be used</label>
                        <input type="text" name="record[setting_max_wallet_use]" value="{{ @$edit->setting_max_wallet_use }}" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label>Max percentage of wallet amount can be used</label>
                        <input type="text" name="record[setting_max_wallet_percent_use]" value="{{ @$edit->setting_max_wallet_percent_use }}" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
