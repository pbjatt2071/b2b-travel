<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-cart"></i> Add-on Items</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ url($company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li class="active">Add-on Items</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ url($company.'/add-on-item/add') }}" class="btn btn-primary">Add New</a>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
    @endif
    <div class="card">
        <h3 class="card-title"><i class="icon-filter1"></i> Filter By</h3>
        {{ Form::open(['method' => 'GET']) }}
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">
                    <input type="text" name="search[keyword]" value="{{ @$search['keyword'] }}" class="form-control" placeholder="By keywords">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block"><i class="icon-search"></i> Search</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
	<div class="card">
        {{ Form::open() }}
            <a href="#remove" class="float-right" data-toggle="tooltip" title="Remove Selected"> <i class="icon-trash-o"></i> </a>
    		<h3 class="card-title">
                <div class="mr-auto"><i class="icon-cart"></i> View Add-on Items</div>
    		</h3>
		    @if(!$records->isEmpty())
		    <div class="table-responsive">
			    <table class="table table-bordered">
			          <thead>
			               <tr>
			                    <th style="width: 50px;">
			                        <label class="animated-checkbox">
			                            <input type="checkbox" class="checkall">
			                            <span class="label-text"></span>
			                        </label>
			                    </th>
			                   <th style="width: 50px;">S.No.</th>
                               <th>Image</th>
                               <th>Name</th>
			                   <th>Description</th>
			                   <th>Unit</th>
                               <th>Price</th>
			               </tr>
			          </thead>

			          <tbody>
			          		@php $sn = $records->firstItem(); @endphp
			          		@foreach($records as $rec)
                            @php
                                $image   = !empty( $rec->item_image ) ? url( "/imgs/items/".$rec->item_image ) : url( 'imgs/no-image.png' );
                            @endphp
			               	<tr>
		                        <td>
		                            <label class="animated-checkbox">
		                                <input type="checkbox" name="check[]" value="{{ $rec->country_id  }}" class="check">
		                                <span class="label-text"></span>
		                            </label>
		                        </td>
								<td>{{ $sn++ }}</td>
                                <td> <img src="{{ $image }}" alt="{{ $rec->item_name }}" title="{{ $rec->item_name }}" class="table-img"> </td>
								<td>
                                    <a href="{{ url($company.'/add-on-item/add/'.$rec->item_id) }}" class="pencil">
                                        <i class="icon-pencil" title="Edit"></i> {{ $rec->item_name }}
                                    </a>
                                </td>
                                <td>{!! $rec->item_description !!}</td>
                                <td>{{ $rec->unit->unit_name }}</td>
                                <td><i class="icon-inr"></i> {{ $rec->item_price }}</td>
			               	</tr>
			               @endforeach
			          </tbody>
			    </table>
			</div>
            @php
                $get_param = request()->input();
                if(isset($get_param['page'])) {
                    unset($get_param['page']);
                }
            @endphp
            {{ $records->appends($get_param)->links() }}
		    @else
		    <div class="no_records_found">
		      No records found yet.
		    </div>
			@endif
		{{ Form::close() }}
	</div>
</div>
