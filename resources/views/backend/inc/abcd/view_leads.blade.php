<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-user2"></i> Leads</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ url($company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li class="active">Leads</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ url($company.'/leads/add') }}" class="btn btn-primary">Create New</a>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
    @endif
    <div class="card">
        <h3 class="card-title"><i class="icon-filter1"></i> Filter By</h3>
        {{ Form::open(['method' => 'GET']) }}
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">
                    <input type="text" name="search[keyword]" value="{{ @$search['keyword'] }}" class="form-control" placeholder="By keywords">
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block"><i class="icon-search"></i> Search</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
	<div class="card">
        @if(!$records->isEmpty())
            {{ Form::open() }}
            <a href="#remove" class="float-right" data-toggle="tooltip" title="Remove Selected"> <i class="icon-trash-o"></i> </a>
    		<h3 class="card-title">
                {{ $records->total() }} records(s) found.
    		</h3>
		    <div class="table-responsive">
			    <table class="table table-bordered">
			          <thead>
			               <tr>
			                    <th style="width: 50px;">
			                        <label class="animated-checkbox">
			                            <input type="checkbox" class="checkall">
			                            <span class="label-text"></span>
			                        </label>
			                    </th>
			                   <th style="width: 50px;">S.No.</th>
                               <th>Lead Track ID</th>
                               <th>Customer Info</th>
                               <th>Lead Info</th>
                               <th>Date Info</th>
                               <th>Remarks</th>
			               </tr>
			          </thead>

			          <tbody>
			          		@php $sn = $records->firstItem(); @endphp
			          		@foreach($records as $rec)
                            @php
                                $image   = !empty( $rec->user->user_image ) ? url( "/imgs/users/{$company}/".$rec->user->user_image ) : url( 'imgs/no-image.png' );
                            @endphp
			               	<tr>
		                        <td>
		                            <label class="animated-checkbox">
		                                <input type="checkbox" name="check[]" value="{{ $rec->country_id  }}" class="check">
		                                <span class="label-text"></span>
		                            </label>
		                        </td>
								<td>{{ $sn++ }}</td>
                                <td>
                                    <a href="{{ url($company.'/leads/add/'.$rec->lead_id) }}" class="pencil">
                                        <i class="icon-pencil" title="Edit"></i> {{ sprintf("#%06d", $rec->lead_id)  }}
                                    </a>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <strong>Traveller Name:</strong>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ @$rec->lead_customer_name }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <strong>Contact No.:</strong>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ @$rec->lead_contact_no }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <strong>Phone No.:</strong>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ @$rec->lead_phone }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <strong>Email:</strong>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ @$rec->lead_customer_email }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <strong>Address:</strong>
                                        </div>
                                        <div class="col-sm-8">
                                            {!! nl2br(@$rec->lead_address1."\n".@$rec->lead_address2) !!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <strong>State:</strong>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ @$rec->state->state_name }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <strong>City:</strong>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ @$rec->city->city_name }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <strong>Pincode:</strong>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ @$rec->pincode->pin_code }}
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <strong>Query For:</strong>
                                        </div>
                                        <div class="col-sm-6">
                                            {{ $rec->lead_query_for }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <strong>Visiting Country:</strong>
                                        </div>
                                        <div class="col-sm-6">
                                            {{ $rec->country->country_short_name }}
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <strong>No. Of Adult:</strong>
                                        </div>
                                        <div class="col-sm-2">
                                            {{ $rec->lead_adult_no }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <strong>No. Of Kids:</strong>
                                        </div>
                                        <div class="col-sm-2">
                                            {{ $rec->lead_kid_no }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <strong>No. Of Infant:</strong>
                                        </div>
                                        <div class="col-sm-2">
                                            {{ $rec->lead_infant }}
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <!-- <div class="row">
                                        <div class="col-sm-6">
                                            <strong>Next Contact Date:</strong>
                                        </div>
                                        <div class="col-sm-6">
                                            {!! strtotime($rec->lead_next_contact_date) > 0 ? date( 'd/m/Y', strtotime($rec->lead_next_contact_date) ) : '&mdash;' !!}
                                        </div>
                                    </div> -->
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <strong>Created At:</strong>
                                        </div>
                                        <div class="col-sm-6">
                                            {!! strtotime($rec->lead_created_on) > 0 ? date( 'd/m/Y h:i A', strtotime($rec->lead_created_on) ) : '&mdash;' !!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <strong>Last Updated At:</strong>
                                        </div>
                                        <div class="col-sm-6">
                                            {!! strtotime($rec->lead_updated_on) > 0 ? date( 'd/m/Y h:i A', strtotime($rec->lead_updated_on) ) : '&mdash;' !!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <strong>Lead Date:</strong>
                                        </div>
                                        <div class="col-sm-6">
                                            {!! strtotime($rec->lead_date) > 0 ? date( 'd/m/Y', strtotime($rec->lead_date) ) : '&mdash;' !!}
                                        </div>
                                    </div>
                                </td>
								<td>{{ $rec->lead_remark }}</td>
			               	</tr>
			               @endforeach
			          </tbody>
			    </table>
			</div>
            @php
                $get_param = request()->input();
                if(isset($get_param['page'])) {
                    unset($get_param['page']);
                }
            @endphp
            {{ $records->appends($get_param)->links() }}
		    @else
		    <div class="no_records_found">
		      No records found yet.
		    </div>
			@endif
		{{ Form::close() }}
	</div>
</div>
