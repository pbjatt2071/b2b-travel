<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-cart"></i> {{ !empty($edit->item_id) ? "Edit" : "Add" }} Item</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ url($company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li><a href="{{ url($company.'/add-on-item') }}">Item</a></li>
                    <li class="active">{{ !empty($edit->item_id) ? "Edit" : "Add" }} Item</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ url($company.'/add-on-item/') }}" class="btn btn-primary">View Add-on Items</a>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    {{ Form::open(["files" => true]) }}
        @if (\Session::has('danger'))
            <div class="alert alert-danger">
                {!! \Session::get('danger') !!}</li>
            </div>
        @endif
    	<div class="row">
    		<div class="col-sm-8 col-lg-9">
                <div class="card">
                    <h3 class="card-title">Item Information</h3>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Item Name (Required)</label>
                                <input type="text" name="record[item_name]" value="{{ @$edit->item_name }}" placeholder="Name" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Unit (Required)</label>
                                <select class="form-control" name="record[item_unit]" required>
                                    <option value="">Select Unit</option>
                                  
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Price (Required)</label>
                                <input type="number" name="record[item_price]" value="{{ @$edit->item_price }}" min="0" placeholder="Price" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="record[item_description]" rows="5" class="form-control" placeholder="Description"></textarea>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-lg-3">
                <div class="card">
                    <button type="submit" class="btn btn-block btn-primary">Submit</button>
                </div>
                <div class="card">
                    <h3 class="card-title">Upload Image</h3>
                    <label class="upload_image">
                        <img src="{{ empty($edit->item_image) ? url('imgs/no-image.png') : url("imgs/items/".$edit->item_image) }}" alt="Upload Image" title="Upload Image">
                        <input type="file" name="item_image" accept="image/*" id="itemImage">
                    </label>
                    <label for="itemImage" class="btn btn-primary btn-block">Choose Image</label>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
