<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-tasks"></i> {{ !empty($edit->lead_id) ? "Edit" : "Add" }} Leads</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ url($company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li><a href="{{ url($company.'/leads') }}">Leads</a></li>
                    <li class="active">{{ !empty($edit->lead_id) ? "Edit" : "Add" }} Leads</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ url($company.'/leads/') }}" class="btn btn-primary">View Leads</a>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    {{ Form::open(["files" => true]) }}
        @if (\Session::has('danger'))
            <div class="alert alert-danger">
                {!! \Session::get('danger') !!}</li>
            </div>
        @endif
    	<div class="row">
    		<div class="col-sm-6">
                <div class="card">
                    <h3 class="card-title">Traveller Information</h3>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Date</label>
                                <input type="text" name="record[lead_date]" value="{{ isset($edit->lead_date) ? date('Y-m-d', strtotime($edit->lead_date)) : date('Y-m-d') }}" class="form-control datepicker_no_future" readonly required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Select Account</label>
                                <select class="form-control" name="record[lead_uid]" id="leadAccount" data-url="{{ $company.'/ajax/get_account' }}">
                                    <option value="">Select Account</option>
                                    @foreach($customers as $c)
                                    <option value="{{ $c->user_id }}" @if(@$edit->lead_uid == $c->user_id) selected @endif>{{ $c->user_name.' '.$c->user_mobile }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Traveller Name</label>
                                <input type="text" name="record[lead_customer_name]" value="{{ @$edit->lead_customer_name }}" placeholder="Traveller Name *" class="form-control name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Email Address</label>
                                <input type="email" name="record[lead_customer_email]" value="{{ @$edit->lead_customer_email }}" placeholder="Email Address" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Mobile No.</label>
                                <input type="tel" name="record[lead_contact_no]" value="{{ @$edit->lead_contact_no }}" placeholder="Mobile No. *" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Phone / Landline No.</label>
                                <input type="tel" name="record[lead_phone]" value="{{ @$edit->lead_phone }}" placeholder="Phone / Landline No." class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Address Line 1</label>
                        <input type="text" name="record[lead_address1]" value="{{ @$edit->lead_address1 }}" class="form-control" placeholder="Address Line 1 *" required>
                    </div>
                    <div class="form-group">
                        <label>Address Line 2</label>
                        <input type="text" name="record[lead_address2]" value="{{ @$edit->lead_address2 }}" class="form-control" placeholder="Address Line 2">
                    </div>

                    <input type="hidden" name="record[lead_country]" value="{{ $country_id }}">

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>State</label>
                                <select class="form-control state" data-target="#leadCity" name="record[lead_state]" required>
                                    <option value="">Select State</option>
                                    @if(!empty($states))
                                        @foreach($states as $s)
                                            <option value="{{ $s->state_id }}" @if(@$edit->lead_state == $s->state_id) selected @endif>{{ $s->state_name.' ('.$s->state_short_name.')' }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>City</label>
                                <select class="form-control city" data-target="#leadPincode" id="leadCity" name="record[lead_city]" required>
                                    <option value="">Select City</option>
                                    @if(!empty($cities))
                                        @foreach($cities as $c)
                                            <option value="{{ $c->city_id }}" @if(@$edit->lead_city == $c->city_id) selected @endif>{{ $c->city_name.' ('.$c->city_short_name.')' }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Pincode</label>
                                <select class="form-control" id="leadPincode" name="record[lead_pincode]" required>
                                    <option value="">Select Pincode</option>
                                    @if(!empty($pincodes))
                                        @foreach($pincodes as $p)
                                            <option value="{{ $p->pin_id }}" @if(@$edit->lead_pincode == $c->pin_id) selected @endif>{{ $c->pin_code }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="animated-checkbox">
                            <input type="checkbox" name="create_account" value="Yes">
                            <span class="label-text">Create Account</span>
                        </label>
                    </div>
                    <div class="form-group user_data d-none">
                        <label>User ID</label>
                        <input type="text" name="user[user_login]" class="form-control" autocomplete="new-username">
                    </div>
                    <div class="form-group user_data d-none">
                        <label>Password</label>
                        <input type="text" name="user[user_login]" class="form-control" autocomplete="new-password">
                    </div>
        		</div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <h3 class="card-title">Lead Information</h3>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Query For (Required)</label>
                    			<select class="form-control" name="record[lead_query_for]" id="lead_visa" required>
                                    <option value="">Select Option</option>
                                    <option value="FIT" @if(@$edit->lead_query_for == "Visa") selected @endif>FIT (Free Independent Traveler)</option>
                                    <option value="Visa" @if(@$edit->lead_query_for == "Visa") selected @endif>Visa</option>
                                    <option value="Hotel" @if(@$edit->lead_query_for == "Hotel") selected @endif>Hotel</option>
                                    <option value="Air ticket" @if(@$edit->lead_query_for == "Air ticket") selected @endif>Air ticket</option>
                                    <option value="Holidays Packages" @if(@$edit->lead_query_for == "Holidays Packages") selected @endif>Holidays Packages</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6 lead_data fit">
                            <div class="form-group">
                                <label>Type Of Holiday (Required)</label>
                                <select class="form-control" name="record[lead_holiday_type]" required>
                                    <option value="">Select Type Of Holiday</option>
                                    <option value="Adventure / Camping">Adventure / Camping</option>
                                    <option value="Honeymoon">Honeymoon</option>
                                    <option value="Cruise">Cruise</option>
                                    <option value="Corporate / Event">Corporate / Event</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6 lead_data visa" @if(@$edit->lead_query_for == "Visa") style="display: block;" @endif>
                            <div class="form-group">
                                <label>Country (Required)</label>
                    			<select class="form-control" name="record[lead_destination]" required>
                                    <option value="">Select Country</option>
                                    @if(!$countries->isEmpty())
                                        @foreach($countries as $c)
                                            <option value="{{ $c->country_id }}" @if(@$edit->lead_destination == $c->country_id) selected @endif>{{ $c->country_name.' ('.$c->country_short_name.')' }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Next Follow-up Date</label>
                                <input type="text" name="record[lead_next_contact_date]" value="{{ @$edit->lead_next_contact_date }}" class="form-control datepicker_no_past" placeholder="yyyy-mm-dd" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6 lead_data visa fit" @if(@$edit->lead_query_for == "Visa") style="display: block;" @endif>
                            <div class="form-group">
                                <label>No. Of Adult (Required)</label>
                                <input type="number" min="1" name="record[lead_adult_no]" value="{{ @$edit->lead_adult_no }}" class="form-control" placeholder="No. Of Adult" required>
                            </div>
                        </div>
                        <div class="col-sm-6 lead_data visa fit" @if(@$edit->lead_query_for == "Visa") style="display: block;" @endif>
                            <div class="form-group">
                                <label>No. Of Kids (Required)</label>
                                <input type="number" min="0" name="record[lead_kid_no]" value="{{ @$edit->lead_kid_no }}" class="form-control" placeholder="No. Of Kids" required>
                            </div>
                        </div>
                        <div class="col-sm-6 lead_data visa fit" @if(@$edit->lead_query_for == "Visa") style="display: block;" @endif>
                            <div class="form-group">
                                <label>No. Of Infant (Required)</label>
                                <input type="number" min="0" name="record[lead_infant]" value="{{ @$edit->lead_infant }}" class="form-control" placeholder="No. Of Infant" required>
                            </div>
                        </div>
                        <div class="col-sm-6 lead_data visa">
                            <div class="form-group">
                                <label>Visa Type (Required)</label>
                                <select class="form-control" name="record[lead_visa_type]" required>
                                    <option value="">Select Visa Type</option>
                                    @foreach($visa_types as $vt)
                                    <option value="{{ $vt->vtype_name }}" @if(@$edit->lead_visa_type == $vt->vtype_name) selected @endif>{{ $vt->vtype_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6 lead_data">
                            <div class="form-group">
                                <label>Priority (Required)</label>
                                <select class="form-control" name="record[lead_priority]" required>
                                    <option value="">Select Priority</option>
                                    <option value="High" @if(@$edit->lead_priority == "High") selected @endif>High</option>
                                    <option value="Normal" @if(@$edit->lead_priority == "Normal") selected @endif>Normal</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Reference (Required)</label>
                                <select class="form-control" name="record[lead_reference]" required>
                                    <option value="">Select Reference</option>
                                    <option value="B2B" @if(@$edit->lead_reference == "B2B") selected @endif>B2B</option>
                                    <option value="Website" @if(@$edit->lead_reference == "Website") selected @endif>Website</option>
                                    <option value="Direct" @if(@$edit->lead_reference == "Direct") selected @endif>Direct</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Lead Pass To (Required)</label>
                                <select class="form-control" name="record[lead_staff]">
                                    <option value="">Lead Pass To</option>
                                    @if(!$staffs->isEmpty())
                                        @foreach($staffs as $s)
                                            <option value="{{ $s->user_id }}" @if(@$edit->lead_staff == $s->user_id) selected @endif>{{ $s->user_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="lead_data fit">
                        <input type="hidden" id="total_destination_no" value="1">

                        <div class="row">
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Select Country</label>
                                            <select class="form-control country" id="destCountry" data-target="#destState" name="destination[ldest_country]">
                                                <option value="">Select Country</option>
                                                @foreach($countries as $c)
                                                <option value="{{ $c->country_id }}">{{ $c->country_name." (".$c->country_short_name.")" }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Select State</label>
                                            <select class="form-control state" data-target="#destCity" id="destState" name="destination[ldest_state]">
                                                <option value="">Select State</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Select City</label>
                                            <select class="form-control" id="destCity" name="destination[ldest_city]">
                                                <option value="">Select City</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label>&nbsp;</label>
                                <button type="button" class="btn btn-block btn-primary" id="add_destination">Add</button>
                            </div>
                        </div>
                        <div class="mb-3" id="destinationList">
                            @include('backend.template.view_destinations')
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Remarks</label>
                        <textarea name="record[lead_remark]" class="form-control" rows="8" placeholder="Remarks">{!! @$edit->lead_remark !!}</textarea>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-5 text-right">
                <button type="submit" class="btn btn-primary btn">Submit</button>
            </div>
        {{ Form::close() }}
    </div>
</div>
