<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-tasks"></i> {{ !empty($edit->lead_id) ? "Edit" : "Add" }} Activity</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ url($company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li><a href="{{ url($company.'/activity') }}">Activity</a></li>
                    <li class="active">{{ !empty($edit->activity_id) ? "Edit" : "Add" }} Activity</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ url($company.'/activity/') }}" class="btn btn-primary">View Activities</a>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    {{ Form::open(["files" => true]) }}
        @if (\Session::has('danger'))
            <div class="alert alert-danger">
                {!! \Session::get('danger') !!}</li>
            </div>
        @endif
    	<div class="row">
    		<div class="col-sm-8 col-lg-9">
                <div class="card">
                    <h3 class="card-title">Activity Information</h3>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label>Activity Availability (Required)</label>
                            <input type="text" name="record[activity_availability]" value="{{ @$edit->activity_availability }}" placeholder="Activity Availability" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label>Activity Duration (Required)</label>
                            <input type="text" name="record[activity_duration]" value="{{ @$edit->activity_duration }}" placeholder="Activity Duration" class="form-control" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Activity Starting Time (Required)</label>
                          <input type="tel" name="record[activity_time]" value="{{ @$edit->activity_time }}" placeholder="Activity Starting Time" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label>Activity Title (Required)</label>
                            <input type="text" name="record[activity_title]" value="{{ @$edit->activity_title }}" placeholder="Activity Title" class="form-control" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Activity Slug (Required)</label>
                          {!! Form::text('record[activity_slug]',@$edit->activity_slug, array('class' => 'form-control','placeholder' => 'Activity Slug')) !!}
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Activity Price With Currency (Required)</label>
                          {!! Form::text('record[activity_price]',@$edit->activity_price, array('class' => 'form-control','placeholder' => 'Activity Price With Currency')) !!}
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Activity Why Should I Go For This? (Required)</label>
                          <textarea name="record[activity_why]"  placeholder="Activity Why Should I Go For This?" class="form-control editor" required>{{ @$edit->activity_why }}</textarea>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Activity Overview (Required)</label>
                          <textarea name="record[activity_overview]" placeholder="Activity Overview" class="form-control editor" required>{{ @$edit->activity_overview }}</textarea>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Activity Information (Required)</label>
                          <textarea name="record[activity_information]"  placeholder="Activity Important Information" class="form-control editor" required>{{ @$edit->activity_information }}</textarea>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Activity Inclusions (Required)</label>
                          <textarea name="record[activity_inclusions]" placeholder="Activity Inclusions" class="form-control editor" required>{{ @$edit->activity_inclusions }}</textarea>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Activity Advantage (Required)</label>
                          <textarea name="record[activity_advantage]"  placeholder="Activity Advantage" class="form-control editor" required>{{ @$edit->activity_advantage }}</textarea>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Activity Booking Cancellation Policy (Required)</label>
                          <textarea name="record[activity_booking_cancellation]" placeholder="Activity Booking Cancellation Policy" class="form-control editor" required>{{ @$edit->activity_booking_cancellation }}</textarea>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Activity Booking Child Policy (Required)</label>
                          <textarea name="record[activity_booking_child]" placeholder="Activity Booking Child Policy" class="form-control editor" required>{{ @$edit->activity_booking_child }}</textarea>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-lg-3">
              <div class="card">
                  <h3 class="card-title">Upload Image</h3>
                  <label class="upload_image">
                      <img src="{{ empty($edit->activity_image) ? url('imgs/no-image.png') : url("imgs/activities/".$edit->activity_image) }}" alt="Upload Image" title="Upload Image">
                      <input type="file" name="activity_image" accept="image/*" id="activityImage">
                  </label>
                  <label for="activityImage" class="btn btn-primary btn-block">Choose Image</label>
              </div>
              <div class="card">
                  <button type="submit" class="btn btn-block btn-primary">Submit</button>
              </div>
            </div>
        {{ Form::close() }}
    </div>
</div>
