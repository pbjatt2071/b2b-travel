<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-cart"></i> {{ !empty($edit->id) ? "Edit" : "Add" }} Airline</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ route('dashboard',$company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li><a href="{{ route('airline.index',$company) }}">Airline</a></li>
                    <li class="active">{{ !empty($edit->id) ? "Edit" : "Add" }} Airline</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ route('airline.index',$company) }}" class="btn btn-primary">View Airlines</a>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    {{ Form::open(["files" => true]) }}
    @if (\Session::has('danger'))
    <div class="alert alert-danger">
        {!! \Session::get('danger') !!}</li>
    </div>
    @endif
    <div class="row">
        <div class="col-sm-8 col-lg-9">
            <div class="card">
                <h3 class="card-title">Airline Information</h3>
                <div class="form-group">
                    <label>Airline Name (Required)</label>
                    <input type="text" name="record[name]" value="{{ @$edit->name }}" placeholder="Name" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <input name="record[description]" value="{{ @$edit->description }}" class="form-control" placeholder="Description">
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-lg-3">
            <div class="card">
                <h3 class="card-title">Upload Image</h3>
                <label class="upload_image">
                    <img src="{{ empty($edit->image) ? url('imgs/no-image.png') : url("imgs/airline/".$edit->image) }}" alt="Upload Image" title="Upload Image">
                    <input type="file" name="image" accept="image/*" id="itemImage">
                </label>
                <label for="itemImage" class="btn btn-primary btn-block">Choose Image</label>
            </div>
            <div class="card">
                <button type="submit" class="btn btn-block btn-primary">Submit</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>