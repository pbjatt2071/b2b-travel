<!-- Back Button Disble-->
<script type="text/javascript" language="javascript">
    function DisableBackButton() {
        window.history.forward()
    }
    DisableBackButton();
    window.onload = DisableBackButton;
    window.onpageshow = function(evt) {
        if (evt.persisted) DisableBackButton()
    }
    window.onunload = function() {
        void(0)
    }
</script>

<script>
    $(function() {
        "use strict";
        new WOW().init();
        $("#departure_date").datepicker();
        $("#return_date").datepicker();
        //$("#check_out").datepicker({ minDate: -0, maxDate: "+12M" });
        // $("#check_in").datepicker({ minDate: -0, maxDate: "+12M" });
    });
</script>




<script type="text/javascript">
    function PrintElem(elem) {
        Popup($(elem).html());
    }

    function Popup(data) {
        var mywindow = window.open('', 'ticket_show', 'height=400,width=600');
        mywindow.document.write('<html><head><title>Ticket Copy</title>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>



<!-- Track Online User 
<script type = "text/javascript">

    $(document).ready(function () {
        setHeartbeat();
    });


    function setHeartbeat() {
        setInterval('chatHeartbeatForOnlineUsers();', '30000');
    }

    function chatHeartbeatForOnlineUsers() {
        $.ajax({
            type: "POST",
            url: "OnlineUser.aspx/GetCurrentUser",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //success: OnSuccess,
            failure: function (response) {
                alert(response.d);
            }

        });
    }
</script> -->





<div class="container mt-3">
    <div class="prnt-btn mt-20">
        <input type="button" class="btn btn-primary" value="PRINT" onclick="PrintElem(&#39;#ticket_show&#39;)">
    </div>
</div>

<div class="srch-pnl mb-5">

    <div class="container" onload="window.print();">

        <!------flight info--------->

        <div id="ticket_show" class="tckt-show">
            <div> <img src="./Search _ AIRIQ_files/Spicejet.png" width="800" height="79" alt="Logo" style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0px; padding: 0px; border: 0px;">
                <p style="color:rgb(0,0,0);font-family:Arial,Helvetica,sans-serif;font-size:12px;margin:0px;padding:5px 0px 7px 18px;float:left;width:776px"><span style="margin:0px;padding:0px;float:left"><strong><br>Ticket is Non Refundable / Non Changeable / Non Cancellable.<br>&nbsp;</strong><br><strong><label style="color:red;">COVID-19 UPDATE : WEB-CHECKIN is mandatory to board flight with applicable charges. Kindly contact your travel agent for more information.</label> <br>&nbsp;</strong> </span><span style="margin:0px;padding:0px;float:right"><a href="mailto:info@airiq.com" target="_blank" style="color:rgb(0,0,0);margin:0px;padding:0px;outline:none;text-decoration:none"></a></span><br style="margin:0px;padding:0px"></p>
                <table width="798" cellpadding="0" cellspacing="0" border="1" summary="" style="color:rgb(0,0,0);margin:0px 0px 10px;padding:0px;font-size:14px;border:1px solid rgb(82,82,82);border-collapse:collapse;clear:both;empty-cells:show;font-weight:bold;font-family:Arial!important;background:rgb(208,212,237)">
                    <tbody style="margin:0px;padding:0px;font-size:12px;color:rgb(60,60,60);border:0px">
                        <tr style="margin:0px;padding:0px;border:0px">
                            <td width="30%" style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:5px 2px 5px 18px;color:rgb(0,0,0);border:1px solid rgb(0,0,0);font-family:Arial!important"><span style="margin:0px;padding:0px;float:left">Airline PNR Number :</span><span style="margin:0px;padding:0px 0px 0px 3px;float:left"><span>{{ $ticket->pnr_number }}</span></span></td>
                            <td width="25%" nowrap="" style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:5px 5px 5px 18px;color:rgb(0,0,0);border:1px solid rgb(0,0,0);font-family:Arial!important">Booked on :<span style="margin:0px;padding:0px 0px 0px 3px;text-transform:uppercase">{{ $ticket->created_at }}</span></td>
                            <td width="25%" nowrap="" style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:5px 5px 5px 18px;color:rgb(0,0,0);border:1px solid rgb(0,0,0);font-family:Arial!important">Status :<span style="margin:0px;padding:0px 0px 0px 3px">{{ $ticket->status }}</span></td>
                            <td width="20%" nowrap="" style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:5px 5px 5px 18px;color:rgb(0,0,0);border:1px solid rgb(0,0,0);font-family:Arial!important">Booking ID :<span style="margin:0px;padding:0px 0px 0px 3px">------</span></td>
                        </tr>
                    </tbody>
                </table>

                <table width="798" cellpadding="0" cellspacing="0" border="0" style="margin:0px 0px 10px;padding:6px 0px;color:rgb(60,60,60);border:1px solid rgb(0,0,0);border-collapse:collapse;clear:both;empty-cells:show;font-weight:bold;float:left;font-family:Arial!important">
                    <tbody style="margin:0px;padding:0px;border:0px">
                        <tr style="margin:0px;padding:0px;border:0px">
                            <td valign="top" width="100%" style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:3px 8px 3px 5px;border:0px;font-family:Arial!important">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0" summary="" style="margin:0px;padding:0px;border:0px;border-collapse:collapse;clear:both;empty-cells:show;font-weight:normal">
                                    <tbody style="margin:0px;padding:0px;border:0px">
                                        <tr style="margin:0px;padding:0px;border:0px">
                                            <td colspan="2" style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:3px 18px 10px;color:rgb(0,0,0);border:0px;font-weight:bold;font-color:red;font-family:Arial!important"> Passenger(s) Information</td>
                                        </tr>
                                        @foreach($ticket->passengers as $i => $p)
                                        <tr valign="top" style="margin:0px;padding:0px;border:0px">
                                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:3px 5px 2px 18px;border:0px;font-weight:bold;width:16px;font-family:Arial!important">{{ $i +1 }}.</td>
                                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:3px 5px 2px;border:0px;font-weight:bold;text-transform:uppercase;font-family:Arial!important"> <span style="font-family:arial,sans,sans-serif;color:rgb(34,34,34)">{{$p->title}} {{ $p->fname }} {{ $p->lname }}</span><br></td>
                                        </tr>
                                        @endforeach
                                        <tr valign="top" style="margin:0px;padding:0px;border:0px">
                                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:3px 5px 2px 18px;border:0px;font-weight:bold;width:16px;font-family:Arial!important"><br></td>
                                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:3px 5px 2px;border:0px;text-transform:uppercase;font-weight:bold;font-family:Arial!important"><br></td>
                                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:3px 5px;border:0px;font-family:Arial!important"><br></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table width="798" cellpadding="0" cellspacing="0" border="0" summary="" style="color:rgb(0,0,0);margin:0px 0px 10px;padding:0px;border:1px solid rgb(82,82,82);border-collapse:collapse;clear:both;empty-cells:show;font-weight:bold;text-transform:uppercase;font-size:12px!important;font-family:Arial!important;background:rgb(208,212,237)!important">
                    <tbody style="margin:0px;padding:0px;color:rgb(60,60,60);border:0px">
                        <tr style="margin:0px;padding:0px;border:0px">
                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:3px 3px 3px 18px;color:rgb(0,0,0);border:1px solid rgb(0,0,0);font-family:Arial!important">TRAVEL DATE </td>
                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:3px 3px 3px 18px;color:rgb(0,0,0);border:1px solid rgb(0,0,0);font-family:Arial!important">SECTOR</td>
                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:3px 3px 3px 18px;color:rgb(0,0,0);border:1px solid rgb(0,0,0);font-family:Arial!important">DEP TIME</td>
                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:5px 5px 5px 18px;color:rgb(0,0,0);border:1px solid rgb(0,0,0);font-family:Arial!important">ARR TIME</td>
                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:5px 5px 5px 18px;color:rgb(0,0,0);border:1px solid rgb(0,0,0);font-family:Arial!important">FLIGHT NO.</td>
                        </tr>
                        <tr style="margin:0px;padding:0px;border:0px">
                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:3px 3px 3px 18px;color:rgb(0,0,0);border:1px solid rgb(0,0,0);font-family:Arial!important">{{$ticket->tour->date}}</td>
                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:3px 3px 3px 18px;color:rgb(0,0,0);border:1px solid rgb(0,0,0);font-family:Arial!important">{{$ticket->tour->froms->name}} // {{$ticket->tour->tos->name}}</td>
                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:3px 3px 3px 18px;color:rgb(0,0,0);border:1px solid rgb(0,0,0);font-family:Arial!important">{{$ticket->tour->takeoff}}</td>
                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:3px 3px 3px 18px;color:rgb(0,0,0);border:1px solid rgb(0,0,0);font-family:Arial!important">{{$ticket->tour->landing}}</td>
                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:3px 3px 3px 18px;color:rgb(0,0,0);border:1px solid rgb(0,0,0);font-family:Arial!important">{{$ticket->tour->flight_number}} </td>
                        </tr>
                    </tbody>
                </table>
                <table width="798" cellpadding="0" cellspacing="0" border="0" style="font-size:12px;margin:0px;padding:0px;border:0px;border-collapse:collapse;clear:both;empty-cells:show;font-family:Arial!important">
                    <tbody style="margin:0px;padding:0px;border:0px">
                        <tr style="margin:0px;padding:0px;border:0px">
                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:5px 10px 0px 18px;font-size:8px;border:1px solid rgb(0,0,0);font-family:Arial!important">
                                <p style="margin:0px;padding:0px"><strong style="margin:0px;padding:0px;font-size:12px!important">Infant Information</strong></p>
                                <p style="margin:3px 0px 2px 4px;padding:0px;font-size:10px;font-weight:bold"></p>
                            </td>
                        </tr>
                    </tbody>
                </table><br>
                <table width="798" cellpadding="0" cellspacing="0" border="0" style="font-size:12px;margin:0px;padding:0px;border:0px;border-collapse:collapse;clear:both;empty-cells:show;font-family:Arial!important">
                    <tbody style="margin:0px;padding:0px;border:0px">
                        <tr style="margin:0px;padding:0px;border:0px">
                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:5px 10px 0px 18px;font-size:8px;border:1px solid rgb(0,0,0);font-family:Arial!important">
                                <p style="margin:0px;padding:0px"><strong style="margin:0px;padding:0px;font-size:12px!important">Contact Information</strong></p>
                                <p style="margin:3px 0px 2px 4px;padding:0px;font-size:10px;font-weight:bold">{{ $ticket->user->fname }} {{ $ticket->user->lname }}</p>
                                <p style="margin:3px 0px 2px 4px;padding:0px;font-size:10px;font-weight:bold">{{ $ticket->user->email }} {{ $ticket->user->mobile }}</p>
                            </td>
                        </tr>
                    </tbody>
                </table><br>
                <table width="798" cellpadding="0" cellspacing="0" border="0" style="font-size:12px;margin:0px;padding:0px;color:rgb(60,60,60);border:0px;border-collapse:collapse;clear:both;empty-cells:show;font-family:Arial!important">
                    <tbody style="margin:0px;padding:0px;border:0px">

                        <tr style="margin:0px;padding:0px;border:0px">

                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:5px 10px 0px 18px;font-size:10px;border:1px solid rgb(0,0,0);font-family:Arial!important">
                                <p style="margin:0px;padding:0px"><strong style="margin:0px;padding:0px;font-size:12px!important">Terms &amp; Conditions</strong></p>

                                <ol style="margin:0px;padding:0px 0px 0px 18px">

                                    <li style="margin:0px;padding:0px">
                                        <strong style="margin:0px;padding:0px">
                                            This group ticket is 100% Non Refundable, Non Changeable &amp; Non Cancellable.</strong>
                                    </li>
                                    <li style="margin:0px;padding:0px">
                                        <strong style="margin:0px;padding:0px">
                                            Charged fare is totally agreed between "BUYER &amp; SELLER", any issues related to fares thereafter will not be entertained. </strong>
                                    </li>

                                    <li style="margin:0px;padding:0px">
                                        <strong style="margin:0px;padding:0px">
                                            Check flight &amp; passenger(s) details directly by logging / calling to the respective airlines, any dispute with in 24 hours prior to departure will not be entertained.
                                        </strong>
                                    </li>

                                    <li style="margin:0px;padding:0px">
                                        <strong style="margin:0px;padding:0px">
                                            No updates will be shared from our end in respect to flight cancellation / changes in timings, "BUYER" had to check directly with the respective airlines before departure.
                                        </strong>
                                    </li>

                                    <li style="margin:0px;padding:0px">
                                        <strong style="margin:0px;padding:0px">
                                            Web checkin not allowed in this ticket.
                                        </strong>
                                    </li>


                                </ol>

                            </td>
                        </tr>
                    </tbody>
                </table>

                <table width="798" cellpadding="0" cellspacing="0" border="0" style="font-size:12px;margin:0px;padding:0px;color:rgb(60,60,60);border:0px;border-collapse:collapse;clear:both;empty-cells:show;font-family:Arial!important">
                    <tbody style="margin:0px;padding:0px;border:0px">
                        <tr style="margin:0px;padding:0px;border:0px">

                            <td style="margin-top:2px;margin-bottom:2px;margin-left:10px;padding:5px 10px 0px 18px;font-size:10px;border:1px solid rgb(0,0,0);font-family:Arial!important">

                                <p style="margin:0px;padding:0px"><strong style="margin:0px;padding:0px;font-size:12px!important">Important Information</strong></p>

                                <ol style="margin:0px;padding:0px 0px 0px 18px">

                                    <li style="margin:0px;padding:0px">
                                        <strong style="margin:0px;padding:0px">
                                            Valid Govt. ID proof required at the time of Boarding.</strong>
                                    </li>

                                    <li style="margin:0px;padding:0px">
                                        <strong style="margin:0px;padding:0px">
                                            Check-in counter opens 2 hours prior to scheduled departure time.</strong>
                                    </li>

                                    <li style="margin:0px;padding:0px">
                                        <strong style="margin:0px;padding:0px">
                                            Obtain your Boarding pass from check-in counters at least 45 minutes prior to scheduled departure time.</strong>
                                    </li>

                                    <li style="margin:0px;padding:0px">
                                        <strong style="margin:0px;padding:0px">
                                            Free Baggage allowance is 15 Kilo &amp; Hand Baggage allowance is 7 Kilo.<br>&nbsp;</strong>
                                    </li>



                                </ol>

                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <div class="srch-air">

    </div>



    <script language="javascript" type="text/javascript">
        function validate() {

            if (document.getElementById("footerID_amount_txt").value == "") {
                alert("Please enter the Amount.");
                document.getElementById("footerID_amount_txt").focus();
                return false;
            } else if (document.getElementById("footerID_ref_txt").value == "") {
                alert("Please enter the Reference No.");
                document.getElementById("footerID_ref_txt").focus();
                return false;
            } else if (document.getElementById("footerID_DropDownList1").value == "Select Payment Mode") {
                alert("Please select the Payment Mode.");
                document.getElementById("footerID_DropDownList1").focus();
                return false;
            }


            return true;
        }
    </script>

    <div class="modal fade" id="adreq" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div id="enquiry">
                    <div class="enquiry-bg">
                        <h2>Upload Request
                            <button type="button" class="close" data-dismiss="modal">×</button>
                        </h2>
                        <div class="enquiry">


                            <div method="post" action="" class="enquiry-form">
                                <div class="col-md-12">

                                    <input name="footerID$amount_txt" type="text" id="footerID_amount_txt" placeholder="Amount*">
                                </div>

                                <div class="col-md-12">
                                    <input name="footerID$ref_txt" type="text" id="footerID_ref_txt" placeholder="Reference No.*">
                                </div>

                                <div class="col-md-12">
                                    <select name="footerID$DropDownList1" id="footerID_DropDownList1">
                                        <option value="Select Payment Mode">Select Payment Mode</option>
                                        <option value="Bank Transfer">Bank Transfer</option>
                                        <option value="IMPS">IMPS</option>
                                        <option value="NEFT">NEFT</option>
                                        <option value="RTGS">RTGS</option>
                                        <option value="Cash Deposit">Cash Deposit</option>
                                        <option value="Credit Request">Credit Request</option>

                                    </select>
                                </div>

                                <div class="col-md-12">
                                    <input type="file" name="footerID$FileUpload1" id="footerID_FileUpload1">
                                </div>

                                <div class="col-md-12">
                                    <textarea name="footerID$msg_txt" id="footerID_msg_txt" type="text" placeholder="Message"></textarea>
                                </div>

                                <div class="col-md-12">
                                    <input type="submit" name="footerID$Button1" value="Submit" onclick="return validate();" id="footerID_Button1" class="pop-btn">
                                </div>
                            </div>
                        </div>

                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- add req -->



    <!-- Notification -->


    <script type="text/javascript">
        $(document).ready(function() {
            $("#notificationLink").click(function() {
                $("#notificationContainer").fadeToggle(300);
                $("#notification_count").fadeOut("slow");
                return false;
            });

            //open when load
            //        $(window).load(function () {
            //            $("#notificationContainer").fadeToggle(300);
            //            //$("#notification_count").fadeOut("slow");
            //            return false;
            //        });

            //Document Click
            $(document).click(function() {
                $("#notificationContainer").hide();
                $("#notification_count").fadeOut("slow");
            });
            //Popup Click
            $("#notificationContainer").click(function() {
                return false
            });

            //close when click on close button
            $("#closebtn").click(function() {
                $("#notificationContainer").hide();
                $("#notification_count").fadeOut("slow");
            });


            //close when click on close button/////////////////////////////////////
            $("#closebtn").click(function() {
                $("#navContainer").hide();
                $("#navalt_count").fadeOut("slow");
            });


            $("#navaltLink").click(function() {
                $("#navaltContainer").fadeToggle(300);
                $("#navalt_count").fadeOut("slow");
                return false;
            });
            //Document Click
            $(document).click(function() {
                $("#navaltContainer").hide();
                $("#navalt_count").fadeOut("slow");
            });
            //Popup Click
            $("#navaltContainer").click(function() {
                return true
            });

            //close when click on close button
            $("#closebtn").click(function() {
                $("#navaltContainer").hide();
                $("#navalt_count").fadeOut("slow");
            });
        });
    </script>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/5f42d0b61e7ade5df443413e/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->

    <!-- Track Online User -->
    <script type="text/javascript">
        $(document).ready(function() {
            setHeartbeat();
        });


        function setHeartbeat() {
            setInterval('chatHeartbeatForOnlineUsers();', '60000');
        }

        function chatHeartbeatForOnlineUsers() {
            $.ajax({
                type: "POST",
                url: "OnlineUser.aspx/GetCurrentUser",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                //success: OnSuccess,
                failure: function(response) {
                    alert(response.d);
                }

            });
        }
    </script>



    <script src="./Search _ AIRIQ_files/jQuery-2.1.4.min.js.download"></script>
    <script src="./Search _ AIRIQ_files/select2.full.min.js.download"></script>
    <script src="./Search _ AIRIQ_files/bootstrap.min.js.download"></script>
    <script src="./Search _ AIRIQ_files/jquery-ui.min.js.download"></script>
    <script src="./Search _ AIRIQ_files/wow.min.js.download"></script>
    <script src="./Search _ AIRIQ_files/js.js.download"></script>
</div>