<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-cart"></i> My Booking</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ route('dashboard',$company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li class="active">My Booking</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
    @endif
    <div class="card">
        {{ Form::open() }}
        <!-- <a href="#remove" class="float-right" data-toggle="tooltip" title="Remove Selected"> <i class="icon-trash-o"></i> </a> -->
        <h3 class="card-title">
            <div class="mr-auto"><i class="icon-cart"></i> My Booking</div>
        </h3>
        @if(!$records->isEmpty())
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 50px;">S.No.</th>
                        <th>PNR Number</th>
                        <th>Passenger</th>
                        <th>Price</th>
                        <th>Booking Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $sn = $records->firstItem();  @endphp
                    @foreach($records as $rec)
                    <tr>
                        <td>{{ $sn++ }}</td>
                        <td>{{ $rec->pnr_number ? $rec->pnr_number : 'N/A' }}</td>
                        <td>{{ $rec->passenger }}</td>
                        <td>{{ $rec->price }}</td>
                        <td>{{ $rec->created_at }}</td>
                        <td>{{ $rec->status }}</td>
                        <td>
                            <!-- <a class="btn btn-sm btn-primary"> -->
                            <a href="{{ route('bookticket.detail',['mahadev',$rec->id]) }}">
                                <i class="icon-eye"></i> Details
                            </a> &nbsp;
                            | &nbsp;
                            <a href="{{ route('bookticket.ticket',['mahadev',$rec->id]) }}">
                                <i class="icon-print"></i> Print
                            </a>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @php
        $get_param = request()->input();
        if(isset($get_param['page'])) {
        unset($get_param['page']);
        }
        @endphp
        {{ $records->appends($get_param)->links() }}
        @else
        <div class="no_records_found">
            No records found yet.
        </div>
        @endif
        {{ Form::close() }}
    </div>
</div>