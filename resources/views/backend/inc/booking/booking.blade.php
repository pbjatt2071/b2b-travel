<!---------------search panel----------->
<div class="">
	<!---------------search panel----------->
	<div class="air-list mt-4">
		<div class="container-fluid">
			<div class="card">
				<!-- <div class="row web-booking-head">
					<div class="col col-1">
						<h5>Airline</h5>
					</div>
					<div class="col col-2">
						<h5>Destination</h5>
					</div>
					<div class="col">
						<h5>Date</h5>
					</div>
					<div class="col">
						<h5>Time</h5>
					</div>
					<div class="col">
						<h5>Pax</h5>
					</div>
					<div class="col">
						<h5>Price</h5>
					</div>
				</div> -->

				<div class="row mt-2">
					<div class="col-md-2 col-6">
						<img src="{{ url('/imgs/airline/'.$edit->airlines->image) }}" alt="">
						<p class="mb-0">{{ $edit->airlines->name }}</p>
					</div>
					<div class="col-md-2 col-6 v-center">
						{{ $edit->froms->city->name }} <i class="icon-plane" aria-hidden="true"></i> {{ $edit->tos->city->name }}
					</div>
					<div class="col-md-2 col-6 v-center">
						{{ date('M d, Y', strtotime($edit->date)) }}
					</div>
					<div class="col-md-2 col-6 v-center">
						{{ date('h:i A', strtotime($edit->takeoff)) }}
						<!-- - {{ date('h:i A', strtotime($edit->landing)) }} -->
					</div>
					<div class="col-md-2 col-6 v-center">
						{{ $passenger }} Pass.
					</div>
					<div class="col-md-2 col-6 v-center">
						@if($edit->adult)
						{{ number_format($edit->new_adult) }} ₹
						@else
						{{ number_format($edit->new_adult) }} ₹
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="srch-pnl">
		<div class="container-fluid">
			{{ Form::open(["files" => true, 'url' => route('booking.store',[$company,$edit->id])]) }}
			<div class="card">
				<h6><i class="fa fa-address-card-o" aria-hidden="true"></i> Details entered below are exactly as per Traveller's ID</h6>
				<div class="srch-air pt-3" style="border-top: 1px solid #ced4da">
					@foreach($passArr as $arr)
					<div class="row adult-info">
						<div class="col-md-2">
							<label>Title</label>
							<div class="form-group">
								<select name="pass[{{ $arr }}][title]" class="form-control">
									<option value="Mr.">Mr.</option>
									<option value="Mrs.">Mrs.</option>
									<option value="Ms.">Ms.</option>
									<option value="Mstr.">Mstr.</option>
									<option value="Miss">Miss</option>
								</select>
							</div>
						</div>
						<div class="col-md-5">
							<label>First Name </label>
							<div class="form-group">
								<input name="pass[{{ $arr }}][fname]" type="text" class="form-control" placeholder="First Name" required="">
							</div>
						</div>
						<div class="col-md-5">
							<label>Last Name </label>
							<div class="form-group">
								<input name="pass[{{ $arr }}][lname]" type="text" id="PList_last_name_txt_0" class="form-control" placeholder="Last Name" required="">
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					@endforeach
				</div>
				<div class="clearfix"></div>
			</div>
			<br>
			<!------------------------->
			<div class="card">
				<input type="text" name="passenger" value="{{ $passenger }}" hidden>
				<input type="text" name="pnr_number" value="{{ $edit->pnr_number }}" hidden>
				<input type="text" name="price" value="{{ $edit->new_adult * $passenger }}" hidden>
				<h4><i class="fa fa-paper-plane-o" aria-hidden="true"></i> Infant Details</h4>
				@php
				$user = auth()->user();
				@endphp
				<div class="srch-air row">
					<div class="col-md-4">
						<label>Email ID</label>
						<div class="input-group">
							<div class="input-group">
								<input name="email" type="text" id="email_txt" class="form-control" placeholder="Email ID" required="" value="{{ $user->email }}">
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<label>Phone No. </label>
						<div class="input-group">
							<input name="phone" type="text" id="phone_txt" class="form-control" placeholder="Phone no." required="" value="{{ $user->mobile }}">
						</div>
					</div>
					<div class="col-md-4">
						<label>Internal Note</label>
						<div class="input-group">
							<input name="note" type="text" id="amount_txt" class="form-control" placeholder="Remarks/Note">
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<br>
			<!------------------------>
			<div class="card">
				<div class="srch-air">
					<div class="col-md-6">
						<div class="chck-box mb-3"><input id="CheckBox1" type="checkbox" checked name="CheckBox1" required> I have READ and AGREE to the <a href="#">Terms &amp; Conditions</a>
						</div>
						<input type="submit" name="Button1" value="Confirm Booking" id="Button1" class="btn2">
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>