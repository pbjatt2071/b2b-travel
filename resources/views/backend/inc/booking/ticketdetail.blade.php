<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-cart"></i> Ticket Details</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ route('dashboard',$company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li class="active">Ticket Details</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
    @endif
    <div class="card">
        <!-- <a href="#remove" class="float-right" data-toggle="tooltip" title="Remove Selected"> <i class="icon-trash-o"></i> </a> -->
        <h3 class="card-title mb-4">
            <div class="mr-auto"><i class="icon-cart"></i> Ticket Details</div>
        </h3>
        <div class="row">
            <div class="col-3 mb-3">
                <h6>PNR Number</h6>
                <div>{{ $record->pnr_number ? $record->pnr_number : 'N/A' }}</div>
            </div>
            <div class="col-3 mb-3">
                <h6>Booking Date</h6>
                <div>{{ $record->created_at ? $record->created_at : 'N/A' }}</div>
            </div>
            <div class="col-3 mb-3">
                <h6>Status</h6>
                <div>{{ $record->status ? $record->status : 'N/A' }}</div>
            </div>
            <div class="col-3 mb-3">
                <h6>Flight Number</h6>
                <div>{{ $record->tour ? $record->tour->flight_number : 'N/A' }}</div>
            </div>
            <div class="col-3 mb-3">
                <h6>From</h6>
                <div>{{ $record->tour ? $record->tour->froms->name : 'N/A' }} ({{ $record->tour->froms->city->name }})</div>
            </div>
            <div class="col-3 mb-3">
                <h6>To</h6>
                <div>{{ $record->tour ? $record->tour->tos->name : 'N/A' }} ({{ $record->tour->froms->city->name }})</div>
            </div>
            <div class="col-3 mb-3">
                <h6>DEP Time</h6>
                <div>{{ $record->tour ? $record->tour->takeoff : 'N/A' }}</div>
            </div>
            <div class="col-3 mb-3">
                <h6>ARR Time</h6>
                <div>{{ $record->tour ? $record->tour->landing : 'N/A' }}</div>
            </div>
        </div>
        <h3 class="card-title mt-4 mb-4">
            <div class="mr-auto"><i class="icon-cart"></i> Passenger Details</div>
        </h3>
        <div class="row">
            @foreach($record->passengers as $pass)
            <div class="col-3 mb-3">
                <h6>Name</h6>
                <div>{{ $pass->title }} {{ $pass->fname }} {{ $pass->lname }}</div>
            </div>
            @endforeach
        </div>
        <h3 class="card-title  mt-4 mb-4">
            <div class="mr-auto"><i class="icon-cart"></i> Infant Details</div>
        </h3>
        <div class="row">
            <div class="col-6 mb-3">
                <h6>Email</h6>
                <div>{{ $record->user ? $record->user->email : 'N/A' }}</div>
            </div>
            <div class="col-6 mb-3">
                <h6>Mobile</h6>
                <div>{{ $record->user ? $record->user->mobile : 'N/A' }}</div>
            </div>
            <div class="col-12 mb-3">
                <h6>Note</h6>
                <div>{{ $record->note ? $record->note : 'N/A' }}</div>
            </div>
        </div>
    </div>
</div>