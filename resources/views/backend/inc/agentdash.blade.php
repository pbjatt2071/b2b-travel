<!---------------search panel----------->
<div class="">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card mt-4 p-0">
					<ul class="resp-tabs-list">
						<li class="active">Oneway</li>
						<li>Round Trip</li>
					</ul>
					<div class="resp-tabs-container active">
						<div>
							<div class="srch-box wow  fadeIn animated">
								<form id="searchData" enctype="multipart/form-data" autocomplete="off">
									<div class="srch-air row m-0 p-0">
										<div class="col-md-12">
											<span id="show_lbl" style="color:Yellow;"></span>
										</div>
										<div class="col-md-5">
											<div class="row">
												<div class="col-md-6">
													<label> From </label>
													<div class="form-group">
														{{ Form::select('from', $airportArr, '', ['class' => 'form-control select2','id'=>'from','required'=>'required']) }}
													</div>
												</div>
												<div class="col-md-6">
													<label> To </label>
													<div class="form-group">
														{{ Form::select('to', $airportArr, '', ['class' => 'form-control select2','id'=>'to','required'=>'required']) }}
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<label>Travel Date </label>
											<div class="form-group">
												{{ Form::date('date', @$edit->date, ['class' => 'form-control','id'=>'check_out','placeholder' => 'DD/MM/YYYY','autocomplete'=>'off','required'=>'required']) }}
											</div>
										</div>
										<div class="col-md-1">
											<label>Passanger</label>
											@php
											$pass = [
											'1' => '1',
											'2' => '2',
											'3' => '3',
											'4' => '4',
											'5' => '5',
											'6' => '6',
											]
											@endphp
											<div class="form-group">
												{{ Form::select('passenger', $pass, '', ['class' => 'form-control','id'=>'passenger','required'=>'required']) }}
											</div>
										</div>
										<div class="col-md-2">
											<label>Airline</label>
											<div class="form-group">
												{{ Form::select('airline', $airlineArr, '', ['class' => 'form-control select2','id'=>'airline']) }}
												<!-- <option value="All Airlines">All Airlines</option>
												<option value="1">IndiGo</option>
												<option value="2">SpiceJet</option>
												<option value="3">GoAir</option>
												<option value="4">Jet Airways</option>
												<option value="5">Vistara</option>
												<option value="6">Air-Asia</option>
												<option value="11">Air India</option> -->
												</select>
											</div>
										</div>
										<div class="col-md-2">
											<label></label>
											<div class="form-group mt-2">
												<button id="SearchBtn" class="btn w-100" style="background-color: #fff300;">Search <i class="fa fa-search" aria-hidden="true"></i></button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!---------------search panel----------->
	<div class="air-list">
		<div class="container-fluid">
			@if(count($tours) != 0)
			@foreach($tours as $tour)
			<div class="card m">
				<div class="row">
					<div class="col-md-1 col-6">
						<img src="{{ url('/imgs/airline/'.$tour->airlines->image) }}" alt="">
						<p class="mb-0">{{ $tour->airlines->name }}</p>
					</div>
					<div class="col-md-2 col-6 v-center">
						{{ $tour->froms->city->name }} <i class="icon-plane" aria-hidden="true"></i> {{ $tour->tos->city->name }}
					</div>
					<div class="col-md-2 col-6 v-center">
						{{ date('M d, Y', strtotime($tour->date)) }}
					</div>
					<div class="col-md-2 col-6 v-center">
						{{ date('h:i A', strtotime($tour->takeoff)) }} 
						<!-- - {{ date('h:i A', strtotime($tour->landing)) }} -->
					</div>
					<div class="col-md-2 col-6 v-center">
						{{ $tour->seat }} Seat
					</div>
					<div class="col-md-1 col-6 v-center">
						@if($tour->adult)
						{{ number_format($tour->new_adult) }} ₹
						@else
						{{ number_format($tour->new_adult) }} ₹
						@endif
					</div>
					<div class="col-md-2 col-12 v-center">
						@if($tour->seat >= $edit['passenger'])
						<a href="{{ url($company.'/booking/'.$tour->id.'?passenger='.$edit['passenger']) }}">
						<button id="SearchBtn" class="btn w-100" style="background-color: #e83232; color: white;">Book Now</button></a>
						@else
						<button id="SearchBtn" class="btn w-100" style="background-color: #e83232; color: white;" disabled>Book Now</button>
						@endif
					</div>
				</div>
			</div>
			@endforeach
			@endif
		</div>
	</div>

	<!-- <div class="banr-in">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<img id="Image1" class="img-responsive" src="https://airiq.in/img/offer/2.jpg?r=XOnqb8yrakqUL/nrBhOasQ" />
				</div>
			</div>
		</div>
	</div> -->
</div>