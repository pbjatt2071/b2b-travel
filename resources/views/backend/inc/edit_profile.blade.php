<section class="page-header mb-3">
    <div class="container-fluid">
        <h1><i class="icon-pencil1"></i> Edit Profile</h1>
        <ul class="page-breadcrumb">
            <li><a href="{{ url($company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
            <li class="active">Edit Profile</li>
        </ul>
    </div>
</section>
<div class="container-fluid">
    {{ Form::open(['files' => true]) }}

        @if (\Session::has('success'))
        <div class="alert alert-success">
            {!! \Session::get('success') !!}
        </div>
        @endif
        @if (\Session::has('danger'))
        <div class="alert alert-danger">
            {!! \Session::get('danger') !!}
        </div>
        @endif
        <div class="row">
            <div class="col-sm-8 col-lg-9">
                <div class="card">
                    <h3 class="card-title">
                        <i class="icon-pencil1"></i> Edit Profile
                    </h3>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" value="{{ $profile->user_login }}" class="form-control" readonly>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>First Name (Required)</label>
                                <input type="text" name="record[user_fname]" value="{{ $profile->user_fname }}" class="form-control name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="record[user_lname]" value="{{ $profile->user_lname }}" class="form-control name">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Email ID</label>
                        <input type="email" name="record[user_email]" placeholder="Enter Your Email ID" value="{{ $profile->user_email }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Mobile No.</label>
                        <input type="tel" name="record[user_mobile]" placeholder="Enter 10 Digit Mobile No." maxlength="10" value="{{ $profile->user_mobile }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-lg-3">
                <div class="card">
                    <h3 class="card-title">Upload Photo</h3>
                    <label class="upload_image">
                        <img src="{{ empty($profile->user_image) ? url('imgs/no-image.png') : url("imgs/users/{$company}/{$profile->user_image}") }}" alt="Upload Image" title="Upload Image">
                        <input type="file" name="user_image" accept="image/*" id="userImage">
                    </label>
                    <label for="userImage" class="btn btn-primary btn-block">Choose Image</label>
                </div>
            </div>
        </div>
    {{ Form::close() }}
</div>
