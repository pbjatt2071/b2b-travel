<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-cart"></i> {{ !empty($edit->id) ? "Edit" : "Add" }} {{ $role }}</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ route('dashboard',$company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li><a href="{{ route('user.index',[$company,$role]) }}">{{ $role }}</a></li>
                    <li class="active">{{ !empty($edit->id) ? "Edit" : "Add" }} {{ $role }}</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ route('user.index',[$company,$role]) }}" class="btn btn-primary">View {{ $role }}s</a>
            </div>
        </div>
    </div>
</section>
<div>
    <div class="card">
        <form method="post" enctype="multipart/form-data" autocomplete="off">
            <input autocomplete="off" name="hidden" type="search" style="display:none;">
            @csrf
            @if (\Session::has('danger'))
            <div class="alert alert-danger">
                {!! \Session::get('danger') !!}</li>
            </div>
            @endif
            <h3 class="card-title">
                <div class="mr-auto">Login Details</div>
            </h3>
            <div class="row">
                <div class="col-sm-8 col-lg-9">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>First Name (Required)</label>
                                <input type="text" name="user[fname]" value="{{ @$edit->fname }}" placeholder="First Name" class="form-control" required autocomplete="new_name">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" name="user[lname]" value="{{ @$edit->lname }}" placeholder="Last Name" class="form-control" autocomplete="new_name">

                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Password</label>
                                @if(@$edit->password)
                                <input type="password" name="user[password]" placeholder="Password" class="form-control" autocomplete="">
                                @else
                                <input type="password" name="user[password]" placeholder="Password" class="form-control" required autocomplete="">
                                @endif
                            </div>
                        </div>
                        <!-- <div class="col-sm-4">
                            <div class="form-group">
                                <label>Date Of Birth</label>
                                <input type="text" name="user[user_dob]" value="{{ @$edit->user_dob }}" class="form-control datepicker_dob" placeholder="yyyy-mm-dd" readonly>
                            </div>
                        </div> -->
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Login (Required)</label>
                                <input type="text" name="user[login]" value="{{ @$edit->login }}" placeholder="Login" class="form-control" required autocomplete="new_login">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Mobile No. (Required)</label>
                                <input type="tel" name="user[mobile]" value="{{ @$edit->mobile }}" placeholder="Mobile No." class="form-control" required autocomplete="new_mobile">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Email ID</label>
                                <input type="email" name="user[email]" value="{{ @$edit->email }}" placeholder="Email ID" class="form-control" autocomplete="new_email">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Address Line 1 (Required)</label>
                        <input type="text" maxlength="100" name="userdetail[address1]" value="{{ @$edit1->address1 }}" class="form-control" placeholder="Address Line 1" required>
                    </div>
                    <div class="form-group">
                        <label>Address Line 2</label>
                        <input type="text" maxlength="100" name="userdetail[address2]" value="{{ @$edit1->address2 }}" class="form-control" placeholder="Address Line 2">
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Country (Required)</label>
                                <select class="form-control country" name="userdetail[country_id]" data-target="#userState" required>
                                    <option value="">Select Country</option>
                                    @if(!$countries->isEmpty())
                                    @foreach($countries as $c)
                                    <option value="{{ $c->id }}" @if($c->id == @$edit1->country_id) selected @endif>{{ $c->name.' ('.$c->short_name.')' }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>State (Required)</label>
                                <select class="form-control state" name="userdetail[state_id]" data-target="#userCity" id="userState" required>
                                    <option value="">Select State</option>
                                    @if(!empty($states) && !$states->isEmpty())
                                    @foreach($states as $s)
                                    <option value="{{ $s->id }}" @if($s->id == @$edit1->state_id) selected @endif>{{ $s->name.' ('.$s->short_name.')' }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>City</label>
                                <select class="form-control city" data-target="#userPincode" name="userdetail[city_id]" id="userCity">
                                    <option value="">Select City</option>
                                    @if(!empty($cities) && !$cities->isEmpty())
                                    @foreach($cities as $c)
                                    <option value="{{ $c->id }}" @if($c->id == @$edit1->city_id) selected @endif>{{ $c->name.' ('.$c->short_name.')' }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Pincode</label>
                                <select class="form-control" id="userPincode" name="userdetail[pincode_id]">
                                    <option value="">Select Pincode</option>
                                    @if(!empty($pincodes) && !$pincodes->isEmpty())
                                    @foreach($pincodes as $p)
                                    <option value="{{ $p->id }}" @if($p->id == @$edit1->pincode_id) selected @endif>{{ $p->code }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Company Name</label>
                                <input type="text" name="userdetail[company_name]" value="{{ @$edit1->company_name }}" placeholder="Company Name" class="form-control" autocomplete="new_company">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Pan Number</label>
                                <input type="text" name="userdetail[pan_number]" value="{{ @$edit1->pan_number }}" placeholder="Pan Number" class="form-control" autocomplete="new_pan_number">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Gst Number</label>
                                <input type="text" name="userdetail[gst]" value="{{ @$edit1->gst }}" placeholder="Gst Number" class="form-control" autocomplete="new_gst">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Bio</label>
                        <textarea name="userdetail[bio]" rows="4" class="form-control" placeholder="Remarks">{{ @$edit1->bio }}</textarea>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-3">
                    <label class="upload_image">
                        <img src="{{ empty($edit->image) ? url('imgs/no-image.png') : url('imgs/users/'.$edit1->image) }}" alt="Upload Image" title="Upload Image">
                        <input type="file" name="image" accept="image/*" id="userImage">
                    </label>
                    <label for="userImage" class="btn btn-primary btn-block">Choose File</label>

                    <div class="card">
                        <button type="submit" class="btn btn-block btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>