<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-globe"></i> Country</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ url($company_info->company_sitename) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li class="active">Country</li>
                </ul>
            </div>
            <div class="float-right">
                <!-- {{ Form::open(['url' => route('country.importcsv',$company), 'files' => true]) }}
                <label class="btn btn-primary import_csv">
                    Import CSV
                    <input name="import_file" type="file" accept=".csv">
                </label> -->
                <a href="{{ route('country.exportcsv',$company) }}" class="btn btn-primary">Export CSV</a>
                <!-- {{ Form::close() }} -->
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
    @endif
    <div class="row">
        <div class="col-sm-4">
        	<div class="card">
                @if (\Session::has('danger'))
                <div class="alert alert-danger">
                    {!! \Session::get('danger') !!}
                </div>
                @endif
        	     <form method="post">
                     @csrf
              		 <h3 class="card-title"> <i class="icon-globe"></i> {{ !empty($edit->id) ? "Edit" : "Add" }} Country</h3>
                     <div class="form-group">
                         <label>Country Name (Required)</label>
                         <input type="text" name="record[name]" value="{{ @$edit->name }}" class="form-control" required>
                     </div>
                     <div class="form-group">
                         <label>Country Short Name (Required)</label>
                         <input type="text" name="record[short_name]" value="{{ @$edit->short_name }}" class="form-control" required>
                     </div>
                     <div class="form-group">
                         <label>Country Code (Required)</label>
                         <input type="text" name="record[code]" value="{{ @$edit->code }}" class="form-control" placeholder="Like +1,+91 etc." required>
                     </div>
                     <div class="form-group">
                         <button type="submit" class="btn btn-block btn-primary">Submit</button>
                     </div>
        		 </form>
        	</div>
        </div>
        <div class="col-sm-8">
            <div class="card">
                <h3 class="card-title"><i class="icon-filter1"></i> Filter By</h3>
                {{ Form::open(['method' => 'GET']) }}
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input type="text" name="search[keyword]" value="{{ @$search['keyword'] }}" class="form-control" placeholder="By keywords">
                        </div>
                    </div>
                    <div class="col-sm-3 col-xl-2">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block"><i class="icon-search"></i> Search</button>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        	<div class="card">
                <form method="post">
                    @csrf
                    <a href="#remove" class="float-right" data-toggle="tooltip" title="Remove Selected"> <i class="icon-trash-o"></i> </a>
            		<h3 class="card-title">
                        <div class="mr-auto"><i class="icon-globe"></i> View Country</div>
            		</h3>
        		    @if(!$records->isEmpty())
        		    <div class="table-responsive">
        			    <table class="table table-bordered">
        			          <thead>
        			               <tr>
        			                    <th style="width: 50px;">
        			                        <label class="animated-checkbox">
        			                            <input type="checkbox" class="checkall">
        			                            <span class="label-text"></span>
        			                        </label>
        			                    </th>
        			                   <th style="width: 50px;">S.No.</th>
                                       <th>Country Name</th>
        			                   <th>Country Code</th>
        			                   <th>Country Short Name</th>
        			               </tr>
        			          </thead>

        			          <tbody>
        			          		@php $sn = $records->firstItem(); @endphp
        			          		@foreach($records as $rec)
        			               	<tr>
        		                        <td>
        		                            <label class="animated-checkbox">
        		                                <input type="checkbox" name="check[]" value="{{ $rec->id  }}" class="check @if($rec->states_count) disabled @endif" @if($rec->states_count) disabled @endif>
        		                                <span class="label-text"></span>
        		                            </label>
        		                        </td>
        								<td>{{ $sn++ }}</td>
                                        <td>
                                            <a href="{{ route('country.edit',[$company,$rec->id]) }}" class="pencil">
                                                <i class="icon-pencil" title="Edit"></i> {{ $rec->name }}
                                            </a>
                                            ({{ $rec->states_count }} States)
                                        </td>
        								<td>{{ $rec->code }}</td>
        								<td>{{ $rec->short_name }}</td>
        			               	</tr>
        			               @endforeach
        			          </tbody>
        			    </table>
        			</div>
                    @php
                        $get_param = request()->input();
                        if(isset($get_param['page'])) {
                            unset($get_param['page']);
                        }
                    @endphp
                    {{ $records->appends($get_param)->links() }}
        		    @else
        		    <div class="no_records_found">
        		      No records found yet.
        		    </div>
        			@endif
        		</form>
        	</div>
        </div>
    </div>

</div>
