<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-cart"></i> {{ !empty($edit->id) ? "Edit" : "Add" }} Tour</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ route('dashboard',$company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li><a href="{{ route('tour.index',$company) }}">Tour</a></li>
                    <li class="active">{{ !empty($edit->id) ? "Edit" : "Add" }} Tour</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ route('tour.index',$company) }}" class="btn btn-primary">View Tours</a>
            </div>
        </div>
    </div>
</section>
<div>
    <div class="card">
        <form method="post" enctype="multipart/form-data" autocomplete="off">
            <input autocomplete="off" name="hidden" type="search" style="display:none;">
            @csrf
            @if (\Session::has('danger'))
            <div class="alert alert-danger">
                {!! \Session::get('danger') !!}</li>
            </div>
            @endif
            <h3 class="card-title">
                <div class="mr-auto">Tour Details</div>
            </h3>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>From (Required)</label>
                        <select class="form-control" name="record[from]" required>
                            <option value="">Select starting point</option>
                            @if(!$airports->isEmpty())
                            @foreach($airports as $c)
                            <option value="{{ $c->id }}" @if($c->id == @$edit1->from) selected @endif>{{ $c->name.' ('.$c->city->name.')' }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>To (Required)</label>
                        <select class="form-control" name="record[to]" required>
                            <option value="">Select departing point</option>
                            @if(!$airports->isEmpty())
                            @foreach($airports as $c)
                            <option value="{{ $c->id }}" @if($c->id == @$edit1->to) selected @endif>{{ $c->name.' ('.$c->city->name.')' }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Airline (Required)</label>
                        <select class="form-control" name="record[airline_id]" required>
                            <option value="">Select airline</option>
                            @if(!$airlines->isEmpty())
                            @foreach($airlines as $airline)
                            <option value="{{ $airline->id }}" @if($airline->id == @$edit1->airline_id) selected @endif>{{ $airline->name }}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Seat (Required)</label>
                        <input type="number" name="record[seat]" value="{{ @$edit->seat }}" placeholder="Enter seat" class="form-control" required autocomplete="new_mobile">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Class </label>
                        <select class="form-control" name="record[class]" required>
                            <option value="">Select class</option>
                            @foreach($classes as $i => $c)
                            <option value="{{ $i }}" @if($i==@$edit1->class) selected @endif>{{ $c }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Disable Before (min 1 & max 72 hour)</label>
                        <input type="number" name="record[before_disable]" value="{{ @$edit->before_disable }}" min="1" max="72" placeholder="Before disable" class="form-control" required>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Adult Price</label>
                        <input type="number" name="record[adult]" value="{{ @$edit->adult }}" placeholder="Adult price" class="form-control" required>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Child Price</label>
                        <input type="number" name="record[child]" value="{{ @$edit->child }}" placeholder="Child price" class="form-control" required>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Infant Price</label>
                        <input type="number" name="record[infants]" value="{{ @$edit->infant }}" placeholder="Infant price" class="form-control" required>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Flight Number</label>
                        <input type="text" name="record[flight_number]" value="{{ @$edit->flight_number }}" placeholder="Flight number" class="form-control" required>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>PNR Number</label>
                        <input type="text" name="record[pnr_number]" value="{{ @$edit->pnr_number }}" placeholder="PNR number" class="form-control">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Arival Time</label>
                        <input type="time" name="record[takeoff]" value="{{ @$edit->takeoff }}" placeholder="Arival time" class="form-control " required>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Departure Time</label>
                        <input type="time" name="record[landing]" value="{{ @$edit->landing }}" placeholder="Departure time" class="form-control " required>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Date (Required)</label>
                        <input type="text" name="date" value="" class="form-control daterangepicker" placeholder="yyyy-mm-dd" required>
                        <input type="text" name="startdate" id="startdate" required hidden>
                        <input type="text" name="enddate" id="enddate" required hidden>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-9">
                </div>
                <div class="col-sm-3">
                    <div class=" text-right">
                        <button type="submit" class="btn btn-block btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>