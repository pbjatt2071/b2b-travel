<section class="page-header mb-3">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="float-left">
                <h1><i class="icon-cart"></i> View Tour</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ route('dashboard',$company) }}"><i class="icon-dashboard"></i> Dashboard</a></li>
                    <li class="active">View Tour</li>
                </ul>
            </div>
            <div class="float-right">
                <a href="{{ route('tour.add',$company) }}" class="btn btn-primary">Add New</a>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid">
    @if (\Session::has('success'))
    <div class="alert alert-success">
        {!! \Session::get('success') !!}
    </div>
    @endif
    <div class="card">
        <h3 class="card-title"><i class="icon-filter1"></i> Filter By</h3>
        {{ Form::open(['method' => 'GET']) }}
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group">
                    <select name="record[airline]" class="form-control" id="searchCAirline">
                        <option value="">Select Airline</option>
                        @foreach($airlines as $st)
                        <option value="{{ $st->id }}" @if(@$record['airline']==$st->id) selected @endif>{{ $st->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <select name="record[airport]" class="form-control" id="searchCAirport">
                        <option value="">Select Airport</option>
                        @foreach($airports as $st)
                        <option value="{{ $st->id }}" @if(@$record['airport']==$st->id) selected @endif>{{ $st->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <select name="record[class]" class="form-control" id="searchCClass">
                        <option value="">Select Class</option>
                        @foreach($classes as $i => $st)
                        <option value="{{ $i }}" @if(@$record['class']==$i) selected @endif>{{ $st }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @if(@auth()->user()->role_id != 4)
            <div class="col-sm-2">
                <div class="form-group">
                    <select name="record[user_id]" class="form-control" id="searchCUsers">
                        <option value="">Select Users</option>
                        @foreach($users as $st)
                        <option value="{{ $st->id }}" @if(@$record['user_id']==$st->id) selected @endif>{{ $st->fname }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @endif
            <!-- <div class="col-sm-2">
                <div class="form-group">
                    <input type="text" name="search[keyword]" value="{{ @$search['keyword'] }}" class="form-control" placeholder="By keywords">
                </div>
            </div> -->
            <div class="col-sm-2">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block"><i class="icon-search"></i> Search</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
    <div class="card">
        {{ Form::open() }}
        <a href="#remove" class="float-right" data-toggle="tooltip" title="Remove Selected"> <i class="icon-trash-o"></i> </a>
        <h3 class="card-title">
            <div class="mr-auto"><i class="icon-cart"></i> View Tours</div>
        </h3>
        @if(!$records->isEmpty())
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 50px;">
                            <label class="animated-checkbox">
                                <input type="checkbox" class="checkall">
                                <span class="label-text"></span>
                            </label>
                        </th>
                        <th style="width: 50px;">S.No.</th>
                        <th>From</th>
                        <th>To</th>
                        <th>User</th>
                        <th>Airline</th>
                        <th>Date</th>
                        <th>Seat</th>
                        <th>Price</th>
                        @if(@auth()->user()->role_id == 1)
                        <th>Price (for sell)</th>
                        @endif
                        <th>Flight</th>
                        <!-- <th>DOB</th> -->
                        <th>Status</th>
                    </tr>
                </thead>

                <tbody>
                    @php $sn = $records->firstItem(); @endphp
                    @foreach($records as $rec)
                    @php
                    $image = !empty( $rec->image ) ? url( "/imgs/profile/".$rec->image ) : url( 'imgs/no-image.png' );
                    @endphp
                    <tr>
                        <td>
                            <label class="animated-checkbox">
                                <input type="checkbox" name="check[]" value="{{ $rec->id  }}" class="check">
                                <span class="label-text"></span>
                            </label>
                        </td>
                        <td>{{ $sn++ }} | <a href="{{ route('tour.edit',[$company,$rec->id]) }}" class="pencil">
                                <i class="icon-pencil" title="Edit"></i>
                            </a></td>
                        <!-- <td> <img src="{{ $image }}" alt="{{ $rec->name }}" title="{{ $rec->name }}" class="table-img"> </td> -->
                        <!-- <td>
                            <a href="{{ route('tour.edit',[$company,$rec->id]) }}" class="pencil">
                                <i class="icon-pencil" title="Edit"></i> {{ $rec->froms->name }} ({{ $rec->froms->city->name }})
                            </a>
                        </td> -->
                        <td>
                            {{ $rec->froms->name }} <br>
                            <small>({{ $rec->froms->city->name }})</small>
                        </td>
                        <td>
                            {{ $rec->tos->name }} <br>
                            <small>({{ $rec->tos->city->name }})</small>
                        </td>
                        <td>{{ $rec->user->fname }} {{ $rec->user->lname }}</td>
                        <td>{{ $rec->airlines->name }}</td>
                        <td>{{ date('M d, Y', strtotime($rec->date)) }} <br>
                            <small>({{ date('h:i a', strtotime($rec->takeoff)) }} - {{ date('h:i a', strtotime($rec->landing)) }})</small>
                        </td>
                        <td>{{ $rec->seat }}</td>
                        <td>
                            <small>Adult : {{ number_format($rec->adult) }} ₹</small><br>
                            <small>Child : {{ number_format($rec->child) }} ₹</small><br>
                            <small>Infant : {{ number_format($rec->infants) }} ₹</small>
                        </td>
                        @if(@auth()->user()->role_id == 1)
                        <td>
                            <small>Adult : {{ number_format($rec->new_adult) }} ₹</small><br>
                            <small>Child : {{ number_format($rec->new_child) }} ₹</small><br>
                            <small>Infant : {{ number_format($rec->new_infants) }} ₹</small>
                        </td>
                        @endif
                        <td> <small>Flight : {{ $rec->flight_number }}</small> <br>
                            <small>Class : {{ $rec->class }}</small><br>
                            <small>PNR : {{ $rec->pnr_number }}</small>
                        </td>
                        <!-- <td>{{ date("d/m/Y", strtotime($rec->dob)) }}</td> -->
                        <td>
                            <select class="form-control" onchange="window.location = '{{ url($company."/tour/status/".$rec->id.'/') }}/'+this.value">
                                <option value="true" @if($rec->active == "true") selected @endif>Active</option>
                                <option value="false" @if($rec->active == "false") selected @endif>Inactive</option>
                            </select>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @php
        $get_param = request()->input();
        if(isset($get_param['page'])) {
        unset($get_param['page']);
        }
        @endphp
        {{ $records->appends($get_param)->links() }}
        @else
        <div class="no_records_found">
            No records found yet.
        </div>
        @endif
        {{ Form::close() }}
    </div>
</div>