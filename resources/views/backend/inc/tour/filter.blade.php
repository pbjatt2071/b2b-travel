<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Search | AIRIQ</title>
    <!--css-->
    <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/animate.min.css">
    <link rel="stylesheet" href="../css/style.css?parameter=3">
    <link href="../css/des.css" rel="stylesheet" />
    <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">

    <!-- Datepicker New Css -->
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <!-- Tab New Css -->
    <link rel="stylesheet" type="text/css" href="../css/tabs.css">

    <script src="../js/jquery.min.js"></script>

    <!-- Back Button Disble-->
    <script type="text/javascript" language="javascript">
        function DisableBackButton() {
            window.history.forward()
        }
        DisableBackButton();
        window.onload = DisableBackButton;
        window.onpageshow = function(evt) {
            if (evt.persisted) DisableBackButton()
        }
        window.onunload = function() {
            void(0)
        }
    </script>

    <script>
        $(function() {
            "use strict";
            new WOW().init();
            $("#departure_date").datepicker();
            $("#return_date").datepicker();
            //$("#check_out").datepicker({ minDate: -0, maxDate: "+12M" });
            // $("#check_in").datepicker({ minDate: -0, maxDate: "+12M" });
        });
    </script>




</head>

<header class="top-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="logo"><a href="Search.aspx"><img src="../img/logo.png" alt="" class="valign logo-login"></a></div>
                <div class="admn">
                    <!-----navaltcon--------->
                    <div class="navaltcon">
                        <ul>
                            <li id="navalt_li"><a title="Agent Airiq" href="#" id="navaltLink"><i class="fa fa-user-circle" aria-hidden="true"></i></a>
                                <div id="navaltContainer">
                                    <div id="navaltTitle"> <span id="menuID_user_info_sp">GANESH TRAVELS | 3355</span> </div>
                                    <div id="navaltsBody" class="notifications">
                                        <a href="soldticket.aspx">
                                            <div class="navaltcon-box">
                                                <img src="../img/icons/Sales_Record.png">
                                                <h2>Sales Record</h2>
                                            </div>
                                        </a>
                                        <a href="refundticket.aspx">
                                            <div class="navaltcon-box">
                                                <img src="../img/icons/Refunds_Record.png">
                                                <h2>Refunds Record</h2>
                                            </div>
                                        </a>
                                        <a href="account.aspx">
                                            <div class="navaltcon-box">
                                                <img src="../img/icons/Accounts_Ledger.png">
                                                <h2>Accounts Ledger</h2>
                                            </div>
                                        </a>
                                        <a href="#" data-toggle="modal" data-target="#adreq">
                                            <div class="navaltcon-box">
                                                <img src="../img/icons/Upload_Request.png">
                                                <h2>Upload Request</h2>
                                            </div>
                                        </a>
                                        <a href="TempCreditRep.aspx">
                                            <div class="navaltcon-box">
                                                <img src="../img/icons/Temporary_Credit.png">
                                                <h2>Temporary Credit</h2>
                                            </div>
                                        </a>
                                        <a href="bank_details.aspx">
                                            <div class="navaltcon-box">
                                                <img src="../img/icons/Bank_Details.png">
                                                <h2>Bank Details</h2>
                                            </div>
                                        </a>
                                        <a href="group_query.aspx">
                                            <div class="navaltcon-box">
                                                <img src="../img/icons/Group_Query.png">
                                                <h2>Group Query</h2>
                                            </div>
                                        </a>
                                        <a href="recharge.aspx">
                                            <div class="navaltcon-box">
                                                <img src="../img/icons/Online_Recharge.png">
                                                <h2>Online Recharge</h2>
                                            </div>
                                        </a>
                                        <a href="ChangePass.aspx">
                                            <div class="navaltcon-box">
                                                <img src="../img/icons/Change_Password.png">
                                                <h2>Change Password</h2>
                                            </div>
                                        </a>
                                    </div>

                                    <div id="navaltFooter">
                                        <div class="cdtblns"> </div>
                                        <div class="lgut"><a href="Logout.aspx"><i class="fa fa-power-off" aria-hidden="true"></i> Logout</a></div>
                                    </div>

                                </div>
                            </li>
                        </ul>
                    </div>

                </div>

                <div class="admn-blnce"><b>Balance</b> <span><i class="fa fa-inr" aria-hidden="true"></i> <span id="menuID_bal_lbl">3950</span></span></div>
                <a type="button" data-toggle="modal" data-target="#adreq">
                </a>
            </div>
        </div>
    </div>
</header>
<!-- Track Online User 
<script type = "text/javascript">

    $(document).ready(function () {
        setHeartbeat();
    });


    function setHeartbeat() {
        setInterval('chatHeartbeatForOnlineUsers();', '30000');
    }

    function chatHeartbeatForOnlineUsers() {
        $.ajax({
            type: "POST",
            url: "OnlineUser.aspx/GetCurrentUser",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            //success: OnSuccess,
            failure: function (response) {
                alert(response.d);
            }

        });
    }
</script> -->





<script>
    jQuery(document).ready(function() {

        // An array of dates
        var eventDates = {};

        jQuery('#check_out').datepicker({
            beforeShowDay: function(date) {
                var highlight = eventDates[date];
                if (highlight) {
                    return [true, "event", highlight];
                } else {
                    //return [true, '', ''];
                    return [false, '', ''];
                }
            }
        });
    });
</script>

<script>
    //Notification open when load
    $(window).load(function() {
        $("#notificationContainer").fadeToggle(300);
        //$("#notification_count").fadeOut("slow");
        return false;
    });
</script>

<body class="gray">
    <form method="post" action="./RoundTripSearch.aspx" id="form1" class="srch-air" enctype="multipart/form-data">
        <div class="aspNetHidden">
            <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
            <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
            <input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="" />
            <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTc5NDI3MTM5Ng8WCh4OQWlybGluZVZpc2libGUFFTEsMiwzLDQsNSw2LDcsOCwxMCwxMR4LRGVzdFZpc2libGUF7AkxLDIsMyw0LDUsNiw3LDgsOSwxMCwxMSwxMiwxMywxNCwxNywxOCwxOSwyMiwyMywyNCwyNSwyNiwyNywyOCwyOSwzMCwzMSwzMiwzMywzNCwzNSwzNiwzNywzOCw0MCw0Miw0Myw0NCw0NSw0Niw0Nyw0OCw0OSw1MCw1MSw1Miw1NCw1NSw1Niw1Nyw1OCw1OSw2MCw2MSw2Miw2Myw2NCw2NSw2Niw2Nyw2OCw2OSw3MCw3MSw3Miw3Myw3NCw3NSw3Niw3Nyw3OCw4MCw4MSw4Miw4Myw4NCw4NSw4Niw4Nyw4OCw4OSw5MSw5Miw5Myw5NCw5NSw5Niw5Nyw5OCw5OSwxMDAsMTAxLDEwMiwxMDMsMTA0LDEwNSwxMDYsMTA3LDEwOCwxMDksMTEwLDExMiwxMTMsMTE0LDExNSwxMTYsMTE3LDExOCwxMTksMTIwLDEyMSwxMjIsMTIzLDEyNCwxMjUsMTI3LDEyOCwxMjksMTMwLDEzMSwxMzIsMTMzLDEzNCwxMzUsMTM2LDEzNywxMzgsMTM5LDE0MCwxNDEsMTQyLDE0MywxNDQsMTQ1LDE0NiwxNDcsMTQ4LDE0OSwxNTAsMTUxLDE1MiwxNTMsMTU0LDE1NSwxNTYsMTU3LDE1OCwxNTksMTYwLDE2MSwxNjIsMTYzLDE2NCwxNjUsMTY2LDE2NywxNjgsMTY5LDE3MCwxNzEsMTcyLDE3MywxNzQsMTc1LDE3NiwxNzcsMTc4LDE3OSwxODAsMTgxLDE4MiwxODMsMTg2LDE4NywxODgsMTg5LDE5MSwxOTIsMTkzLDE5NCwxOTUsMTk2LDE5NywxOTgsMTk5LDIwMCwyMDEsMjAyLDIwMywyMDQsMjA1LDIwNiwyMDcsMjA4LDIwOSwyMTAsMjExLDIxMiwyMTMsMjE0LDIxNSwyMTYsMjE3LDIxOCwyMTksMjIwLDIyMSwyMjIsMjI0LDIyNSwyMjYsMjI3LDIyOCwyMjksMjMwLDIzMSwyMzIsMjMzLDIzNCwyMzUsMjM2LDIzNywyMzgsMjM5LDI0MCwyNDIsMjQzLDI0NCwyNDUsMjQ2LDI0NywyNDgsMjQ5LDI1MCwyNTEsMjUyLDI1MywyNTQsMjU1LDI1NiwyNTcsMjU4LDI1OSwyNjAsMjYxLDI2MiwyNjMsMjY0LDI2NSwyNjYsMjY3LDI2OCwyNjksMjcwLDI3MSwyNzIsMjczLDI3NCwyNzUsMjc2LDI3NywyNzgsMjc5LDI4MCwyODEsMjgyLDI4MywyODQsMjg1LDI4NiwyODcsMjg4LDI4OSwyOTAsMjkxLDI5MiwyOTMsMjk0LDI5NSwyOTYsMjk3LDI5OCwyOTksMzAwLDMwMSwzMDIsMzAzLDMwNCwzMDUsMzA2LDMwNywzMDgsMzA5LDMxMCwzMTIsMzEzLDMxNCwzMTUsMzE2LDMxNywzMTgsMzE5LDMyMCwzMjEsMzIyLDMyMywzMjQsMzI1LDMyNiwzMjcsMzI4LDMyOSwzMzAsMzMxLDMzMiwzMzMsMzM0LDMzNSwzMzYsMzM3LDMzOCwzMzksMzQwLDM0MSwzNDIsMzQzLDM0NCwzNDUsMzQ2LDM0NywzNTAsMzUxLDM1MiwzNTMsMzU0LDM1NSwzNTYsMzU3LDM1OCwzNTkeB0VtYWlsQWMFFFJhbXVAZ2FuZXNodHJhdmVsLmluHgZDaXR5QWMFATgeBkFjTmFtZQUOR0FORVNIIFRSQVZFTFMWBgIDDw8WAh4EQWNJRAUEMzM1NWQWBAIBDxYCHglpbm5lcmh0bWwFFUdBTkVTSCBUUkFWRUxTIHwgMzM1NWQCAw8PFgIeBFRleHQFBDM5NTBkZAIHDxYCHgdlbmN0eXBlBRNtdWx0aXBhcnQvZm9ybS1kYXRhFggCAQ8WAh4LXyFJdGVtQ291bnRmZAIFDxAPFgYeDURhdGFUZXh0RmllbGQFCUZEZXN0TmFtZR4ORGF0YVZhbHVlRmllbGQFB0ZEZXN0SUQeC18hRGF0YUJvdW5kZ2QQFQEWU2VsZWN0IENpdHkgb3IgQWlycG9ydBUBFlNlbGVjdCBDaXR5IG9yIEFpcnBvcnQUKwMBZxYBZmQCCw8QDxYGHwoFBWFOYW1lHwsFCUFpckxpbmVJRB8MZ2QQFQgMQWxsIEFpcmxpbmVzBkluZGlHbwhTcGljZUpldAVHb0FpcgtKZXQgQWlyd2F5cwdWaXN0YXJhCEFpci1Bc2lhCUFpciBJbmRpYRUIDEFsbCBBaXJsaW5lcwExATIBMwE0ATUBNgIxMRQrAwhnZ2dnZ2dnZ2RkAhsPDxYCHghJbWFnZVVybAUqfi9pbWcvb2ZmZXIvMi5qcGc/cj1YT25xYjh5cmFrcVVML25yQmhPYXNRZGQCCQ8PFgIfDQUqfi9pbWcvb2ZmZXIvMS5qcGc/cj1YT25xYjh5cmFrcVVML25yQmhPYXNRZGRk9GbroapR7AJwErtdjSQSZl9W91qdaKmHA55MHEA1h1k=" />
        </div>

        <script type="text/javascript">
            //<![CDATA[
            var theForm = document.forms['form1'];
            if (!theForm) {
                theForm = document.form1;
            }

            function __doPostBack(eventTarget, eventArgument) {
                if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
                    theForm.__EVENTTARGET.value = eventTarget;
                    theForm.__EVENTARGUMENT.value = eventArgument;
                    theForm.submit();
                }
            }
            //]]>
        </script>


        <div class="aspNetHidden">

            <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="3E8EBEBB" />
            <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdACKOfQaSSkJYP0PGYnZufDGPD92Z1I9P06xqDjPnZKaU8iDsvsaHr2/KUl/ZkF6n9pq8Px+Ie9gtomtqhrqj6igQ1e2LkD/PC+mbyhgq+4ucKb1+CLnXGW8RS22gQIOXsxX8zsWdDe4sRqZrQuezV0JEuHWxyXEf+/zhayNfGNaIyrRqSWoKaskS9VZAGyCcTPgUULBKYfJLNlLFXEBXddSCsIM4WXOZNvnoasKgO0mbn8x7rbkRYwYogup8Lr2v1M0qxzpPd8cLgDV+U/f4T2lA2Ubz/8QlKKwtwWi+aX69tQOm100Umc/kSt1xRZO8VYsxhI/k4TJI/RQUpfHzlp8SA3yZ7a9PKXvtX6vdn5f3uk41pj5IofVsvgcZ9Ih58H1D8SbDBtIQYykjGmQ89ru+YhM0SpqGl6sQ9WQzHF7zLrU3zaTs9Ah+Lyp4AX88QyEtoTbSwlwiqvdkyjI2EuAlg2sDNEIvjPfhivmscDm0w2914R/wLjAPVLnyi4lJB+yslPiZvK6trP3X2QB2Za950wj0jzo56+XNN6J6bHGK/oIX7A+y1km4UDENxlShRIK7AOWRvh3nI2XnS32XC227iq0jw+MsEQKK4qU59FjucVmp6PexttVBkhVoL94TsWuTnG6uI0p2FSOU0skiF6rtUdmsdr0Sfu1Nqd21HiXOAd0KCzHK9QX5UuvmlkW/291nQYxi5ZXxFNgZq2+18mLmr067DdLfE4NCZS2g4tKEJDnnbgjh4g4b1lWa+L++Ns4=" />
        </div>



        <!---------------search panel----------->
        <div class="srch-pnl">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="horizontalTab">
                            <ul class="resp-tabs-list">
                                <a href="Search.aspx">
                                    <li>Oneway</li>
                                </a>
                                <li class="resp-tab-active">Round Trip</li>
                            </ul>
                            <div class="resp-tabs-container">
                                <div>
                                    <div class="srch-box wow  fadeIn animated">
                                        <div class="srch-air ">
                                            <div class="col-md-12">
                                                <span id="show_lbl" style="color:Yellow;"></span>
                                            </div>

                                            <div class="col-md-3">
                                                <label> Destination </label>
                                                <div class="input-group">
                                                    <select name="dest_cmd" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;dest_cmd\&#39;,\&#39;\&#39;)&#39;, 0)" id="dest_cmd" class="form-control select2" style="width: 100%;">
                                                        <option selected="selected" value="Select City or Airport">Select City or Airport</option>

                                                    </select>


                                                    <span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Travel Date </label>
                                                <div class="input-group">
                                                    <input name="check_out" type="text" id="check_out" class="form-control" autocomplete="off" placeholder="DD/MM/YYYY" />
                                                    <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Adults (12 + Yrs)</label>
                                                <div class="input-group">
                                                    <select name="adul_cmd" id="adul_cmd">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>

                                                    </select>

                                                    <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Airline</label>
                                                <div class="input-group">
                                                    <select name="airline_cmd" id="airline_cmd" class="form-control select2" style="width: 100%;">
                                                        <option value="All Airlines">All Airlines</option>
                                                        <option value="1">IndiGo</option>
                                                        <option value="2">SpiceJet</option>
                                                        <option value="3">GoAir</option>
                                                        <option value="4">Jet Airways</option>
                                                        <option value="5">Vistara</option>
                                                        <option value="6">Air-Asia</option>
                                                        <option value="11">Air India</option>

                                                    </select>

                                                    <span class="input-group-addon"><i class="fa fa-plane" aria-hidden="true"></i></span>
                                                </div>
                                            </div>
                                            <div class="col-md-2">


                                                <a id="SearchBtn" class="btn" href="javascript:__doPostBack(&#39;SearchBtn&#39;,&#39;&#39;)">Search <i class="fa fa-search" aria-hidden="true"></i></a>
                                                <input type="hidden" name="HiddenField1" id="HiddenField1" value="3355" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!-------------------srch Domestic-------------------->


                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!------srch panel---------->


            <!---------------search panel----------->
            <div class="pagination-pnl">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pagination-box">
                                <div class="col-md-3">
                                    <a id="LinkButton1" class="pagination-btn-prv" href="javascript:__doPostBack(&#39;LinkButton1&#39;,&#39;&#39;)"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Prev Day</a>
                                </div>
                                <div class="col-md-6">
                                    <p>
                                        <span id="search_dt"></span>
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <a id="LinkButton2" class="pagination-btn-next" href="javascript:__doPostBack(&#39;LinkButton2&#39;,&#39;&#39;)">Next Day <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!---------------pagination panel----------->


            <!---------------search panel----------->
            <div class="air-list">

                <div class="container">
                    <!------flight info--------->

                    <!-- tp -->

                    <span id="empty_lbl"></span>
                </div>

            </div>
            <!--
<div class="banr-in"> 
      <div class="container">
        <div class="row">
          <div class="col-md-12"> 
              <img id="Image1" class="img-responsive" src="../img/offer/2.jpg?r=XOnqb8yrakqUL/nrBhOasQ" />  </div>
        </div>
      </div>
    </div> -->





            <footer>
                <div class="container">
                    <div class="row">




                        <p>Support : <span><a href="tel:9555202202" title="">9555 202 202 | </a><a href="mailto:info@airiq.in">info@airiq.in</a> </span> </p>
                        </p>
                    </div>
                </div>
            </footer>

            <script language="javascript" type="text/javascript">
                function validate() {

                    if (document.getElementById("footerID_amount_txt").value == "") {
                        alert("Please enter the Amount.");
                        document.getElementById("footerID_amount_txt").focus();
                        return false;
                    } else if (document.getElementById("footerID_ref_txt").value == "") {
                        alert("Please enter the Reference No.");
                        document.getElementById("footerID_ref_txt").focus();
                        return false;
                    } else if (document.getElementById("footerID_DropDownList1").value == "Select Payment Mode") {
                        alert("Please select the Payment Mode.");
                        document.getElementById("footerID_DropDownList1").focus();
                        return false;
                    }


                    return true;
                }
            </script>

            <div class="modal fade" id="adreq" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div id="enquiry">
                            <div class="enquiry-bg">
                                <h2>Upload Request
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </h2>
                                <div class="enquiry">


                                    <div method="post" action="" class="enquiry-form">
                                        <div class="col-md-12">

                                            <input name="footerID$amount_txt" type="text" id="footerID_amount_txt" placeholder="Amount*" />
                                        </div>

                                        <div class="col-md-12">
                                            <input name="footerID$ref_txt" type="text" id="footerID_ref_txt" placeholder="Reference No.*" />
                                        </div>

                                        <div class="col-md-12">
                                            <select name="footerID$DropDownList1" id="footerID_DropDownList1">
                                                <option value="Select Payment Mode">Select Payment Mode</option>
                                                <option value="Bank Transfer">Bank Transfer</option>
                                                <option value="IMPS">IMPS</option>
                                                <option value="NEFT">NEFT</option>
                                                <option value="RTGS">RTGS</option>
                                                <option value="Cash Deposit">Cash Deposit</option>
                                                <option value="Credit Request">Credit Request</option>

                                            </select>
                                        </div>

                                        <div class="col-md-12">
                                            <input type="file" name="footerID$FileUpload1" id="footerID_FileUpload1" />
                                        </div>

                                        <div class="col-md-12">
                                            <textarea name="footerID$msg_txt" id="footerID_msg_txt" type="text" placeholder="Message"></textarea>
                                        </div>

                                        <div class="col-md-12">
                                            <input type="submit" name="footerID$Button1" value="Submit" onclick="return validate();" id="footerID_Button1" class="pop-btn" />
                                        </div>
                                    </div>
                                </div>

                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- add req -->



            <!-- Notification -->


            <script type="text/javascript">
                $(document).ready(function() {
                    $("#notificationLink").click(function() {
                        $("#notificationContainer").fadeToggle(300);
                        $("#notification_count").fadeOut("slow");
                        return false;
                    });

                    //open when load
                    //        $(window).load(function () {
                    //            $("#notificationContainer").fadeToggle(300);
                    //            //$("#notification_count").fadeOut("slow");
                    //            return false;
                    //        });

                    //Document Click
                    $(document).click(function() {
                        $("#notificationContainer").hide();
                        $("#notification_count").fadeOut("slow");
                    });
                    //Popup Click
                    $("#notificationContainer").click(function() {
                        return false
                    });

                    //close when click on close button
                    $("#closebtn").click(function() {
                        $("#notificationContainer").hide();
                        $("#notification_count").fadeOut("slow");
                    });


                    //close when click on close button/////////////////////////////////////
                    $("#closebtn").click(function() {
                        $("#navContainer").hide();
                        $("#navalt_count").fadeOut("slow");
                    });


                    $("#navaltLink").click(function() {
                        $("#navaltContainer").fadeToggle(300);
                        $("#navalt_count").fadeOut("slow");
                        return false;
                    });
                    //Document Click
                    $(document).click(function() {
                        $("#navaltContainer").hide();
                        $("#navalt_count").fadeOut("slow");
                    });
                    //Popup Click
                    $("#navaltContainer").click(function() {
                        return true
                    });

                    //close when click on close button
                    $("#closebtn").click(function() {
                        $("#navaltContainer").hide();
                        $("#navalt_count").fadeOut("slow");
                    });
                });
            </script>

            <!--Start of Tawk.to Script-->
            <script type="text/javascript">
                var Tawk_API = Tawk_API || {},
                    Tawk_LoadStart = new Date();
                (function() {
                    var s1 = document.createElement("script"),
                        s0 = document.getElementsByTagName("script")[0];
                    s1.async = true;
                    s1.src = 'https://embed.tawk.to/5f42d0b61e7ade5df443413e/default';
                    s1.charset = 'UTF-8';
                    s1.setAttribute('crossorigin', '*');
                    s0.parentNode.insertBefore(s1, s0);
                })();
            </script>
            <!--End of Tawk.to Script-->

            <!-- Track Online User -->
            <script type="text/javascript">
                $(document).ready(function() {
                    setHeartbeat();
                });


                function setHeartbeat() {
                    setInterval('chatHeartbeatForOnlineUsers();', '60000');
                }

                function chatHeartbeatForOnlineUsers() {
                    $.ajax({
                        type: "POST",
                        url: "OnlineUser.aspx/GetCurrentUser",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        //success: OnSuccess,
                        failure: function(response) {
                            alert(response.d);
                        }

                    });
                }
            </script>






            <script src="../js/bootstrap.min.js"></script>

            <!-- Modal For Terms and Condition -->




            <input type="hidden" name="HiddenField2" id="HiddenField2" />
    </form>

    <!------pop up
<script>
    $(document).ready(function () {
        if ($('.wrap-empt').is(':empty')) {
            alert("yes");
        }
        else {
            alert("no");
        }
    }); 
</script>  
<div class="wrap-empt">
 <div id="popup-container">
       
          <div id="popup-window">
          <div class="modal-dialog"> 
            <div class="modal-content m-pop">
             <a class="close">X</a>
            <div class="modal-body text-center"> 
           
             <img id="Image2" src="../img/offer/1.jpg?r=XOnqb8yrakqUL/nrBhOasQ" />
          
            </div>
          </div>
          </div>
        </div>

      </div>  --------->




</body>

</html>



<script src="../js/wow.min.js"></script>
<script src="../js/calender.js"></script>
<script src="../js/jquery-ui.min.js"></script>


<script src="../js/srch-hlgt.js" type="text/javascript"></script>
<script src="../js/srch-hlgt-txt.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).on('click touchstart', '.SendQry', function() {
        var b_id = $(this).attr("id");
        $("#HiddenField2").val(b_id);
    });
</script>