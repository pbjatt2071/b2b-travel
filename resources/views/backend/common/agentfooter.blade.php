@php
$setting = App\Model\Setting::findOrFail(1);
@endphp
<footer style="background-color: #fff;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center mt-3">
                <p>Support : <span><a href="tel:{{ $setting->mobile }}" title="">{{ $setting->mobile }} | </a><a href="mailto:{{ $setting->email }}">{{ $setting->email }}</a> </span> </p>
                </p>
            </div>
        </div>
    </div>
</footer>
<div id="loadingBox">
    <div class="loading-bg"></div>
    <div class="loading-body">
        <div class="loader-border">
            <img src="{{ url('imgs/logo/airb2b1.jpg') }}" alt="Company Logo" title="Company Logo">
        </div>
    </div>
</div>

{{ HTML::script('js/jquery.min.js') }}
<!-- {{ HTML::script('https://cdn.jsdelivr.net/jquery/1/jquery.min.js') }} -->
{{ HTML::script('https://cdn.jsdelivr.net/momentjs/latest/moment.min.js') }}
{{ HTML::script('https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js') }}
{{ HTML::script('js/popper.min.js') }}
{{ HTML::script('js/bootstrap.min.js') }}
{{ HTML::script('js/sweetalert.min.js') }}
{{ HTML::script('js/validation.js') }}
{{ HTML::script('js/jquery-ui.js') }}
{{ HTML::script('js/moment.js') }}
{{ HTML::script('admin/tinymce/js/tinymce/tinymce.min.js') }}
{{ HTML::script('admin/js/select2.min.js') }}
{{ HTML::script('admin/js/main.js') }}


{{ HTML::script('js/jquery.min.js') }}
{{ HTML::script('https://cdn.jsdelivr.net/jquery/1/jquery.min.js') }}
{{ HTML::script('https://cdn.jsdelivr.net/momentjs/latest/moment.min.js') }}
{{ HTML::script('https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js') }}
{{ HTML::script('js/popper.min.js') }}
{{ HTML::script('js/bootstrap.min.js') }}
{{ HTML::script('js/sweetalert.min.js') }}
{{ HTML::script('js/validation.js') }}
{{ HTML::script('js/jquery-ui.js') }}
{{ HTML::script('js/moment.js') }}
{{ HTML::script('admin/tinymce/js/tinymce/tinymce.min.js') }}
{{ HTML::script('admin/js/select2.min.js') }}
{{ HTML::script('admin/js/main.js') }}

<!-- {{ HTML::script('js/owl.carousel.min.js') }} -->
<!-- {{ HTML::script('js/bootstrap-tagsinput.min.js') }} -->
<!-- {{ HTML::script('js/bootstrap-tagsinput-angular.min.js') }} -->

<script>
    $(document).on('click', '.menu-icon', function(e) {
        $('#sidebar').toggleClass('mob-menu-deactive mob-menu-active');
        $('.overlay').toggleClass('d-none d-block');
    });

    $(document).on('click', '.overlay', function(e) {
        $('#sidebar').toggleClass('mob-menu-deactive mob-menu-active');
        $('.overlay').toggleClass('d-none d-block');
    });
</script>



</body>

</html>