@php
$setting = App\Model\Setting::findOrFail(1);
@endphp
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <base href="{{ url('/').'/' }}">
    <!-- <title>{{ $title }} | {{ $company_info->company_name }}</title> -->
    <title>{{ $title }} | Mahadev</title>
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('icomoon/style.css') }}
    <!-- {{ HTML::style('css/bootstrap-tagsinput.css') }} -->
    {{ HTML::style('css/jquery-ui.css') }}
    {{ HTML::style('admin/css/select2.min.css') }}
    {{ HTML::style('admin/css/style.css') }}
    {{ HTML::style('admin/css/style2.css') }}
    {{ HTML::style('admin/css/theme/blue.css') }}

    {{ HTML::style('https://cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css') }}
    {{ HTML::style('https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css') }}

    <link rel="icon" href="{{ url('imgs/companies/'.$company_info->company_favicon) }}">
    <style>
        /* .container-fluid {
            width: 100%;
            padding-left: 70px;
            padding-right: 70px;
        } */

        .daterangepicker.ltr .calendar.right {
            display: none;
        }

        .daterangepicker {
            position: absolute;
            top: 30px;
            left: 15px;
        }
    </style>
    @if(@auth()->user()->role_id == 3)
    <style>
        ul.resp-tabs-list {
            margin: 0;
            padding: 0px;
        }

        ul.resp-tabs-list li {
            margin: 0;
            padding: 15px;
            /* padding: 0px; */
            list-style: none;
            display: inline-block;

        }

        ul.resp-tabs-list li.active {
            background-color: #3498db;
            color: white;
        }

        .resp-tabs-container {
            padding: 10px 0;
        }

        .resp-tabs-container.active {
            background-color: #3498db;
            color: white;
        }

        .table {
            margin: 0;
        }

        .table th,
        .table td {
            border: 0;
        }

        .table p {
            text-align: center;
            font-style: none;
            font-weight: normal;
            margin-bottom: 0;
        }

        .v-center {
            height: 74px;
            line-height: 74px;
        }

        .btn,
        .form-control {
            border-radius: 0;
            border-color: transparent;
        }

        .card {
            border-radius: 0;
            border-color: transparent;
        }
    </style>
    @endif
</head>

<body style="background-color: #ececec;">
    <div class="overlay d-none"></div>
    <input type="hidden" id="base_url" value="{{ url($company) }}">

    <header class="header-sticky">
        <div class="container-fluid">
            @php
            $profile_pic = !empty( $profile->user_image ) ? url('imgs/users/'.$company.'/'.$profile->user_image) : url('imgs/user_default.png');
            @endphp
            <ul class="header-menu">
                <li class="site-name">
                    <a href="{{ url($company) }}">
                        <h3>{{ $setting->name }}</h3>
                        <!-- <img src="{{ url('imgs/logo/airb2b.jpg') }}" alt="" style="width: 100%;"> -->
                    </a>
                </li>
                <i class="icon-bars menu-icon"></i>
                <li class="menu-li"><a href="{{ url($company) }}" class="menu-list">Dashboard</a></li>
                @if(auth()->user()->role_id == 4)
                <li class="menu-li"><a href="{{ route('tour.index',$company) }}" class="menu-list">Tour</a></li>
                @endif
                <li class="menu-li"><a href="{{ route('bookticket.index',$company) }}" class="menu-list">Booking</a></li>
                <li class="ml-auto menu-li"><a href="#" class="menu-list">Hi, {{ auth()->user()->fname }} <span class="icon-angle-down"></span></a>
                    <ul>
                        <li class="profile-heading text-center">
                            <div class="">
                                <img src="{{ $profile_pic }}" alt="{{ $profile->user_name }}" title="{{ $profile->user_name }}" class="header-profile-pic">
                            </div>
                            <h4>{{ $profile->user_name }}</h4>
                        </li>
                        <li><a href="{{ url($company.'/edit-profile') }}"><i class="icon-pencil"></i> &nbsp; Edit Profile</a></li>
                        @if($profile->user_role == "admin" || $profile->user_role == "super-admin")
                        <li><a href="{{ url($company.'/general-setting') }}"><i class="icon-cog"></i> &nbsp; Settings</a></li>
                        @endif
                        <li><a href="{{ url($company.'/change-password') }}"><i class="icon-lock_open"></i> &nbsp; Change Password</a></li>
                        <li><a href="{{ url($company.'/user/logout') }}"><i class="icon-logout"></i> &nbsp; Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </header>
    <header class="mob-menu-deactive header-2-static " id="sidebar" style="border-top: 1px solid #fff;">
        <!-- <div class="container-fluid"> -->
        <ul class="header-menu">
            <li><a href="{{ url($company) }}" class="menu-list">Dashboard</a></li>
            @if(auth()->user()->role_id == 4)
            <li><a href="{{ route('tour.index',$company) }}" class="menu-list">Tour</a></li>
            @endif
            <li><a href="{{ route('bookticket.index',$company) }}" class="menu-list">Booking</a></li>
            <li class="ml-auto"><a href="#" class="menu-list">Hi, {{ auth()->user()->fname }} <span class="icon-angle-down"></span></a>
                <ul>
                    <li class="profile-heading text-center">
                        <div class="">
                            <img src="{{ $profile_pic }}" alt="{{ $profile->user_name }}" title="{{ $profile->user_name }}" class="header-profile-pic">
                        </div>
                        <h4>{{ $profile->user_name }}</h4>
                    </li>
                    <li><a href="{{ url($company.'/edit-profile') }}"><i class="icon-pencil"></i> &nbsp; Edit Profile</a></li>
                    @if($profile->user_role == "admin" || $profile->user_role == "super-admin")
                    <li><a href="{{ url($company.'/general-setting') }}"><i class="icon-cog"></i> &nbsp; Settings</a></li>
                    @endif
                    <li><a href="{{ url($company.'/change-password') }}"><i class="icon-lock_open"></i> &nbsp; Change Password</a></li>
                    <li><a href="{{ url($company.'/user/logout') }}"><i class="icon-logout"></i> &nbsp; Logout</a></li>
                </ul>
            </li>
        </ul>
        <!-- </div> -->
    </header>