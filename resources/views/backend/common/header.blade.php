@php
$setting = App\Model\Setting::findOrFail(1);
@endphp
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <base href="{{ url('/').'/' }}">
    <!-- <title>{{ $title }} | {{ $company_info->company_name }}</title> -->
    <title>{{ $title }} | Mahadev</title>
    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('icomoon/style.css') }}
    <!-- {{ HTML::style('css/bootstrap-tagsinput.css') }} -->
    {{ HTML::style('css/jquery-ui.css') }}
    {{ HTML::style('admin/css/select2.min.css') }}
    {{ HTML::style('admin/css/style.css') }}
    {{ HTML::style('admin/css/theme/blue.css') }}


    {{ HTML::style('https://cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css') }}
    {{ HTML::style('https://cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css') }}

    <link rel="icon" href="{{ url('imgs/companies/'.$company_info->company_favicon) }}">
    <style>
        .container-custom {
            width: 100%;
            padding-left: 70px;
            padding-right: 70px;
        }

        .daterangepicker.ltr .calendar.right {
            display: none;
        }

        .daterangepicker {
            position: absolute;
            top: 30px;
            left: 15px;
        }
    </style>
</head>

<body style="background-color: #ececec;">
<div class="overlay loading-bg d-none">

</div>
    <input type="hidden" id="base_url" value="{{ url($company) }}">
    <header class="header-1-static">
        <div class="container-fluid">
            <h3 class="mb-0 pt-2 pb-2"> {{ $setting->name }}</h3>
            <i class="icon-bars menu-icon"></i>
        </div>
    </header>
    <header class="mob-menu-deactive header-2-static" id="sidebar" style="border-top: 1px solid #fff;">
        <div class="container-fluid">
            <ul class="header-menu">
                <li><a href="{{ url($company) }}">Dashboard</a></li>
                <li><a href="#">Master <span class="icon-angle-down"></span></a>
                    <ul>
                        <li><a href="#">Location</a>
                            <ul>
                                <li><a href="{{ route('country.index',$company) }}">Country</a></li>
                                <li><a href="{{ route('state.index',$company) }}">State</a></li>
                                <li><a href="{{ route('city.index',$company) }}">City</a></li>
                                <li><a href="{{ route('pincode.index',$company) }}">Pincode</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Account</a>
                            <ul>
                                <!-- <li><a href="#">Admin</a></li> -->
                                <li><a href="{{ route('user.index',[$company,'Agent']) }}">Agent</a></li>
                                <li><a href="{{ route('user.index',[$company,'Supplier']) }}">Supplier</a></li>
                                <!-- <li><a href="{{ url( $company.'/account/staff' ) }}">Staff</a></li> -->
                            </ul>
                        </li>
                        <!-- <li><a href="#">Add-on Items</a>
                            <ul>
                                <li><a href="{{ url($company.'/unit') }}">Unit</a></li>
                                <li><a href="{{ url($company.'/add-on-item') }}">Add-on Items</a></li>
                            </ul>
                        </li> -->
                        <!-- <li><a href="{{ url($company.'/vehicle') }}">Vehicle</a></li>
                        <li><a href="{{ url($company.'/department') }}">Department</a></li>
                        <li><a href="#">Packages / Fixed Cost Tour</a></li> -->
                        <li><a href="{{ route('airline.index',$company) }}">Airlines</a></li>
                        <li><a href="{{ route('airport.index',$company) }}">Airports</a></li>
                    </ul>
                </li>
                <!-- <li>
                    <a href="{{ url($company.'/leads') }}">Leads <span class="icon-angle-down"></span></a>
                    <ul>
                        <li><a href="{{ url($company.'/leads') }}">View Lead</a></li>
                        <li><a href="{{ url($company.'/leads/follow-up/today') }}">Follow-up</a></li>
                        <li><a href="{{ url($company.'/leads/management') }}">Lead Management</a></li>
                    </ul>
                </li> -->
                <!-- <li>
                    <a href="#">Visa <span class="icon-angle-down"></span></a>
                    <ul>
                        <li><a href="{{ url($company.'/visa/add') }}">Add Visa</a></li>
                        <li><a href="{{ url($company.'/visa') }}">View Visa</a></li>
                    </ul>
                </li> -->
                <li><a href="{{ route('tour.index',$company) }}">Tours</a></li>
                <li><a href="{{ route('booking.index',$company) }}">Booking</a></li>
                <!-- <li><a href="#">Billing</a></li>
                <li><a href="#">Reports <span class="icon-angle-down"></span></a>
                    <ul>
                        <li><a href="#">My Drawer</a></li>
                        <li><a href="#">Cashbook</a></li>
                        <li><a href="#">Bankbook</a></li>
                        <li><a href="#">Daybook</a></li>
                    </ul>
                </li>
                <li><a href="#">Website Master <span class="icon-angle-down"></span></a>
                    <ul>
                        <li><a href="#">Activity</a>
                            <ul>
                                <li><a href="{{ url( $company.'/activity/add' ) }}">Add Activity</a></li>
                                <li><a href="{{ url( $company.'/activity' ) }}">View Activity</a></li>
                            </ul>
                        </li>
                    </ul>
                </li> -->
                @php
                $profile_pic = !empty( $profile->user_image ) ? url('imgs/users/'.$company.'/'.$profile->user_image) : url('imgs/user_default.png');
                @endphp
                <li class="ml-auto"><a href="#">Hi, {{ $profile->user_name }} <span class="icon-angle-down"></span></a>
                    <ul>
                        <li class="profile-heading text-center">
                            <div class="">
                                <img src="{{ $profile_pic }}" alt="{{ $profile->user_name }}" title="{{ $profile->user_name }}" class="header-profile-pic">
                            </div>
                            <h4>{{ $profile->user_name }}</h4>
                        </li>
                        <li><a href="{{ url($company.'/edit-profile') }}"><i class="icon-pencil"></i> &nbsp; Edit Profile</a></li>
                        @if($profile->user_role == "admin" || $profile->user_role == "super-admin")
                        <li><a href="{{ url($company.'/general-setting') }}"><i class="icon-cog"></i> &nbsp; Settings</a></li>
                        @endif
                        <li><a href="{{ url($company.'/change-password') }}"><i class="icon-lock_open"></i> &nbsp; Change Password</a></li>
                        <li><a href="{{ url($company.'/user/logout') }}"><i class="icon-logout"></i> &nbsp; Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </header>