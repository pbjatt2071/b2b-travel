<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookTicket extends Model
{
    protected $with = ['tour','user','passengers'];
    public function tour() {
        return $this->hasOne('App\Model\Tour', 'id', 'tour_id');
    }
    public function user() {
        return $this->hasOne('App\Model\User', 'id', 'user_id');
    }
    
    public function passengers() {
        return $this->hasMany('App\BookTicketPassenger', 'book_ticket_id', 'id');
    }
}
