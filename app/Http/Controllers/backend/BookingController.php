<?php

namespace App\Http\Controllers\backend;

use App\BookTicket;
use App\BookTicketPassenger;
use App\Model\Airline;
use App\Model\Airport;
use App\Model\Tour;
use App\User;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class BookingController extends BaseController
{
	public function index(Request $request, $company)
	{
		$query   = BookTicket::query();
		// $search  = $request->input('search');
		// if (!empty($search['keyword'])) {
		//     $query->where(function ($q) use ($search) {
		//         $q->where('name', 'LIKE', '%' . $search['keyword'] . '%');
		//     });
		// }
		$records = $query->latest()->paginate(30);
		$title  = "Booking";
		$page   = "booking.bookings";
		$data = compact('title', 'page', 'records');
		// dd($data);
		return view('backend/layout', $data);
	}
	public function edit(Request $request, $company, $id)
	{
		$edit = Tour::findOrFail($id);
		$passenger = $request->passenger;
		$passArr = [];
		for ($i = 0; $i < $passenger; $i++) {
			$passArr[] = $i;
		}

		$title     = "Booking";
		$page     = "booking.booking";
		$data     = compact('page', 'title', 'edit', 'company', 'passenger', 'passArr');
		return view('backend/layout', $data);
	}

	public function update(Request $request, $company, $id)
	{
		$tour = new BookTicket;
		$tour->tour_id = $id;
		$tour->user_id = auth()->user()->id;
		$tour->price = $request->price;
		$tour->passenger = $request->passenger;
		$tour->pnr_number = $request->pnr_number;
		$tour->save();
		foreach ($request->pass as $key => $passenger) {
			$pass = new BookTicketPassenger;
			$pass->book_ticket_id = $tour->id;
			$pass->title = $passenger['title'];
			$pass->fname = $passenger['fname'];
			$pass->lname = $passenger['lname'];
			$pass->save();
		}

		return redirect(route('bookticket.index', 'mahadev'))->with('success', 'Your Ticket has been Booked Successfully.');
	}
}
