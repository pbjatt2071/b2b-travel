<?php
namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Model\Activity;
use App\Model\User;
use App\Model\Role;

class ActivityController extends BaseController {
    public function index(Request $request, $company, $id = NULL) {
        $profile = $this->profile($request);

        $title 	= "Activity";
        $page 	= "view_activity";

        $search = $request->input('search');


        $query   =  Activity::where('activity_is_deleted', 'N')->where('activity_company', $profile->user_company);

        if(!empty($search['keyword'])) {
            $query->where(function($q) use($search) {
                $s = trim( $search['keyword'] );

                $q->where('activity_tilte', 'LIKE', '%'.$s.'%');
            });
        }

        $records = $query->orderBy('activity_id', 'DESC')->paginate(30);

        $data 	= compact('page', 'title', 'records', 'search');
        return view('backend/layout', $data);
    }

    public function add(Request $request, $company, $id = null) {
        $profile = $this->profile($request);

        if(!empty($id)) {
            $edit       = Activity::find($id);
        }

        $title 	= empty($edit->activity_id) ? "Add Activity" : "Edit Activity";
        $page 	= "add_activity";

        if($request->isMethod('post')) {
            $record = $request->input('record');

            $record['activity_updated_on']    = date('Y-m-d H:i:s', time());
            $record['activity_added_by']      = $profile->user_id;
            $record['activity_company']       = $profile->user_company;

            $record = array_filter($record);

            if(empty($id)) {
                $user['activity_created_on'] = date('Y-m-d H:i:s', time());
                $id = Activity::insertGetId($record);
            } else {
                Activity::where('activity_id', $id)->update($record);
            }
            if ($request->hasFile('activity_image')) {
                if(!empty($edit->activity_image) && file_exists(public_path()."/imgs/activities/".$edit->activity_image)) {
                    unlink(public_path()."/imgs/activities/".$edit->activity_image);
                }

                $image           = $request->file('activity_image');
                $name            = 'IMG'.$id.'.'.$image->getClientOriginalExtension();
                $destinationPath = "public/imgs/activities/";
                $image->move($destinationPath, $name);

                if(!empty($edit->user_image)) {
                    $name .= "?v=".uniqid();
                }

                Activity::where('activity_id', $id)->update( array('activity_image' => $name) );
            }

            return redirect("{$company}/activity/")->with('success', "Success! New Activity has been created.");
        }

        if(!empty($profile)) {
            $customers  = User::where('user_is_deleted', 'N')->where('user_role', 'customer')->where('user_company', @$profile->user_company)->get();
            $staffs     = User::where('user_is_deleted', 'N')->where('user_role', 'staff')->where('user_company', @$profile->user_company)->get();
        }


        $data 	    = compact('page', 'title', 'edit', 'customers', 'staffs');
        return view('backend/layout', $data);
    }
}
