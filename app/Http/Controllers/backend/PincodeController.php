<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\City;
use App\Model\Pincode;
use PHPUnit\Framework\Constraint\Count;

class PincodeController extends BaseController
{
    public function index(Request $request, $company)
    {
        $query   = Pincode::latest();
        $search  = $request->input('search');
        if (!empty($search['city'])) {
            $query->where('city_id', $search['city']);
        }
        if (!empty($search['keyword'])) {
            $query->where(function ($q) use ($search) {
                $q->where('code', 'LIKE', '%' . $search['keyword'] . '%');
            });
        }
        $records = $query->orderBy('code', 'ASC')->paginate(30);
        $cities  = City::get();

        $title  = "Pincodes";
        $page   = "location.pincode";
        $data   = compact('page', 'title', 'records', 'cities', 'search', 'company');
        return view('backend/layout', $data);
    }

    public function edit(Request $request, $company, $id)
    {
        $query   = Pincode::latest();
        $search  = $request->input('search');
        if (!empty($search['keyword'])) {
            $query->where(function ($q) use ($search) {
                $q->where('code', 'LIKE', '%' . $search['keyword'] . '%');
            });
        }
        $records = $query->orderBy('code', 'ASC')->paginate(30);
        $edit = Pincode::findOrFail($id);

        $cities  = City::get();

        $title     = "Pincodes";
        $page     = "location.pincode";
        $data     = compact('page', 'title', 'records', 'edit', 'search', 'company', 'cities');
        return view('backend/layout', $data);
    }

    public function store(Request $request, $company, $id = null)
    {
        $company = 'mahadev';
        $input = $request->input('record');
        $edit = [];
        if (!empty($id)) {
            $edit = Pincode::find($id);
        }
        if (!empty($input)) {
            $isExistsQuery = Pincode::where(function ($q) use ($input) {
                $q->where('code', $input['code']);
            });

            if (!empty($edit->id)) {
                $isExistsQuery->where('id', '!=', $edit->id);
            }

            $isExists = $isExistsQuery->count();

            if (!$isExists) :
                if (empty($edit->id)) {
                    $id   = Pincode::insertGetId($input);
                    $message = "Success! New record has been added.";
                } else {
                    Pincode::where('id', $id)->update($input);
                    $message = "Success! New record has been updated.";
                }
            else :
                return redirect(route('pincode.index', $company))->with('danger', 'Pincode name already exists.');
            endif;
        }
        $check = $request->input('check');
        if (!empty($check)) {
            Pincode::whereIn('id', $check)->delete();
            $message = "Success! Selected records has been removed.";
        }

        return redirect(route('pincode.index', $company))->with('success', $message);
    }

    public function exportcsv(Request $request, $company)
    {
        $records = Pincode::get()->toArray();

        $fields = $values = [];
        foreach ($records as $i => $rec) {
            if ($i == 0) {
                $fields = array_keys($rec);
            }
            $values[] = array_values($rec);
        }

        $filename = "PincodeData_{$company}.csv";

        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");

        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, $fields);
        foreach ($values as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

    public function importcsv(Request $request, $company)
    {
        $file = $request->file('import_file');
        $fileName = $file->getPathName();
        $file = fopen($fileName, "r");
        $i = 0;
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
            if ($i == 0) {
                $fieldArr = $getData;
            } else {
                $row = array_combine($fieldArr, $getData);
                Pincode::insert($row);
            }
            $i++;
        }
        return redirect(route('pincode.index', $company))->with('success', 'Success! Records has been imported.');
    }
}
