<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\City;
use App\Model\Country;
use App\Model\Pincode;
use App\Model\Role;
use App\Model\State;
use App\Model\User;
use App\Model\UserDetail;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\Constraint\Count;

class UserController extends BaseController
{
    public function index(Request $request, $company, $role)
    {
        $checkrole = Role::where('name', $role)->count();
        if ($checkrole == 0) {
            return redirect(route('404-error'));
        }
        $query   = User::latest();
        $search  = $request->input('search');
        if (!empty($role)) {
            $query->whereHas('role', function ($q) use ($role) {
                $q->where('name',  $role);
            });
        }
        if (!empty($search['keyword'])) {
            $query->where(function ($q) use ($search) {
                $q->where('fname', 'LIKE', '%' . $search['keyword'] . '%')
                    ->orWhere('lname', 'LIKE', '%' . $search['keyword'] . '%')
                    ->orWhere('mobile', 'LIKE', '%' . $search['keyword'] . '%')
                    ->orWhere('email', 'LIKE', '%' . $search['keyword'] . '%');
            });
        }
        $records = $query->orderBy('fname', 'ASC')->paginate(30);

        $title  = $role;
        $page   = "user.index";
        $data   = compact('page', 'title', 'records', 'search', 'role', 'company');
        return view('backend/layout', $data);
    }

    public function add(Request $request, $company, $role)
    {
        $countries  = Country::get();
        $states  = State::get();
        $cities  = City::get();
        $pincodes  = Pincode::get();

        $title    = $role;
        $page     = "user.add";
        $data     = compact('page', 'title', 'company', 'role', 'countries', 'states', 'cities', 'pincodes');
        return view('backend/layout', $data);
    }

    public function edit(Request $request, $company, $role, $id = null)
    {
        $query   = User::query();
        $search  = $request->input('search');
        if (!empty($search['keyword'])) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search['keyword'] . '%');
            });
        }
        $records = $query->orderBy('fname', 'ASC')->paginate(10);
        $edit = User::findOrFail($id);
        $edit1 = UserDetail::where('user_id', $edit->id)->first();

        $countries  = Country::get();
        $states  = State::get();
        $cities  = City::get();
        $pincodes  = Pincode::get();

        $title    = $role;
        $page     = "user.add";
        $data     = compact('page', 'title', 'records', 'edit', 'edit1', 'search', 'company', 'role', 'countries', 'states', 'cities', 'pincodes');
        return view('backend/layout', $data);
    }

    public function store(Request $request, $company, $role, $id = null)
    {
        $company = 'mahadev';
        $input = $request->input('user');
        $input1 = $request->input('userdetail');
        $edit = [];
        if (!empty($id)) {
            $edit = User::find($id);
        }
        if (!empty($input)) {
            $isExistsQuery = User::where(function ($q) use ($input) {
                $q->where('login', $input['login']);
            });

            if (!empty($edit->id)) {
                $isExistsQuery->where('id', '!=', $edit->id);
            }

            $isExists = $isExistsQuery->count();

            if (!$isExists) {
                if (empty($edit->id)) {
                    $roleid = Role::where('name', $role)->first();
                    $input['role_id'] = $roleid->id;
                    $input['password'] = Hash::make($input['password']);
                    $id   = User::insertGetId($input);
                    $input1['user_id'] = $id;
                    $id_userdetail   = UserDetail::insertGetId($input1);
                    $message = "Success! New record has been added.";
                } else {
                    if ($input['password']) {
                        $input['password'] = Hash::make($input['password']);
                    }
                    User::where('id', $id)->update($input);
                    UserDetail::where('user_id', $id)->update($input1);
                    $message = "Success! New record has been updated.";
                }
                if ($request->hasFile('image')) {
                    if (!empty($edit->image) && file_exists(storage_path() . "users/" . $edit->image)) {
                        unlink(storage_path() . "users/" . $edit->image);
                    }
                    $file           = $request->file('image');
                    $name            = 'IMG' . $id . '.' . $file->getClientOriginalExtension();

                    // $image = Image::make($file)->resize(150,60);
                    // $image = Image::make($file)->fit(300);
                    $image = Image::make($file)->resize(70, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    Storage::put('public/users/' . $name, (string) $image->encode());

                    if (!empty($edit->image)) {
                        $name .= "?v=" . uniqid();
                    }
                    UserDetail::where('user_id', $id)->update(array('image' => $name));
                }
            } else {
                return redirect(route('user.index', [$company, $role]))->with('danger', 'Airline name already exists.');
            }
        }
        $check = $request->input('check');
        if (!empty($check)) {
            User::whereIn('id', $check)->delete();
            $message = "Success! Selected records has been removed.";
        }

        return redirect(route('user.index', [$company, $role]))->with('success', $message);
    }


    public function status(Request $request, $company, $id, $status)
    {
        $user = User::find($id);
        $user->verified = $status;
        $user->save();

        return redirect()->back()->with('success', "Success! Status has been updated.");
    }

    public function exportcsv(Request $request, $company)
    {
        $records = City::get()->toArray();

        $fields = $values = [];
        foreach ($records as $i => $rec) {
            if ($i == 0) {
                $fields = array_keys($rec);
            }
            $values[] = array_values($rec);
        }

        $filename = "CityData_{$company}.csv";

        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");

        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, $fields);
        foreach ($values as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

    public function importcsv(Request $request, $company)
    {
        $file = $request->file('import_file');
        $fileName = $file->getPathName();
        $file = fopen($fileName, "r");
        $i = 0;
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
            if ($i == 0) {
                $fieldArr = $getData;
            } else {
                $row = array_combine($fieldArr, $getData);
                City::insert($row);
            }
            $i++;
        }
        return redirect(route('city.index', $company))->with('success', 'Success! Records has been imported.');
    }

    public function logout(Request $request, $company)
    {
        Auth::logout();
        session()->forget('user_auth');
        return redirect(route('dashboard', $company));
    }
}
