<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\Country;
use PHPUnit\Framework\Constraint\Count;

class CountryController extends BaseController
{
    public function index(Request $request, $company)
    {
        $query   = Country::withCount('states');
        $search  = $request->input('search');
        if (!empty($search['keyword'])) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search['keyword'] . '%')
                    ->orWhere('short_name', 'LIKE', '%' . $search['keyword'] . '%')
                    ->orWhere('code', 'LIKE', '%' . $search['keyword'] . '%');
            });
        }
        $records = $query->orderBy('name', 'ASC')->paginate(30);

        $title     = "Countries";
        $page     = "location.country";
        $data     = compact('page', 'title', 'records', 'search', 'company');
        return view('backend/layout', $data);
    }

    public function edit(Request $request, $company, $id)
    {
        $query   = Country::withCount('states');
        $search  = $request->input('search');
        if (!empty($search['keyword'])) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search['keyword'] . '%')
                    ->orWhere('short_name', 'LIKE', '%' . $search['keyword'] . '%')
                    ->orWhere('code', 'LIKE', '%' . $search['keyword'] . '%');
            });
        }
        $records = $query->orderBy('name', 'ASC')->paginate(30);
        $edit = Country::findOrFail($id);

        $title     = "Countries";
        $page     = "location.country";
        $data     = compact('page', 'title', 'records', 'edit', 'search', 'company');
        return view('backend/layout', $data);
    }

    public function store(Request $request, $company, $id = null)
    {
        $company = 'mahadev';
        $input = $request->input('record');
        $edit = [];
        if (!empty($id)) {
            $edit = Country::find($id);
        }
        if (!empty($input)) {
            $isExistsQuery = Country::where(function ($q) use ($input) {
                $q->where('name', $input['name'])
                    ->orWhere('code', $input['code']);
            });

            if (!empty($edit->id)) {
                $isExistsQuery->where('id', '!=', $edit->id);
            }

            $isExists = $isExistsQuery->count();

            if (!$isExists) :
                if (empty($edit->id)) {
                    $id   = Country::insertGetId($input);
                    $message = "Success! New record has been added.";
                } else {
                    Country::where('id', $id)->update($input);
                    $message = "Success! New record has been updated.";
                }
            else :
                return redirect(route('country.index', $company))->with('danger', 'Country name or code already exists.');
            endif;
        }
        $check = $request->input('check');
        if (!empty($check)) {
            Country::whereIn('id', $check)->delete();
            $message = "Success! Selected records has been removed.";
        }

        return redirect(route('country.index', $company))->with('success', $message);
    }

    public function exportcsv(Request $request, $company)
    {
        $records = Country::get()->toArray();

        $fields = $values = [];
        foreach ($records as $i => $rec) {
            if ($i == 0) {
                $fields = array_keys($rec);
            }
            $values[] = array_values($rec);
        }

        $filename = "CountriesData_{$company}.csv";

        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");

        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, $fields);
        foreach ($values as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

    public function importcsv(Request $request, $company)
    {
        $file = $request->file('import_file');
        $fileName = $file->getPathName();
        $file = fopen($fileName, "r");
        $i = 0;
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
            if ($i == 0) {
                $fieldArr = $getData;
            } else {
                $row = array_combine($fieldArr, $getData);
                Country::insert($row);
            }
            $i++;
        }
        return redirect(route('country.index', $company))->with('success', 'Success! Records has been imported.');
    }
}
