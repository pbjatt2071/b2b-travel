<?php
namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Model\Vehicle;
use App\Model\User;
use App\Model\Role;

class VehicleController extends BaseController {
    public function index(Request $request, $company, $id = NULL) {
        $profile = $this->profile($request);

        $title 	= "Vehicles";
        $page 	= "view_vehicle";

        $search = $request->input('search');


        $query   =  Vehicle::where('vehicle_is_deleted', 'N')->where('vehicle_company', $profile->user_company);

        if(!empty($search['keyword'])) {
            $query->where(function($q) use($search) {
                $s = trim( $search['keyword'] );

                $q->where('vehicle_make', 'LIKE', '%'.$s.'%')
                  ->orWhere('vehicle_model', 'LIKE', '%'.$s.'%')
                  ->orWhere('vehicle_seat', 'LIKE', '%'.$s.'%');
            });
        }

        $records = $query->orderBy('vehicle_id', 'DESC')->paginate(30);

        $data 	= compact('page', 'title', 'records', 'search');
        return view('backend/layout', $data);
    }

    public function add(Request $request, $company, $id = null) {
        $profile = $this->profile($request);

        if(!empty($id)) {
            $edit       = Vehicle::find($id);
        }

        $title 	= empty($edit->vehicle_id) ? "Add Vehicle" : "Edit Vehicle";
        $page 	= "add_vehicle";

        if($request->isMethod('post')) {
            $record = $request->input('record');

            $record['vehicle_updated_on']    = date('Y-m-d H:i:s', time());
            $record['vehicle_added_by']      = $profile->user_id;
            $record['vehicle_company']       = $profile->user_company;

            $record = array_filter($record);

            if(empty($id)) {
                $user['vehicle_created_on'] = date('Y-m-d H:i:s', time());
                $id = Vehicle::insertGetId($record);
            } else {
                Vehicle::where('vehicle_id', $id)->update($record);
            }

            return redirect("{$company}/vehicles/")->with('success', "Success! New Vehicle has been created.");
        }

        if(!empty($profile)) {
            $customers  = User::where('user_is_deleted', 'N')->where('user_role', 'customer')->where('user_company', @$profile->user_company)->get();
            $staffs     = User::where('user_is_deleted', 'N')->where('user_role', 'staff')->where('user_company', @$profile->user_company)->get();
        }


        $data 	    = compact('page', 'title', 'edit', 'customers', 'staffs');
        return view('backend/layout', $data);
    }
}
