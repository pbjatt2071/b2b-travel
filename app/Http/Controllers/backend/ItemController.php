<?php
namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use App\Model\Unit;
use App\Model\Item;
use App\Model\User;
use App\Model\Role;
use App\Query;

class ItemController extends BaseController {
    public function index(Request $request, $company, $id = NULL) {
        $profile = $this->profile($request);

        $title 	= "Add-on Items";
        $page 	= "view_item";

        $search = $request->input('search');


        $query   =  Item::with(['unit'])->where('item_is_deleted', 'N')->where('item_company', $profile->user_company);

        if(!empty($search['keyword'])) {
            $query->where(function($q) use($search) {
                $s = trim( $search['keyword'] );

                $q->where('item_name', 'LIKE', '%'.$s.'%')
                  ->orWhere('item_description', 'LIKE', '%'.$s.'%');
            });
        }

        $records = $query->orderBy('item_id', 'DESC')->paginate(30);

        $data 	= compact('page', 'title', 'records', 'search');
        return view('backend/layout', $data);
    }

    public function add(Request $request, $company, $id = null) {
        $profile = $this->profile($request);

        $edit = [];
        if(!empty($id)) {
            $edit       = Item::find($id);
        }

        $title 	= empty($edit->item_id) ? "Add Add-on Item" : "Edit Add-on Item";
        $page 	= "add_item";

        if($request->isMethod('post')) {
            $record = $request->input('record');

            $record['item_updated_on']    = date('Y-m-d H:i:s', time());
            $record['item_added_by']      = $profile->user_id;
            $record['item_company']       = $profile->user_company;

            $record = array_filter($record);

            if(empty($id)) {
                $user['item_created_on'] = date('Y-m-d H:i:s', time());
                $id = Item::insertGetId($record);

                $mess = "Success! New record has been added.";
            } else {
                Item::where('item_id', $id)->update($record);

                $mess = "Success! A record has been updated.";
            }

            if ($request->hasFile('item_image')) {
                if(!empty($edit->item_image) && file_exists(public_path()."/imgs/items/".$edit->item_image)) {
                    unlink(public_path()."/imgs/items/".$edit->item_image);
                }

                $image           = $request->file('item_image');
                $name            = 'IMG'.$id.'.'.$image->getClientOriginalExtension();
                $destinationPath = "public/imgs/items/";
                $image->move($destinationPath, $name);

                $dir1 = public_path().'/imgs/items/';
                $dir = url('imgs/items');

                Query::resize_image($dir.'/'.$name, 256, 256, $dir1.'/'.$name);

                if(!empty($edit->item_image)) {
                    $name .= "?v=".uniqid();
                }

                Item::where('item_id', $id)->update( array('item_image' => $name) );
            }

            return redirect("{$company}/add-on-item/")->with('success', $mess);
        }

        // $units   = Unit::where('unit_is_deleted', 'N')->orderBy('unit_name', 'ASC')->get();


        // $data 	    = compact('page', 'title', 'edit', 'units');
        $data 	    = compact('page', 'title', 'edit');
        return view('backend/layout', $data);
    }
}
