<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\City;
use App\Model\Airport;
use PHPUnit\Framework\Constraint\Count;

class AirportController extends BaseController
{
    public function index(Request $request, $company)
    {
        $query   = Airport::query();
        $search  = $request->input('search');
        if (!empty($search['city'])) {
            $query->where('city_id', $search['city']);
        }
        if (!empty($search['keyword'])) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search['keyword'] . '%');
            });
        }
        $records = $query->orderBy('name', 'ASC')->paginate(30);
        $cities  = City::get();

        $title  = "Airport";
        $page   = "airport.airport";
        $data   = compact('page', 'title', 'records', 'cities', 'search', 'company');
        return view('backend/layout', $data);
    }

    public function edit(Request $request, $company, $id)
    {
        $query   = Airport::query();
        $search  = $request->input('search');
        if (!empty($search['city'])) {
            $query->where('city_id', $search['city']);
        }
        if (!empty($search['keyword'])) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search['keyword'] . '%');
            });
        }
        $records = $query->orderBy('name', 'ASC')->paginate(30);
        $edit = Airport::findOrFail($id);

        $cities  = City::get();

        $title     = "Airport";
        $page     = "airport.airport";
        $data     = compact('page', 'title', 'records', 'edit', 'search', 'company', 'cities');
        return view('backend/layout', $data);
    }

    public function store(Request $request, $company, $id = null)
    {
        $company = 'mahadev';
        $input = $request->input('record');
        $edit = [];
        if (!empty($id)) {
            $edit = Airport::find($id);
        }
        if (!empty($input)) {
            $isExistsQuery = Airport::where(function ($q) use ($input) {
                $q->where('name', $input['name']);
            });

            if (!empty($edit->id)) {
                $isExistsQuery->where('id', '!=', $edit->id);
            }

            $isExists = $isExistsQuery->count();

            if (!$isExists) :
                if (empty($edit->id)) {
                    $id   = Airport::insertGetId($input);
                    $message = "Success! New record has been added.";
                } else {
                    Airport::where('id', $id)->update($input);
                    $message = "Success! New record has been updated.";
                }
            else :
                return redirect(route('airport.index', $company))->with('danger', 'Airport name already exists.');
            endif;
        }
        $check = $request->input('check');
        if (!empty($check)) {
            Airport::whereIn('id', $check)->delete();
            $message = "Success! Selected records has been removed.";
        }

        return redirect(route('airport.index', $company))->with('success', $message);
    }

}
