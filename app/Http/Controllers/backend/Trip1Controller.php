<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Model\Department;
use App\Model\City;

class TripController extends BaseController {
    public function add(Request $request, $company, $id = null) {
        $profile = $this->profile($request);

    	$edit = [];
    	if(!empty($id)) {
    		$edit = Department::find($id);
    	}

    	if($request->isMethod('post')) {
    		$input = $request->input('record');
    		if(!empty($input)) {
                session( ['travel' => $input] );

                return redirect( url( $company.'/hotels' ) );
		    }

            return redirect()->back()->with('danger', 'No input found.');
    	}

        $title 	= "Plan Your Trip";
        $page 	= "create_fixed_tour";
        $data 	= compact('page', 'title');
        return view('backend/layout', $data);
    }

    public function select_hotel( Request $request ) {

        $travel_session = session('travel');

        $hotelArr = [];
        $i = 0;
        foreach($travel_session['visit_place'] as $place) {
            $j        = $i + $travel_session[$place]['no_of_nights'];
            $check_in = date('d/m/Y', strtotime( $travel_session['travel_date']." +{$i} days" ));
            $check_out= date('d/m/Y', strtotime( $travel_session['travel_date']." +{$j} days" ));
            if(!empty($travel_session['age_of_child'])){
              $travel_session['age_of_child'] = $travel_session['age_of_child'];
            } else{
              $travel_session['age_of_child'] = 0;
            }
            $param = [
                'city_name'         => $place,
                'chkin_date'        => $check_in,
                'chkout_date'       => $check_out,
                'total_adults'      => $travel_session['no_of_adult'],
                'total_children'    => $travel_session['no_of_child'],
                'age_of_child'      => $travel_session['age_of_child'],
            ];
            $hotel_arr = $this->hotel_api( $param );

            // foreach($hotel_arr->Hotels->Hotel as $key => $hotel) {
                // $hotel_info = $this->hotel_detais_api( $hotel->Id );
                // print_r($hotel_info);
                // die;
                // $hotel_arr->Hotels->Hotel[$key]->hotel_info = $hotel_info;
            // }

            $hotelArr[$place] = $hotel_arr;
            $i++;
        }

        // echo '<pre>';
        // print_r($hotelArr);
        // echo '</pre>';

        $title 	= "Select Your Hotel";
        $page 	= "select_hotel";
        $data 	= compact('page', 'title', 'travel_session', 'hotelArr');
        return view('backend/layout', $data);
    }

    public function hotel_detais_api($hotel_code) {
        $xmlcontent = '
        <?xml version="1.0"?>
            <HotelDetailsRequest>
                <Authentication>
                    <AgentCode>CD32157</AgentCode>
                    <UserName>holidayplanner</UserName>
                    <Password>HolidaY@12</Password>
                </Authentication>
                <Hotels>
                    <HotelId>'.$hotel_code.'</HotelId>
                </Hotels>
            </HotelDetailsRequest>
        ';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_URL, "http://test.xmlhub.com/testpanel.php/action/findhotel");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "XML=".$xmlcontent);
        $content=curl_exec($ch);

        $arr = simplexml_load_string($content) or die("Error: Cannot create object");
        return $arr;
    }

    public function hotel_api($param = []) {
        $city_info = City::where('city_name', 'LIKE', $param['city_name'])->first();

        $xmlcontent = '
            <?xml version="1.0"?>
            <HotelFindRequest>
            <Authentication>
                <AgentCode>CD32157</AgentCode>
                <UserName>holidayplanner</UserName>
                <Password>HolidaY@12</Password>
            </Authentication>
            <Booking>
            <ArrivalDate>'.$param['chkin_date'].'</ArrivalDate>
            <DepartureDate>'.$param['chkout_date'].'</DepartureDate>
            <CountryCode>IN</CountryCode>
            <City>'.$city_info->city_code.'</City>
            <GuestNationality>IN</GuestNationality>
            <HotelRatings>
                <HotelRating>1</HotelRating>
                <HotelRating>2</HotelRating>
                <HotelRating>3</HotelRating>
                <HotelRating>4</HotelRating>
                <HotelRating>5</HotelRating>
            </HotelRatings>
            <Rooms>
                <Room>
                    <Type>Room-1</Type>
                    <NoOfAdults>'.$param['total_adults'].'</NoOfAdults>
                    <NoOfChilds>'.$param['total_children'].'</NoOfChilds>';
        if(!empty($param['total_children'])) :
            $xmlcontent .= '<ChildrenAges>';
            foreach($param['age_of_child'] as $age){
              $xmlcontent .=  '<ChildAge>'.$age.'</ChildAge>';
            }
            $xmlcontent .= '</ChildrenAges>';
        endif;
        $xmlcontent .= '</Room>
            </Rooms>
            </Booking>
            </HotelFindRequest>
        ';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_URL, "http://test.xmlhub.com/testpanel.php/action/findhotel");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "XML=".$xmlcontent);
        $content=curl_exec($ch);

        $arr = simplexml_load_string($content, null, LIBXML_NOCDATA) or die("Error: Cannot create object");
        return $arr;
    }
}
