<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\Country;
use App\Model\State;
use PHPUnit\Framework\Constraint\Count;

class StateController extends BaseController
{
    public function index(Request $request, $company)
    {
        $query   = State::withCount('cities');
        $search  = $request->input('search');
        if (!empty($search['country'])) {
            $query->where('country_id', $search['country']);
        }
        if (!empty($search['keyword'])) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search['keyword'] . '%')
                    ->orWhere('short_name', 'LIKE', '%' . $search['keyword'] . '%');
            });
        }
        $records = $query->orderBy('name', 'ASC')->paginate(30);
        $countries  = Country::get();

        $title  = "States";
        $page   = "location.state";
        $data   = compact('page', 'title', 'records', 'countries', 'search', 'company');
        return view('backend/layout', $data);
    }

    public function edit(Request $request, $company, $id)
    {
        $query   = State::withCount('cities');
        $search  = $request->input('search');
        if (!empty($search['keyword'])) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search['keyword'] . '%')
                    ->orWhere('short_name', 'LIKE', '%' . $search['keyword'] . '%');
            });
        }
        $records = $query->orderBy('name', 'ASC')->paginate(30);
        $edit = State::findOrFail($id);

        $countries  = Country::get();

        $title     = "States";
        $page     = "location.state";
        $data     = compact('page', 'title', 'records', 'edit', 'search', 'company', 'countries');
        return view('backend/layout', $data);
    }

    public function store(Request $request, $company, $id = null)
    {
        $company = 'mahadev';
        $input = $request->input('record');
        $edit = [];
        if (!empty($id)) {
            $edit = State::find($id);
        }
        if (!empty($input)) {
            $isExistsQuery = State::where(function ($q) use ($input) {
                $q->where('name', $input['name'])
                    ->orWhere('short_name', $input['short_name']);
            });

            if (!empty($edit->id)) {
                $isExistsQuery->where('id', '!=', $edit->id);
            }

            $isExists = $isExistsQuery->count();

            if (!$isExists) :
                if (empty($edit->id)) {
                    $id   = State::insertGetId($input);
                    $message = "Success! New record has been added.";
                } else {
                    State::where('id', $id)->update($input);
                    $message = "Success! New record has been updated.";
                }
            else :
                return redirect(route('state.index', $company))->with('danger', 'State name already exists.');
            endif;
        }
        $check = $request->input('check');
        if (!empty($check)) {
            State::whereIn('id', $check)->delete();
            $message = "Success! Selected records has been removed.";
        }

        return redirect(route('state.index', $company))->with('success', $message);
    }

    public function exportcsv(Request $request, $company)
    {
        $records = State::get()->toArray();

        $fields = $values = [];
        foreach ($records as $i => $rec) {
            if ($i == 0) {
                $fields = array_keys($rec);
            }
            $values[] = array_values($rec);
        }

        $filename = "StateData_{$company}.csv";

        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");

        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, $fields);
        foreach ($values as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

    public function importcsv(Request $request, $company)
    {
        $file = $request->file('import_file');
        $fileName = $file->getPathName();
        $file = fopen($fileName, "r");
        $i = 0;
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
            if ($i == 0) {
                $fieldArr = $getData;
            } else {
                $row = array_combine($fieldArr, $getData);
                State::insert($row);
            }
            $i++;
        }
        return redirect(route('state.index', $company))->with('success', 'Success! Records has been imported.');
    }
}
