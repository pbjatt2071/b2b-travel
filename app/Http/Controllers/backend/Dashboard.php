<?php

namespace App\Http\Controllers\backend;

use App\BookTicket;
use App\Model\Airline;
use App\Model\Airport;
use App\Model\Tour;
use App\User;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class Dashboard extends BaseController
{
	public function index(Request $request)
	{
		$airlines  = Airline::get();
		$airlineArr = [
			''  => 'Select Airline'
		];
		foreach ($airlines as $c) {
			$airlineArr[$c->id] = $c->name;
		}

		$airports  = Airport::get();
		$airportArr = [
			''  => 'Select City or Airport'
		];
		foreach ($airports as $c) {
			$airportArr[$c->id] = $c->city->name;
		}

		$totalBooking = BookTicket::count();
		$pendingBooking = BookTicket::where('status', 'Pending')->count();
		$confirmBooking = BookTicket::where('status', 'Confirmed')->count();
		$cancelBooking = BookTicket::where('status', 'Canceled')->count();

		$alltour = Tour::where('disable', false)->get();
		$ongoingtour = Tour::where('disable', false)->get();
		$upcomingtour = Tour::where('disable', false)->get();



		$edit = $request->all();
		$request->replace($edit);
		$request->flash();
		$tours = [];
		if ($request->from != '' && $request->to != '' && $request->date != '' && $request->passenger != '') {
			$tours = Tour::where('from', $request->from)->where('to', $request->to)->where('date', $request->date)->where('active', 'true')->get();
			// dd($tours);
		}

		$title = "Dashboard";
		if (@auth()->user()->role_id == 1 || @auth()->user()->role_id == 4) {
			$page  = "dashboard";
			$data  = compact('page', 'title', 'totalBooking', 'pendingBooking', 'confirmBooking', 'cancelBooking', 'alltour', 'ongoingtour', 'upcomingtour');
		} else {
			$page  = "agentdash";
			$data  = compact('page', 'title', 'airlineArr', 'airportArr', 'tours', 'edit');
		}
		return view('backend/layout', $data);
	}
}
