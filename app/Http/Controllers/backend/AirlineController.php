<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Model\Airline;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\City;
use App\Model\State;
use App\Query;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\Constraint\Count;

class AirlineController extends BaseController
{
    public function index(Request $request, $company)
    {
        $query   = Airline::query();
        $search  = $request->input('search');
        if (!empty($search['keyword'])) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search['keyword'] . '%');
            });
        }
        $records = $query->orderBy('name', 'ASC')->paginate(10);

        $title  = "Airlines";
        $page   = "airline.index";
        $data   = compact('page', 'title', 'records', 'search', 'company');
        return view('backend/layout', $data);
    }

    public function add(Request $request, $company)
    {
        $title    = "Airlines";
        $page     = "airline.add";
        $data     = compact('page', 'title', 'company');
        return view('backend/layout', $data);
    }

    public function edit(Request $request, $company, $id = null)
    {
        $query   = Airline::query();
        $search  = $request->input('search');
        if (!empty($search['keyword'])) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search['keyword'] . '%');
            });
        }
        $records = $query->orderBy('name', 'ASC')->paginate(10);
        $edit = Airline::findOrFail($id);

        $title    = "Airlines";
        $page     = "airline.add";
        $data     = compact('page', 'title', 'records', 'edit', 'search', 'company');
        return view('backend/layout', $data);
    }

    public function store(Request $request, $company, $id = null)
    {
        $company = 'mahadev';
        $input = $request->input('record');
        $edit = [];
        if (!empty($id)) {
            $edit = Airline::find($id);
        }
        if (!empty($input)) {
            $isExistsQuery = Airline::where(function ($q) use ($input) {
                $q->where('name', $input['name']);
            });

            if (!empty($edit->id)) {
                $isExistsQuery->where('id', '!=', $edit->id);
            }

            $isExists = $isExistsQuery->count();

            if (!$isExists) {
                if (empty($edit->id)) {
                    $id   = Airline::insertGetId($input);
                    $message = "Success! New record has been added.";
                } else {
                    Airline::where('id', $id)->update($input);
                    $message = "Success! New record has been updated.";
                }
                if ($request->hasFile('image')) {
                    if (!empty($edit->image) && file_exists(storage_path() . "airline/" . $edit->image)) {
                        unlink(storage_path() . "airline/" . $edit->image);
                    }
                    $file           = $request->file('image');
                    $name            = 'IMG' . $id . '.' . $file->getClientOriginalExtension();

                    // $image = Image::make($file)->resize(150,60);
                    // $image = Image::make($file)->fit(300);
                    $image = Image::make($file)->resize(70, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    Storage::put('public/airline/'.$name, (string) $image->encode());

                    if (!empty($edit->image)) {
                        $name .= "?v=" . uniqid();
                    }
                    Airline::where('id', $id)->update(array('image' => $name));
                }
            } else {
                return redirect(route('airline.index', $company))->with('danger', 'Airline name already exists.');
            }
        }
        $check = $request->input('check');
        if (!empty($check)) {
            Airline::whereIn('id', $check)->delete();
            $message = "Success! Selected records has been removed.";
        }

        return redirect(route('airline.index', $company))->with('success', $message);
    }

    public function exportcsv(Request $request, $company)
    {
        $records = City::get()->toArray();

        $fields = $values = [];
        foreach ($records as $i => $rec) {
            if ($i == 0) {
                $fields = array_keys($rec);
            }
            $values[] = array_values($rec);
        }

        $filename = "CityData_{$company}.csv";

        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");

        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, $fields);
        foreach ($values as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

    public function importcsv(Request $request, $company)
    {
        $file = $request->file('import_file');
        $fileName = $file->getPathName();
        $file = fopen($fileName, "r");
        $i = 0;
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
            if ($i == 0) {
                $fieldArr = $getData;
            } else {
                $row = array_combine($fieldArr, $getData);
                City::insert($row);
            }
            $i++;
        }
        return redirect(route('city.index', $company))->with('success', 'Success! Records has been imported.');
    }
}
