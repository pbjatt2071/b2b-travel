<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Model\Airline;
use App\Model\Airport;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\City;
use App\Model\Country;
use App\Model\Pincode;
use App\Model\Role;
use App\Model\State;
use App\Model\Tour;
use App\Model\User;
use App\Model\UserDetail;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\Constraint\Count;

class TourController extends BaseController
{
    public function index(Request $request, $company)
    {
        $query   = Tour::latest();
        $airports  = Airport::get();
        $airlines  = Airline::get();
        $users  = User::get();
        $classes = [
            'first' => 'First Class',
            'business' => 'Business Class',
            'economy' => 'Economy Class',
        ];
        $search  = $request->input('search');
        $record  = $request->input('record');
        if (!empty($record['airport'])) {
            $query->where('from', $record['airport']);
            $query->where('to', $record['airport']);
        }
        // dd($request);
        if (!empty($record['airline'])) {
            $query->where('airline_id', $record['airline']);
        }
        if (!empty($record['user_id'])) {
            $query->where('user_id', $record['user_id']);
        }
        if (@auth()->user()->role_id == 4) {
            $query->where('user_id', auth()->user()->id);
        }
        if (!empty($record['user_id'])) {
            $query->where('user_id', $record['user_id']);
        }
        if (!empty($record['class'])) {
            $query->where('class', $record['class']);
        }
        if (!empty($search['keyword'])) {
            $query->where(function ($q) use ($search) {
                // $q->where('fname', 'LIKE', '%' . $search['keyword'] . '%')
                //     ->orWhere('lname', 'LIKE', '%' . $search['keyword'] . '%')
                //     ->orWhere('mobile', 'LIKE', '%' . $search['keyword'] . '%')
                //     ->orWhere('email', 'LIKE', '%' . $search['keyword'] . '%');
            });
        }
        // $records = $query->orderBy('fname', 'ASC')->paginate(30);
        $records = $query->paginate(30);

        $title  = 'Tours';
        $page   = "tour.index";
        $data   = compact('page', 'title', 'records', 'search', 'company', 'airports', 'airlines', 'classes', 'users');
        return view('backend/layout', $data);
    }

    public function add(Request $request, $company)
    {
        $airports  = Airport::get();
        $airlines  = Airline::get();
        $classes = [
            'first' => 'First Class',
            'business' => 'Business Class',
            'economy' => 'Economy Class',
        ];

        $title    = 'Tour';
        $page     = "tour.add";
        $data     = compact('page', 'title', 'company', 'airports', 'airlines', 'classes');
        return view('backend/layout', $data);
    }

    public function getDatesFromRange($start, $end, $format = 'Y-m-d')
    {
        // Declare an empty array
        $array = array();

        // Variable that store the date interval
        // of period 1 day
        // $interval = new DateInterval('P1D');

        // $realEnd = new DateTime($end);
        // $realEnd->add($interval);

        // $period = new DatePeriod(new DateTime($start), $realEnd);
        // Use loop to store date into array
        // foreach ($period as $date) {
        //     $array[] = $date;
        // }
        $dates = array();
        $current = strtotime($start);
        $date2 = strtotime($end);
        // dd($current);
        $stepVal = '+1 day';
        while ($current <= $date2) {
            $dates[] = date($format, $current);
            $current = strtotime($stepVal, $current);
        }
        // dd($dates);
        return $dates;

        // $date = displayDates('2019-11-10', '2019-11-20');
        // var_dump($date);
        // dd($array);

        // Return the array elements
        // return $array;
    }

    public function store(Request $request, $company)
    {
        $date = $this->getDatesFromRange($request->startdate, $request->enddate);
        $user = Auth::user();
        $company = 'mahadev';
        $input = $request->input('record');
        if (!empty($input)) {
            foreach ($date as $d) {
                // dd($d);
                // $isExistsQuery = Tour::where(function ($q) use ($input,$user) {
                //     $q->where('user_id', $user->id);
                //     $q->where('from', $input['from']);
                //     $q->where('to', $input['to']);
                //     $q->where('airline_id', $input['airline_id']);
                //     $q->where('class', $input['class']);
                // });
                // $isExists = $isExistsQuery->count();

                // if (!$isExists) {
                //     $id_userdetail   = UserDetail::insertGetId($input);
                //     $message = "Success! New record has been added.";
                // } else {
                //     User::where('id', $id)->update($input);
                //     $message = "Success! New record has been updated.";
                // }
                $input['new_adult'] = $input['adult'];
                $input['new_child'] = $input['child'];
                $input['new_infants'] = $input['infants'];
                $input['date'] = $d;
                $input['user_id'] = $user->id;
                // dd($input);
                $tourid   = Tour::insertGetId($input);
                $message = "Success! New record has been added.";
            }
        }

        return redirect(route('tour.index', $company))->with('success', $message);
    }

    public function edit(Request $request, $company, $id = null)
    {
        $edit = Tour::findOrFail($id);

        $airports  = Airport::get();
        $airlines  = Airline::get();
        $classes = [
            'first' => 'First Class',
            'business' => 'Business Class',
            'economy' => 'Economy Class',
        ];

        $title    = 'Edit Tour';
        $page     = "tour.edit";
        $data     = compact('page', 'title', 'edit', 'company', 'airports', 'airlines', 'classes');
        return view('backend/layout', $data);
    }

    public function update(Request $request, $company, $id)
    {
        $user = auth()->user();
        $company = 'mahadev';
        $input = $request->input('record');
        if (!empty($input)) {
            $input['user_id'] = $user->id;
            $tour   = Tour::where('id', $id)->update($input);
        }
        return redirect(route('tour.index', $company))->with('success', "Success!  Record has been updated.");
    }

    public function destroy(Request $request, $company)
    {
        $check = $request->input('check');
        if (!empty($check)) {
            Tour::whereIn('id', $check)->delete();
        }

        return redirect()->back()->with('success', "Success! Selected records has been removed.");
    }
    public function status(Request $request, $company, $id, $status)
    {
        $tour = Tour::find($id);
        $tour->active = $status;
        $tour->save();

        return redirect()->back()->with('success', "Success! Status has been updated.");
    }

    public function exportcsv(Request $request, $company)
    {
        $records = City::get()->toArray();

        $fields = $values = [];
        foreach ($records as $i => $rec) {
            if ($i == 0) {
                $fields = array_keys($rec);
            }
            $values[] = array_values($rec);
        }

        $filename = "CityData_{$company}.csv";

        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");

        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, $fields);
        foreach ($values as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

    public function importcsv(Request $request, $company)
    {
        $file = $request->file('import_file');
        $fileName = $file->getPathName();
        $file = fopen($fileName, "r");
        $i = 0;
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE) {
            if ($i == 0) {
                $fieldArr = $getData;
            } else {
                $row = array_combine($fieldArr, $getData);
                City::insert($row);
            }
            $i++;
        }
        return redirect(route('city.index', $company))->with('success', 'Success! Records has been imported.');
    }
}
