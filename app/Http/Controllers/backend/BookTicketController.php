<?php

namespace App\Http\Controllers\backend;

use App\BookTicket;
use App\BookTicketPassenger;
use App\Model\Airline;
use App\Model\Airport;
use App\Model\Tour;
use App\User;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

class BookTicketController extends BaseController
{
    public function index(Request $request, $company)
    {
        $query   = BookTicket::query();
        // $search  = $request->input('search');
        // if (!empty($search['keyword'])) {
        //     $query->where(function ($q) use ($search) {
        //         $q->where('name', 'LIKE', '%' . $search['keyword'] . '%');
        //     });
        // }
        $records = $query->latest()->paginate(30);

        $title  = "My Booking";
        $page   = "booking.mybooking";
        $data = compact('title', 'page', 'records');
        return view('backend/layout', $data);
    }

    public function ticket(Request $request, $company, $id)
    {
        $ticket   = BookTicket::findOrFail($id);
        // $search  = $request->input('search');
        // if (!empty($search['keyword'])) {
        //     $query->where(function ($q) use ($search) {
        //         $q->where('name', 'LIKE', '%' . $search['keyword'] . '%');
        //     });
        // }

        $title  = "Ticket Preview";
        $page   = "booking.ticket";
        $data = compact('title', 'page', 'ticket');
        return view('backend/layout', $data);
    }

    public function detail(Request $request, $company, $id)
    {
        $record   = BookTicket::findOrFail($id);
        // $search  = $request->input('search');
        // if (!empty($search['keyword'])) {
        //     $query->where(function ($q) use ($search) {
        //         $q->where('name', 'LIKE', '%' . $search['keyword'] . '%');
        //     });
        // }
        $title  = "Ticket Details";
        $page   = "booking.ticketdetail";
        $data = compact('title', 'page', 'record');
        return view('backend/layout', $data);
    }
}
