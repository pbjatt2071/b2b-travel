<?php
namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\TimeslotModel AS Tslot;
use DB;

class ScheduleController extends BaseController {
    public function index(Request $request) {
        $current_hour = date('H', time());
        $current_date = date('d', time());

        $timeslots      = Tslot::where('tslot_min_time', '>', $current_hour)->get();
        $alltimeslots   = Tslot::get();
        $dates     = [];

        // $current_date = $timeslots->isEmpty() ? date('y-m-d', time());
        $start_count = $timeslots->isEmpty() ? 1 : 0;
        for($i = $start_count; $i <= $start_count + 9; $i++) {
            $dates[] = [
                'month' => date('M', strtotime('+'.$i.' days')),
                'date'  => date('d', strtotime('+'.$i.' days')),
                'day'   => date('D', strtotime('+'.$i.' days')),
                'fulldate'  => date('Y-m-d', strtotime('+'.$i.' days'))
            ];
        }

        if($timeslots->isEmpty()) {
            $timeslots = $alltimeslots;
        }

        $re = [
            'status'        => true,
            'timeslots'     => $timeslots,
            'dates'         => $dates,
            'alltimeslots'  => $alltimeslots
        ];

        return response()->json($re);
    }
}
