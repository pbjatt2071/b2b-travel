<?php
namespace App\Http\Controllers\api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Model\OrderModel as Order;
use App\Model\OrderProductModel as OrderProduct;
use App\Model\UserModel as User;
use App\Model\CartModel as Cart;
use App\Model\WalletModel as Wallet;
use DB;

class OrderController extends BaseController {
    public function add(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];

        if(!empty($post['order_uid']) && !empty($post['order_address']) && !empty($post['order_total']) && !empty($post['order_schedule_date']) && !empty($post['order_schedule_time'])) {
            $post['order_created_on'] = date('Y-m-d H:i:s', time());

            Order::insert( $post );
            $id = DB::getPdo()->lastInsertId();

            $carts = Cart::where('cart_uid', $post['order_uid'])->get();

            foreach($carts as $cart) {
                $arr = [
                    'opro_sid'          => $cart->cart_sid,
                    'opro_qty'          => 1,
                    'opro_oid'          => $id,
                    'opro_time'         => '0',
                    'opro_start_time'   => '00:00:00',
                    'opro_end_time'     => '00:00:00'
                ];
                OrderProduct::insert($arr);
            }

            $order = Order::join('user_addresses AS uaddr', 'orders.order_address', 'uaddr.uaddr_id')
                        ->join('users AS u', 'orders.order_uid', 'u.user_id')
                        ->leftJoin('coupons AS c', 'orders.order_cid', 'c.coupon_id')
                        ->where('order_id', $id)->first();
            $products = OrderProduct::join('services AS s', 's.service_id', 'order_products.opro_sid')
                            ->where('opro_oid', $id)
                            ->get();

            foreach($products as $k => $p) {
                $products[$k]->service_image    = !empty($p->service_image) ? url('imgs/services/'.$p->service_image) : '';
                $products[$k]->service_icon     = !empty($p->service_icon) ? url('imgs/services/'.$p->service_icon) : '';
            }

            $order->products = $products;

            $service_code = rand(1000, 9999);
            $order->order_service_code = $service_code;

            $order->order_subtotal  = (!empty($order->coupon_type) && $order->coupon_type == "Discount") ? $order->order_total + $order->order_discount : $order->order_total;
            $order->order_subtotal += $order->order_wallet_amt;

            Order::where('order_id', $id)->update(['order_service_code' => $service_code]);

            if(!empty($post['order_wallet_amt'])) {
                Wallet::insert([
                    'wallet_uid'        => $post['order_uid'],
                    'wallet_remarks'    => 'Order No. '.sprintf("%06d", $id).' placed.',
                    'wallet_amount'     => $post['order_wallet_amt'],
                    'wallet_type'       => 'Debit',
                    'wallet_created_on' => date('Y-m-d H:i:s', time())
                ]);
            }

            $re = [
                'status'    => TRUE,
                'message'   => 'order has been placed.',
                'order_id'  => $id,
                'order_info'=> $order
            ];
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }

        return response()->json($re);
    }
    public function edit(Request $request, $order_id) {
        $post = $request->input();
        if(!empty($post['order_schedule_date']) && !empty($post['order_schedule_time'])) {
            Order::where('order_id', $order_id)->update($post);

            $re = [
                'status'    => TRUE,
                'message'   => 'Order updated.'
            ];
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }

        return response()->json( $re );
    }
    public function edit_order_cart(Request $request, $endservice = 0) {
        $post = $request->input();

        if(!empty($post['order_id'])) {
            $post['service_time'] = !empty($post['service_time']) ? $post['service_time'] : 0;
            Order::where('order_id', $post['order_id'])->update(['order_status' => 'In Progress']);

            $qty = ceil( $post['service_time'] / 60 );

            $arr = [
                'opro_qty'  => $qty,
                'opro_time' => $post['service_time']
            ];

            if(!empty($post['start_time'])) {
                $arr['opro_start_time'] = $post['start_time'];
            }

            if(!empty($post['end_time'])) {
                $arr['opro_end_time'] = $post['end_time'];
            }

            $order_info = Order::
                join('order_products AS op', 'orders.order_id', 'op.opro_oid')
                ->where('order_id', $post['order_id'])
                ->join('services AS s', 'op.opro_sid', 's.service_id')
                ->leftJoin('coupons AS c', 'c.coupon_id', 'orders.order_cid')
                ->first();

            OrderProduct::where('opro_oid', $post['order_id'])->update($arr);

            $orderPro = OrderProduct::join('services AS s', 'order_products.opro_sid', 's.service_id')->where('opro_oid', $post['order_id'])->first();
            $amount   = $orderPro->service_cost * $orderPro->opro_qty;

            $discount = 0;
            if(!empty($order_info->order_cid)) {
                if($order_info->coupon_discount_type == "Percent") {
                    $discount = $amount * $order_info->coupon_discount / 100;
                } else {
                    $discount = $order_info->coupon_discount;
                }
            }

            $amount -= $discount;
            $amount -= $order_info->order_wallet_amt;

            Order::where('order_id', $post['order_id'])->update(['order_total' => $amount, 'order_discount' => $discount]);

            $re = [
                'status'    => TRUE,
                'message'   => "Data updated"
            ];

            if($endservice) {

                Order::where('order_id', $post['order_id'])->update([
                    'order_time_end'    => 1
                ]);

                $order_info = Order::
                    join('order_products AS op', 'orders.order_id', 'op.opro_oid')
                    ->where('order_id', $post['order_id'])
                    ->join('services AS s', 'op.opro_sid', 's.service_id')
                    ->leftJoin('coupons AS c', 'c.coupon_id', 'orders.order_cid')
                    ->first();

                $order_info->order_subtotal  = (!empty($order_info->coupon_type) && $order_info->coupon_type == "Discount") ? $order_info->order_total + $order_info->order_discount : $order_info->order_total;
                $order_info->order_subtotal += $order_info->order_wallet_amt;

                // $re['happy_code'] = $happy_code;
                $re['order_info'] = $order_info;
            }
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }

        return response()->json( $re );
    }
    public function cancel(Request $request, $order_id) {

        Order::where('order_id', $order_id)->update(['order_status' => 'Cancelled']);

        $re = [
            'status'    => TRUE,
            'message'   => 'Order cancelled.'
        ];

        return response()->json( $re );
    }
    public function completed(Request $request, $order_id, $payment_mode, $txn_id = "") {
        $isExists = Order::where('order_id', $order_id)->count();

        if($isExists) :
            $happy_code = rand(1000, 9999);

            $arr = [
                'order_status'       => 'Completed',
                'order_happy_code'   => $happy_code,
                'order_payment_type' => $payment_mode,
                'order_txn_id'       => $txn_id
            ];
            Order::where('order_id', $order_id)->update($arr);

            $re = [
                'status'    => TRUE,
                'happy_code'=> $happy_code,
                'message'   => 'Order completed marked.'
            ];

        else:
            $re = [
                'status'    => FALSE,
                'message'   => 'Order ID is not correct.'
            ];
        endif;

        return response()->json( $re );
    }
    public function fill_happy_code($order_id, $happy_code) {
        $is_correct = Order::where('order_id', $order_id)->where('order_happy_code', $happy_code)->count();
        if($is_correct) {
            Order::where('order_id', $order_id)->update(['order_is_happy' => 'Y']);
            $re = [
                'status'    => TRUE,
                'message'   => 'Happy code is matched.'
            ];
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Happy code is not matched.'
            ];
        }

        return response()->json( $re );
    }
    public function happy_codes(Request $request, $user_id) {
        $orders = Order::join('order_products AS op', 'orders.order_id', 'op.opro_oid')->join('services AS s', 'op.opro_sid', 's.service_id')->where('order_uid', $user_id)->where('order_is_happy', 'N')->select('order_id', 'order_happy_code', 'order_service_code', 'service_name')->orderBy('order_id', 'DESC')->get();

        if(!$orders->isEmpty()) {
            $re = [
                'status'    => TRUE,
                'message'   => $orders->count()." record(s) found.",
                'data'      => $orders
            ];
        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'No record(s) found.'
            ];
        }

        return response()->json( $re );
    }
    public function my_order(Request $request) {
        $post = $request->isMethod('post') ? $request->input() : [];
        if(!empty($post['user_id'])) {
            $orders = Order::leftJoin('coupons AS c', 'orders.order_cid', 'c.coupon_id')->where('order_uid', $post['user_id'])->orderBy('order_id', 'DESC')->get();

            if(!$orders->isEmpty()) {
                foreach($orders as $key => $o) {
                    $products = OrderProduct::join('services AS s', 's.service_id', 'order_products.opro_sid')
                                    ->where('opro_oid', $o->order_id)
                                    ->get();

                    foreach($products as $k => $p) {
                        $products[$k]->service_image    = !empty($p->service_image) ? url('imgs/services/'.$p->service_image) : '';
                        $products[$k]->service_icon     = !empty($p->service_icon) ? url('imgs/services/'.$p->service_icon) : '';
                    }

                    $orders[$key]->services           = $products;
                    $orders[$key]->order_created_date = date('d-m-Y', strtotime( $o->order_created_on ));
                    $orders[$key]->order_created_time = date('h:i A', strtotime( $o->order_created_on ));

                    $orders[$key]->order_subtotal     = (!empty($o->coupon_type) && $o->coupon_type == "Discount") ? $o->order_total + $o->order_discount : $o->order_total;
                    $orders[$key]->order_subtotal     += $o->order_wallet_amt;
                }
                $re = [
                    'status'    => TRUE,
                    'message'   => $orders->count().' record(s) found.',
                    'data'      => $orders
                ];
            } else {
                $re = [
                    'status'    => FALSE,
                    'message'   => 'No record(s) found.'
                ];
            }


        } else {
            $re = [
                'status'    => FALSE,
                'message'   => 'Required field(s) missing.'
            ];
        }
        return response()->json($re);
    }
    public function order_single(Request $request, $order_id) {
        $order      = Order::join('user_addresses AS uaddr', 'orders.order_address', 'uaddr.uaddr_id')
                            ->leftJoin('users AS u', 'orders.order_vid', 'u.user_id')
                            ->leftJoin('partners AS p', 'u.user_id', 'p.partner_uid')
                            ->leftJoin('coupons AS c', 'c.coupon_id', 'orders.order_cid')
                            ->where('order_id', $order_id)->first();

        if(!empty($order->user_image)) {
            $order->user_image   = url('imgs/users/'.$order->user_image);
        }

        if(!empty($order->partner_uid_file)) {
            $order->partner_uid_file   = url('uploads/users/'.$order->partner_uid_file);
        }

        if(!empty($order->partner_pan_file)) {
            $order->partner_pan_file   = url('uploads/users/'.$order->partner_pan_file);
        }

        if(!empty($order->partner_police_verification)) {
            $order->partner_police_verification   = url('uploads/users/'.$order->partner_police_verification);
        }

        if(!empty($order->partner_id_file)) {
            $order->partner_id_file   = url('uploads/users/'.$order->partner_id_file);
        }

        $order->subtotal     = (!empty($order->coupon_type) && $order->coupon_type == "Discount") ? $order->order_total + $order->order_discount : $order->order_total;
        $order->subtotal    += $order->order_wallet_amt;

        unset($order->partner);

        $prefix                = "SAP".substr( strtoupper( $order->user_name ), 0, 2 ).'';
        $order->employee_id    = sprintf('%s%03d', $prefix, $order->user_id);

        $products              = OrderProduct::join('services AS s', 's.service_id', 'order_products.opro_sid')
                        ->where('opro_oid', $order_id)
                        ->get();

        foreach($products as $k => $p) {
            $products[$k]->service_image    = !empty($p->service_image) ? url('imgs/services/'.$p->service_image) : '';
            $products[$k]->service_icon     = !empty($p->service_icon) ? url('imgs/services/'.$p->service_icon) : '';
        }

        $re = [
            'status'    => TRUE,
            'data'      => $order,
            'products'  => $products
        ];
        return response()->json($re);
    }
}
