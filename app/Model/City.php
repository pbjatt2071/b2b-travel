<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function state()
    {
        return $this->hasOne('App\Model\State', 'id', 'state_id');
    }
    public function pincodes()
    {
        return $this->hasMany('App\Model\Pincode', 'city_id', 'id');
    }
}
