<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    public function country() {
        return $this->hasOne('App\Model\Country', 'country_id', 'user_country');
    }
    public function state() {
        return $this->hasOne('App\Model\State', 'state_id', 'user_state');
    }
    public function city() {
        return $this->hasOne('App\Model\City', 'city_id', 'user_city');
    }
    public function pincode() {
        return $this->hasOne('App\Model\Pincode', 'pin_id', 'user_pincode');
    }
}
