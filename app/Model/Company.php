<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Company extends Model {
    public      $timestamps     = false;
    protected   $table          = 'companies';
    protected   $primaryKey     = 'company_id';

    public function user() {
        return $this->hasOne('App\Model\User', 'user_id', 'company_uid');
    }
}
