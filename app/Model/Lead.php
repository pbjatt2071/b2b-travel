<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model {
    public      $timestamps     = false;
    protected   $table          = 'leads';
    protected   $primaryKey     = 'lead_id';

    public function country() {
        return $this->hasOne('App\Model\Country', 'country_id', 'lead_destination');
    }
    public function state() {
        return $this->hasOne('App\Model\State', 'state_id', 'lead_state');
    }
    public function city() {
        return $this->hasOne('App\Model\City', 'city_id', 'lead_city');
    }
    public function pincode() {
        return $this->hasOne('App\Model\Pincode', 'pin_id', 'lead_pincode');
    }
    public function user() {
        return $this->hasOne('App\Model\User', 'user_id', 'lead_uid');
    }
    public function staff() {
        return $this->hasOne('App\Model\User', 'user_id', 'lead_staff');
    }
}
