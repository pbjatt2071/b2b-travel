<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pincode extends Model
{
    public function city()
    {
        return $this->hasOne('App\Model\City', 'id', 'city_id');
    }
}
