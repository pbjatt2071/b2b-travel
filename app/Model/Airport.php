<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
    protected $with = ['city'];
    public function city()
    {
        return $this->hasOne('App\Model\City', 'id', 'city_id');
    }
}
