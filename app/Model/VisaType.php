<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VisaType extends Model {
    public      $timestamps     = false;
    protected   $table          = 'visa_type';
    protected   $primaryKey     = 'vtype_id';
}
