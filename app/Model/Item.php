<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Item extends Model {
    public      $timestamps     = false;
    protected   $table          = 'items';
    protected   $primaryKey     = 'item_id';

    public function unit() {
        return $this->hasOne('App\Model\Unit', 'unit_id', 'item_unit');
    }
}
