<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    public function cities()
    {
        return $this->hasMany('App\Model\City', 'state_id', 'id');
    }
    public function country()
    {
        return $this->hasOne('App\Model\Country', 'id', 'country_id');
    }
}
