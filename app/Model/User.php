<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

    public function company() {
        return $this->hasOne('App\Model\Company', 'company_uid', 'company_id');
    }
    public function role() {
        return $this->hasOne('App\Model\Role', 'id', 'role_id');
    }
    
}
