<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    protected $with = ['user','froms','tos','airlines'];
    public function user()
    {
        return $this->hasOne('App\Model\User', 'id', 'user_id');
    }
    public function froms()
    {
        return $this->hasOne('App\Model\Airport', 'id', 'from');
    }
    public function tos()
    {
        return $this->hasOne('App\Model\Airport', 'id', 'to');
    }
    public function airlines()
    {
        return $this->hasOne('App\Model\Airline', 'id', 'airline_id');
    }
}
