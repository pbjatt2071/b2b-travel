<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model {
    public      $timestamps     = false;
    protected   $table          = 'hotels';
    protected   $primaryKey     = 'hotel_id';

    public function user() {
        return $this->hasOne('App\Model\User', 'user_id', 'hotel_uid');
    }
}
