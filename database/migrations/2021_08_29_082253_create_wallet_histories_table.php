<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('wallet_id')->unsigned()->nullable();
            $table->foreign('wallet_id')->references('id')->on('wallets');
            $table->integer('amount')->default(0);
            $table->string('detail')->nullable();
            $table->enum('payment', ['credit', 'deposit'])->default('credit');
            $table->enum('payment_mode', ['cash', 'online'])->default('online');
            $table->enum('payment_status', ['paid', 'due'])->default('due');
            $table->enum('added_by', ['self', 'admin'])->default('self');
            $table->string('txn_id')->nullable();
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallet_histories');
    }
}
