<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('from')->unsigned()->nullable();
            $table->foreign('from')->references('id')->on('airports')->onDelete('cascade');
            $table->bigInteger('to')->unsigned()->nullable();
            $table->foreign('to')->references('id')->on('airports')->onDelete('cascade');
            $table->bigInteger('airline_id')->unsigned()->nullable();
            $table->foreign('airline_id')->references('id')->on('airlines')->onDelete('cascade');
            $table->datetime('takeoff')->nullable();
            $table->datetime('landing')->nullable();
            $table->datetime('date')->nullable();
            $table->string('seat')->nullable();
            $table->string('adult')->nullable();
            $table->string('child')->nullable();
            $table->string('infants')->nullable();
            $table->string('flight_number')->nullable();
            $table->string('before_disable')->nullable();
            $table->enum('class', ['first', 'business','economy'])->default('economy');
            $table->enum('active', ['true', 'false'])->default('false');
            $table->enum('disable', ['true', 'false'])->default('false');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
