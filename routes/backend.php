<?php
Route::get('{company}/user/logout', 'backend\UserController@logout')->name('logout');

Route::get('{company}', 'backend\Dashboard@index')->name('dashboard');

Route::any('{company}/page/edit/{slug?}', 'backend\Page@edit');

// User
// Route::any('{company}/user/add/{role?}/{id?}', 'backend\User@add');
// Route::any('{company}/user/{role?}', 'backend\User@index');
// Route::any('{company}/change-password', 'backend\User@change_password');

// Account
// Route::any('{company}/account/{role}', 'backend\AccountController@index');
// Route::any('{company}/account/{role}/add/{id?}', 'backend\AccountController@add');

// Vehicle
// Route::any('{company}/vehicle', 'backend\VehicleController@index');
// Route::any('{company}/vehicle/add/{id?}', 'backend\VehicleController@add');

// Add-on Items
// Route::any('{company}/unit', 'backend\UnitController@index');
// Route::any('{company}/add-on-item', 'backend\ItemController@index');
// Route::any('{company}/add-on-item/add/{id?}', 'backend\ItemController@add');

// Activity
// Route::any('{company}/activity', 'backend\ActivityController@index');
// Route::any('{company}/activity/add/{id?}', 'backend\ActivityController@add');

// Leads
// Route::any('{company}/leads', 'backend\LeadController@index');
// Route::any('{company}/leads/add/{id?}', 'backend\LeadController@add');
// Route::any('{company}/leads/follow-up/{type}', 'backend\LeadController@follow_up');
// Route::any('{company}/leads/management', 'backend\LeadController@management');

// Visa
// Route::any('{company}/visa', 'backend\VisaController@index');
// Route::any('{company}/visa/add/{id?}', 'backend\VisaController@add');
// Route::any('{company}/visa/application/{id?}', 'backend\VisaController@application');
// Route::any('{company}/visa/status/{id}/{status}', 'backend\VisaController@status');

// Trip

// Route::group(['pref, 'middleware' => 'auth'], function () {
Route::get('{company}/tour', 'backend\TourController@index')->name('tour.index');
Route::get('{company}/tour/add', 'backend\TourController@add')->name('tour.add');
Route::post('{company}/tour/add', 'backend\TourController@store')->name('tour.store');
Route::get('{company}/tour/edit/{id}', 'backend\TourController@edit')->name('tour.edit');
Route::post('{company}/tour/edit/{id}', 'backend\TourController@update')->name('tour.update');
Route::post('{company}/tour', 'backend\TourController@destroy')->name('tour.destroy');
Route::get('{company}/tour/status/{id}/{status}', 'backend\TourController@status')->name('tour.status');

// Airport
Route::get('{company}/airport', 'backend\AirportController@index')->name('airport.index');
Route::post('{company}/airport', 'backend\AirportController@store')->name('airport.store');
Route::get('{company}/airport/{id}', 'backend\AirportController@edit')->name('airport.edit');
Route::post('{company}/airport/{id}', 'backend\AirportController@store')->name('airport.update');

// User
Route::get('{company}/user/{role}', 'backend\UserController@index')->name('user.index');
Route::get('{company}/user/add/{role}', 'backend\UserController@add')->name('user.add');
Route::post('{company}/user/add/{role}', 'backend\UserController@store')->name('user.store');
Route::get('{company}/user/edit/{role}/{id}', 'backend\UserController@edit')->name('user.edit');
Route::post('{company}/user/update/{role}/{id}', 'backend\UserController@store')->name('user.update');
Route::get('{company}/user/status/{id}/{status}', 'backend\UserController@status')->name('user.status');

// Location
Route::get('{company}/country', 'backend\CountryController@index')->name('country.index');
Route::post('{company}/country', 'backend\CountryController@store')->name('country.store');
Route::get('{company}/country/{id}', 'backend\CountryController@edit')->name('country.edit');
Route::post('{company}/country/{id}', 'backend\CountryController@store')->name('country.update');
Route::post('{company}/country/csv/import', 'backend\CountryController@importcsv')->name('country.importcsv');
Route::get('{company}/country/csv/export', 'backend\CountryController@exportcsv')->name('country.exportcsv');

Route::get('{company}/state', 'backend\StateController@index')->name('state.index');
Route::post('{company}/state', 'backend\StateController@store')->name('state.store');
Route::get('{company}/state/{id}', 'backend\StateController@edit')->name('state.edit');
Route::post('{company}/state/{id}', 'backend\StateController@store')->name('state.update');
Route::post('{company}/state/csv/import', 'backend\StateController@importcsv')->name('state.importcsv');
Route::get('{company}/state/csv/export', 'backend\StateController@exportcsv')->name('state.exportcsv');

Route::get('{company}/city', 'backend\CityController@index')->name('city.index');
Route::post('{company}/city', 'backend\CityController@store')->name('city.store');
Route::get('{company}/city/{id}', 'backend\CityController@edit')->name('city.edit');
Route::post('{company}/city/{id}', 'backend\CityController@store')->name('city.update');
Route::post('{company}/city/csv/import', 'backend\CityController@importcsv')->name('city.importcsv');
Route::get('{company}/city/csv/export', 'backend\CityController@exportcsv')->name('city.exportcsv');

Route::get('{company}/pincode', 'backend\PincodeController@index')->name('pincode.index');
Route::post('{company}/pincode', 'backend\PincodeController@store')->name('pincode.store');
Route::get('{company}/pincode/{id}', 'backend\PincodeController@edit')->name('pincode.edit');
Route::post('{company}/pincode/{id}', 'backend\PincodeController@store')->name('pincode.update');
Route::post('{company}/pincode/csv/import', 'backend\PincodeController@importcsv')->name('pincode.importcsv');
Route::get('{company}/pincode/csv/export', 'backend\PincodeController@exportcsv')->name('pincode.exportcsv');

// Airlines
Route::get('{company}/airline', 'backend\AirlineController@index')->name('airline.index');
Route::get('{company}/airline/add', 'backend\AirlineController@add')->name('airline.add');
Route::post('{company}/airline/add', 'backend\AirlineController@store')->name('airline.store');
Route::get('{company}/airline/add/{id}', 'backend\AirlineController@edit')->name('airline.edit');
Route::post('{company}/airline/add/{id}', 'backend\AirlineController@store')->name('airline.update');


Route::any('{company}/department/{id?}', 'backend\DepartmentController@index');

Route::any('{company}/create-fixed-tour/{id?}', 'backend\TripController@add');
Route::any('{company}/hotels', 'backend\TripController@select_hotel');
// My Orders
Route::get('{company}/my-booking', 'backend\BookTicketController@index')->name('bookticket.index');
Route::get('{company}/ticket/details/{id}', 'backend\BookTicketController@detail')->name('bookticket.detail');
Route::get('{company}/booking/ticket/{id}', 'backend\BookTicketController@ticket')->name('bookticket.ticket');

// Booking
Route::get('{company}/booking', 'backend\BookingController@index')->name('booking.index');
Route::get('{company}/booking/{id}', 'backend\BookingController@edit')->name('booking.create');
Route::post('{company}/booking/{id}', 'backend\BookingController@update')->name('booking.store');

// Profile
Route::any('{company}/edit-profile', 'backend\UserController@edit_profile');
Route::any('{company}/change-password', 'backend\UserController@change_password');

// Settings
Route::any('{company}/general-setting', 'backend\SettingController@index');
// });
Route::any('{company}/ajax/user_login', 'backend\Ajax@user_login');
Route::any('{company}/ajax/{action}', 'backend\Ajax@index');
