-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2019 at 11:50 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `service_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `sap_carts`
--

CREATE TABLE `sap_carts` (
  `cart_uid` int(11) NOT NULL,
  `cart_sid` int(11) NOT NULL COMMENT 'Sub service ID'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sap_carts`
--

INSERT INTO `sap_carts` (`cart_uid`, `cart_sid`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sap_cities`
--

CREATE TABLE `sap_cities` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_short_name` varchar(255) NOT NULL,
  `city_country` int(11) NOT NULL,
  `city_state` int(11) NOT NULL,
  `city_is_deleted` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sap_cities`
--

INSERT INTO `sap_cities` (`city_id`, `city_name`, `city_short_name`, `city_country`, `city_state`, `city_is_deleted`) VALUES
(1, 'Jodhpur', 'JOD', 1, 1, 'N'),
(2, 'Kota', 'KOT', 1, 1, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `sap_countries`
--

CREATE TABLE `sap_countries` (
  `country_id` int(11) NOT NULL,
  `country_short_name` varchar(5) NOT NULL,
  `country_name` varchar(100) NOT NULL DEFAULT '',
  `country_code` varchar(5) NOT NULL,
  `country_is_deleted` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sap_countries`
--

INSERT INTO `sap_countries` (`country_id`, `country_short_name`, `country_name`, `country_code`, `country_is_deleted`) VALUES
(1, 'IN', 'India', '91', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `sap_coupons`
--

CREATE TABLE `sap_coupons` (
  `coupon_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `coupon_valid_from` date NOT NULL,
  `coupon_valid_upto` date NOT NULL,
  `coupon_never_end` int(1) NOT NULL,
  `coupon_type` enum('Discount','Cashback') NOT NULL DEFAULT 'Discount',
  `coupon_discount` double NOT NULL,
  `coupon_description` text NOT NULL,
  `coupon_is_user_limit` int(1) NOT NULL,
  `coupon_limit_no` double NOT NULL,
  `coupon_is_total_uses` int(1) NOT NULL,
  `coupon_total_uses` double NOT NULL,
  `coupon_is_total_cart` int(1) NOT NULL,
  `coupon_min_cart_total` double NOT NULL,
  `coupon_image` varchar(255) NOT NULL,
  `coupon_is_deleted` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sap_coupons`
--

INSERT INTO `sap_coupons` (`coupon_id`, `coupon_code`, `coupon_valid_from`, `coupon_valid_upto`, `coupon_never_end`, `coupon_type`, `coupon_discount`, `coupon_description`, `coupon_is_user_limit`, `coupon_limit_no`, `coupon_is_total_uses`, `coupon_total_uses`, `coupon_is_total_cart`, `coupon_min_cart_total`, `coupon_image`, `coupon_is_deleted`) VALUES
(1, 'DISC30', '2019-09-08', '0000-00-00', 1, 'Discount', 30, 'You can get upto 30% discount at min order of Rs. 500/-', 1, 1000, 1, 1, 1, 500, 'IMG1.jpg', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `sap_services`
--

CREATE TABLE `sap_services` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `service_slug` varchar(255) NOT NULL,
  `service_image` varchar(255) NOT NULL,
  `service_is_deleted` enum('Y','N') DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sap_services`
--

INSERT INTO `sap_services` (`service_id`, `service_name`, `service_slug`, `service_image`, `service_is_deleted`) VALUES
(1, 'Appliances', 'appliances', 'MIMG1.jpg', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `sap_sliders`
--

CREATE TABLE `sap_sliders` (
  `slider_id` int(11) NOT NULL,
  `slider_name` varchar(255) NOT NULL,
  `slider_image` varchar(255) NOT NULL,
  `slider_is_deleted` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sap_sliders`
--

INSERT INTO `sap_sliders` (`slider_id`, `slider_name`, `slider_image`, `slider_is_deleted`) VALUES
(1, 'Slider 1', 'IMG1.jpg', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `sap_states`
--

CREATE TABLE `sap_states` (
  `state_id` int(11) NOT NULL,
  `state_name` varchar(255) NOT NULL,
  `state_short_name` varchar(5) NOT NULL,
  `state_country` int(11) NOT NULL,
  `state_is_deleted` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sap_states`
--

INSERT INTO `sap_states` (`state_id`, `state_name`, `state_short_name`, `state_country`, `state_is_deleted`) VALUES
(1, 'Rajasthan', 'Raj', 1, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `sap_subservices`
--

CREATE TABLE `sap_subservices` (
  `subservice_id` int(11) NOT NULL,
  `subservice_name` varchar(120) NOT NULL,
  `subservice_slug` varchar(255) NOT NULL,
  `subservice_tagline` varchar(255) NOT NULL,
  `subservice_wish` text NOT NULL,
  `subservice_sid` int(11) NOT NULL COMMENT 'Service ID',
  `subservice_image` varchar(255) NOT NULL,
  `subservice_price` decimal(20,2) NOT NULL,
  `subservice_is_deleted` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sap_subservices`
--

INSERT INTO `sap_subservices` (`subservice_id`, `subservice_name`, `subservice_slug`, `subservice_tagline`, `subservice_wish`, `subservice_sid`, `subservice_image`, `subservice_price`, `subservice_is_deleted`) VALUES
(1, 'Servicing', 'servicing', 'Service | Repair | Installation', 'a:5:{i:0;s:41:\"Prices shown are for labour charges only.\";i:1;s:47:\"Consumable and parts (if used) will be charged.\";i:2;s:44:\"We provide 30 days warranty on the services.\";i:3;s:40:\"For any repair work quote will be given.\";i:4;s:23:\"What your wish include.\";}', 1, 'IMG1.jpg', '200.00', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `sap_users`
--

CREATE TABLE `sap_users` (
  `user_id` int(11) NOT NULL,
  `user_fname` varchar(50) NOT NULL,
  `user_lname` varchar(50) NOT NULL,
  `user_name` varchar(120) NOT NULL,
  `user_login` varchar(100) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_gender` enum('Male','Female','Other') NOT NULL DEFAULT 'Male',
  `user_image` varchar(255) NOT NULL,
  `user_dob` date NOT NULL,
  `user_country` int(11) NOT NULL,
  `user_state` int(11) NOT NULL,
  `user_city` int(11) NOT NULL,
  `user_pincode` int(11) NOT NULL,
  `user_address1` varchar(255) NOT NULL,
  `user_address2` varchar(255) NOT NULL,
  `user_mobile` varchar(20) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_role` enum('super-admin','admin','customer','serviceman') NOT NULL DEFAULT 'customer',
  `user_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_source` enum('Android','Web','iPhone') NOT NULL DEFAULT 'Web',
  `user_is_login` enum('Y','N') NOT NULL DEFAULT 'N',
  `user_is_deleted` enum('Y','N') DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sap_users`
--

INSERT INTO `sap_users` (`user_id`, `user_fname`, `user_lname`, `user_name`, `user_login`, `user_password`, `user_gender`, `user_image`, `user_dob`, `user_country`, `user_state`, `user_city`, `user_pincode`, `user_address1`, `user_address2`, `user_mobile`, `user_email`, `user_role`, `user_created_on`, `user_source`, `user_is_login`, `user_is_deleted`) VALUES
(1, '', '', '', 'rudraksha_tech', '$2y$12$cC2OF9qnZrHS0HQ.z8xiiOjHzxCcCY1l4mMOb7VWF/Epjs.smz4FK', 'Male', '', '0000-00-00', 0, 0, 0, 0, '', '', '', '', 'super-admin', '2019-08-29 05:20:26', 'Web', 'N', 'N'),
(2, 'Dipesh', '', 'Dipesh', 'service_app', '$2y$12$cC2OF9qnZrHS0HQ.z8xiiOjHzxCcCY1l4mMOb7VWF/Epjs.smz4FK', 'Male', '', '0000-00-00', 0, 0, 0, 0, '', '', '', '', 'admin', '2019-08-29 05:20:26', 'Web', 'N', 'N'),
(4, 'Sudarshan', 'Dubaga', 'Sudarshan Dubaga', '9636150842', '', 'Male', 'IMG4.jpg', '1993-08-11', 1, 0, 1, 342011, '11-B Andheri West', 'Johari Market, Jaipur', '9636150842', 'sudarshandubaga@gmail.com', 'customer', '2019-08-30 23:57:47', 'Web', 'N', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `sap_user_addresses`
--

CREATE TABLE `sap_user_addresses` (
  `uaddr_id` int(11) NOT NULL,
  `uaddr_name` varchar(255) NOT NULL,
  `uaddr_address1` varchar(255) NOT NULL,
  `uaddr_address2` varchar(255) NOT NULL,
  `uaddr_uid` int(11) NOT NULL COMMENT 'User ID'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sap_user_addresses`
--

INSERT INTO `sap_user_addresses` (`uaddr_id`, `uaddr_name`, `uaddr_address1`, `uaddr_address2`, `uaddr_uid`) VALUES
(1, 'Sudarshan Dubaga', '36, Rajeev Gandhi Colony', 'Bhati Circle, Ratanada, Jodhpur 342011', 4),
(2, 'Sudarshan Dubaga', '11-B Andheri West', 'Johari Market, Jaipur', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sap_cities`
--
ALTER TABLE `sap_cities`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `sap_countries`
--
ALTER TABLE `sap_countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `sap_coupons`
--
ALTER TABLE `sap_coupons`
  ADD PRIMARY KEY (`coupon_id`),
  ADD UNIQUE KEY `coupon_code` (`coupon_code`);

--
-- Indexes for table `sap_services`
--
ALTER TABLE `sap_services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `sap_sliders`
--
ALTER TABLE `sap_sliders`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `sap_states`
--
ALTER TABLE `sap_states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `sap_subservices`
--
ALTER TABLE `sap_subservices`
  ADD PRIMARY KEY (`subservice_id`);

--
-- Indexes for table `sap_users`
--
ALTER TABLE `sap_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `sap_user_addresses`
--
ALTER TABLE `sap_user_addresses`
  ADD PRIMARY KEY (`uaddr_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sap_cities`
--
ALTER TABLE `sap_cities`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sap_countries`
--
ALTER TABLE `sap_countries`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `sap_coupons`
--
ALTER TABLE `sap_coupons`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sap_services`
--
ALTER TABLE `sap_services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sap_sliders`
--
ALTER TABLE `sap_sliders`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sap_states`
--
ALTER TABLE `sap_states`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sap_subservices`
--
ALTER TABLE `sap_subservices`
  MODIFY `subservice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sap_users`
--
ALTER TABLE `sap_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sap_user_addresses`
--
ALTER TABLE `sap_user_addresses`
  MODIFY `uaddr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
